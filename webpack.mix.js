const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass("resources/sass/bootstrap/app-quiero.scss", "public/css");

mix.js("resources/js/app.js", "public/js");

mix.js("resources/js/plugins/envios.js", "public/js");
mix.js("resources/js/mayoristas.js", "public/js");
mix.js("resources/js/helpers/buscar-envios.js", "public/js");
mix.js("resources/js/checkout.js", "public/js");
mix.js("resources/js/producto.js", "public/js");
mix.js("resources/js/arma-tu-set.js", "public/js");
mix.js("resources/js/arma-tu-set1.js", "public/js");
mix.js("resources/js/sello-personal.js", "public/js");
mix.js("resources/js/admin/pedidos.js", "public/js/admin");

mix.browserSync({
    proxy: "http://127.0.0.1:8000",
    host: "localhost",
    open: true,
    watchOptions: {
        usePolling: true
    },
    files: [
        "app/**/*.php",
        "resources/views/**/*.php",
        "public/js/**/*.js",
        "public/css/**/*.css"
    ]
});
