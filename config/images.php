<?php

return [
    'full_size'   => env('UPLOAD_FULL_SIZE', public_path('imagenes/')),
    'thumb_size'   => env('UPLOAD_THUMB_SIZE', public_path('imagenes/thumbs/')),
    'icon_size'   => env('UPLOAD_ICON_SIZE', public_path('imagenes/icons/')),
    'catalog_size'   => env('UPLOAD_CATALOG_SIZE', public_path('imagenes/catalog/'))

];
