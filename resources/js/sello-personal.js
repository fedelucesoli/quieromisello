window.Vue = require("vue");

import Vue from "vue";

axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

var vm = new Vue({
    el: "#app",
    data: {
        texto: "",
        tipografia: "",
        dibujo: "",
        completado: false
    },
    methods: {
        abrirModal: function() {},
        seleccionarTipografia(n) {
            this.tipografia = n;
        },
        seleccionarDibujo(n) {
            this.dibujo = n;
        },
        addtoCart() {
            let producto = document.getElementById("producto");
            let datos = {
                productoId: producto.dataset.id,
                precio: "300",
                cantidad: 1,
                texto: this.texto,
                tipografia: this.tipografia,
                dibujo: this.dibujo
            };
            axios
                .post("/carrito/agregar", datos)
                .then(function(response) {
                    console.log(response);
                    window.location.replace("/carrito");
                })
                .catch(function(error) {
                    console.log(error);
                });
            console.log(datos);
        }
    },
    mounted: function() {
        let d = document.getElementById("addToCart");

        if (d) {
            d.remove();
        }
    }
});
