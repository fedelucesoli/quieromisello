// import axios from 'axios'

import Vue from "vue";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

var vm = new Vue({
    el: "#app",
    data: {
        envio: {
            cp: "7240",
            showForm: false,
            showHtml: false,
            html: null,
            textButton: "Calcular envio"
        },
        boton: {
            class: "",
            icono: "",
            texto: "Iniciar Compra"
        },
        tarjetas: {
            carrito: true,
            formulario: false,
            texto: "Iniciar Compra"
        },
        datos: {
            nombre: "",
            apellido: "",
            email: ""
        }
    },
    methods: {
        created: function() {
            // if ->cp en cookie
            // cargar ->costos
        },
        mostrarForm: function(event) {
            console.log("que pasa?");
            console.log(this.envio.showHtml);

            this.envio.showForm = true;
            console.log(this.envio.showHtml);
        },
        calcularenvio: function(event) {
            console.log("Calculamos");
            console.log("CP:" + this.envio.cp);
            console.log("Cambiamos a boton cargando");
            this.envio.textButton =
                ' <svg width="1em" height="1em" class="mr-2 animate__animated animate__heartBeat animate__infinite	infinite" viewBox="0 0 16 16" class="bi bi-asterisk" fill="currentColor" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/> </svg> Cargando';
            console.log("Enviamos a API");
            axios
                .get("/api/envio", {
                    params: {
                        cp: this.envio.cp
                    }
                })
                .then(function(response) {
                    console.log(response);
                    vm.$data.envio.html = response.data.html;
                    vm.$data.envio.showHtml = response.data.status;
                    vm.$data.envio.textButton = "Calcular envio";
                })
                .catch(function(error) {
                    console.log(error);
                });

            console.log("Escondemos Form");
            console.log("mostramos -> response ");
        }
        // iniciarcompra: function(){
        //   console.log('iniciar compra');
        //   this.transicion();
        //   this.boton.texto = "Pagar con MercadoPago"
        // },
        // transicion: function(){
        //   console.log('iniciar compra');
        //   //
        //   this.tarjetas.carrito = false;
        //   this.tarjetas.formulario = true

        //   // $( "#tarjeta" ).animate({
        //   //   $('#tarjeta').addClass('slideOutLeft');
        //   //   }, 5000, function() {
        //   //     // Animation complete.
        //   //     $('#form').removeClass('d-none');
        //   //     $('#form').addClass('slideInRight');
        //   //
        //   //   });

        //   // $('#tarjeta').removeClass('fadeOut');

        // },
        // setPreference: function(){
        //   //
        //   if( this.datos.nombre !== '' && this.datos.apellido !== '' && this.datos.email !== '' ){
        //     axios.post('/api/getPreference', {
        //       nombre: this.datos.nombre,
        //       apellido: this.datos.apellido,
        //       email: this.datos.email
        //     })
        //     .then(function (response) {
        //       console.log(response.body);

        //       // window.location.replace(response.data.url)
        //       // console.log('me mando a carrito');
        //       // vm.$data.datos.review = response.data.body

        //     })
        //     .catch(function (error) {
        //       console.log(error);
        //     });
        //   }else{
        //     // error
        //   }
        // }
    }
});
