/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

import "lazysizes";
import "lazysizes/plugins/parent-fit/ls.parent-fit";
import "lazysizes/plugins/aspectratio/ls.aspectratio";
import "lazysizes/plugins/attrchange/ls.attrchange";

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Cargar modal con contenido
$("#modal-producto").on("show.bs.modal", function(e) {
    let boton = e.relatedTarget;
    let slug = boton.dataset.slug;
    $.ajax({
        url: "/api/producto/" + slug,
        dataType: "JSON",
        success: function(result) {
            console.log(result);
            $("#contenido-producto").html(result.html);
        }
    });
});
