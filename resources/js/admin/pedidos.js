window.Vue = require("vue");

import Vue from "vue";

Vue.component(
    "pedidos-component",
    require("./components/PedidosComponent.vue").default
);
Vue.component(
    "pedido-item-component",
    require("./components/PedidoItemComponent.vue").default
);
Vue.component(
    "pedido-modal-component",
    require("./components/PedidoModalComponent.vue").default
);
Vue.component(
    "pedido-component",
    require("./components/PedidoComponent.vue").default
);

axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

var vm = new Vue({
    el: "#app",
    data: {
        data: {}
    },
    methods: {},
    mounted: function() {}
});
