let showCanvas = function(e) {
  let bsOverlay = $(".bs-canvas-overlay");
  let body = $("body");
  let ctrl = $(this),
    elm = ctrl.is("button") ? ctrl.data("target") : ctrl.attr("href");
  console.log(ctrl);
  $(elm).addClass("mr-0");
  $(elm + " .bs-canvas-close").attr("aria-expanded", "true");
  $('[data-target="' + elm + '"], a[href="' + elm + '"]').attr(
    "aria-expanded",
    "true"
  );
  body.addClass("bs-overflow");
  if (bsOverlay.length) bsOverlay.addClass("show");

  return false;
};
let hideCanvas = function() {
  var elm;
  var bsOverlay = $(".bs-canvas-overlay");
  let body = $("body");
  if ($(this).hasClass("bs-canvas-close")) {
    elm = $(this).closest(".bs-canvas");
    $('[data-target="' + elm + '"], a[href="' + elm + '"]').attr(
      "aria-expanded",
      "false"
    );
  } else {
    elm = $(".bs-canvas");
    $('[data-toggle="canvas"]').attr("aria-expanded", "false");
  }
  elm.removeClass("mr-0");
  $(".bs-canvas-close", elm).attr("aria-expanded", "false");
  body.removeClass("bs-overflow");

  if (bsOverlay.length) bsOverlay.removeClass("show");
  return false;
};

jQuery(document).ready(function($) {
  $('[data-toggle="canvas"]').on("click", showCanvas);
  $(".bs-canvas-close, .bs-canvas-overlay").on("click", hideCanvas);
});
