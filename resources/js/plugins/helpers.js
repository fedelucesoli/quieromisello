/**
 * Required vars:
 *      - currency
 *      - timezone_math
 * Included libraries
 *      - devbridge/jQuery-Autocomplete
 */

/**
 * Valido si un string cumple una expresion regular
 * @param string value
 * @param string regex_string
 * @return bool
 */
function validate_regex(value, regex_string) {
  var regex = new RegExp(regex_string.slice(1, -1));
  return regex.test(value);
}

/**
 * Limpio el string de espacios al inicio y fin
 * @param string cadena
 */
function trim(str) {
  return str.replace(/^\s+|\s+$/gm, "");
}

/**
 * Reviso si dos strings son iguales
 * @param string str1
 * @param string str2
 * @return bool
 */
function compare_strings(str1, str2) {
  str1 = trim(str1);
  str2 = trim(str2);
  return str1 === str2;
}

/**
 * Formateo un numero
 * @param any number
 * @return string
 */
function number_format(number) {
  number = parseFloat(number);
  return (
    currency.symbol +
    number.toLocaleString(currency.code, {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    })
  );
}

/**
 * Reviso si date1 <= date2
 * @param datetime date1
 * @param datetime date2
 */
function compare_dates(date1, date2) {
  if (date1 === "now") {
    date1 = new Date();
    date1.setHours(date1.getHours() + 3);
  } else {
    date1 = new Date(date1);
  }

  if (date2 === "now") {
    date2 = new Date();
    date2.setHours(date2.getHours() + 3);
  } else {
    date2 = new Date(date2);
  }
  return date1 <= date2;
}

/**
 * Reviso si date1 <= between <= date2
 * @param datetime between
 * @param datetime date1
 * @param datetime date2
 * @return bool
 */
function compare_dates_between(between, date1, date2) {
  if (between === "now") {
    between = new Date();
    between.setHours(between.getHours() + 3);
  } else {
    between = new Date(between);
  }

  if (date1 === "now") {
    date1 = new Date();
    date1.setHours(date1.getHours() + 3);
  } else {
    date1 = new Date(date1);
  }

  if (date2 === "now") {
    date2 = new Date();
    date2.setHours(date2.getHours() + 3);
  } else {
    date2 = new Date(date2);
  }

  return date1 <= between && between <= date2;
}

/**
 * Transformo una fecha de utc a la hora local
 * @param string datetime
 */
function utc2local_format(datetime) {
  var date = new Date(datetime);
  date.setHours(date.getHours() + timezone_math);
  var dd = date.getDate();
  var mm = date.getMonth() + 1;
  var yyyy = date.getFullYear();
  dd = dd < 10 ? "0" + dd : dd;
  mm = mm < 10 ? "0" + mm : mm;
  return dd + "/" + mm + "/" + yyyy;
}

/**
 * Reviso si un valor se encuentra en un array
 * @param any val
 * @param array arr
 * @return bool
 */
function in_array(val, arr) {
  var i;
  var sz = arr.length;
  for (i = 0; i < sz; i++) {
    if (val === arr[i]) {
      return true;
    }
  }
  return false;
}

/**
 * Obtengo el % off dados un precio y precio oferta
 * @param float precio
 * @param float precio_oferta
 * @return int
 */
function get_percent_off(precio, precio_oferta) {
  return parseInt(100 - (precio_oferta * 100) / precio);
}

/**
 * Obtengo el precio correspondiente al porcentaje del total redondeado a 2 cifras decimales
 * @param float total
 * @param int porcentaje
 * @return float
 */
function get_discount_price(total, porcentaje) {
  return parseFloat((total * (porcentaje / 100)).toFixed(2));
}

/**
 * Agrego un porcentaje a un monto
 * @param float total
 * @param int porcentaje
 * @return float
 */
function add_percent_amount(total, porcentaje) {
  total = total * 1;
  total += get_discount_price(total, porcentaje);
  return total;
}

/**
 * Resto un porcentaje a un monto
 * @param float total
 * @param int porcentaje
 * @return float
 */
function sub_percent_amount(total, porcentaje) {
  total = total * 1;
  total -= get_discount_price(total, porcentaje);
  return total;
}

/**
 * Reviso si se accede desde un celular/tablet
 * @return bool
 */
function is_mobile() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
    navigator.userAgent
  );
}

/**
 * Armo el query string para el filtrado de atributos
 * @param Object params
 * @return string
 */
function get_filter_query_string(params) {
  let url = "";
  let first = true;
  for (param in params) {
    if (params[param].length) {
      url += first ? "?" : "&";
      url += param + "=" + params[param].join("|");
      first = false;
    }
  }
  return url;
}

/**
 * Obtengo la url full sin query strings
 * @return string
 */
function get_url_without_query_strings() {
  return window.location.href.split("?")[0];
}

/**
 * Reviso si un producto tiene stock
 * @param Object stock
 * @return bool
 */
function product_has_stock(stock) {
  var flag = false;
  stock.forEach(function(item_stock) {
    if (item_stock.s_ilimitado || item_stock.s_cantidad > 0) {
      flag = true;
    }
  });
  return flag;
}

/**
 * Obtengo el link completo de una categoria a partir de su id
 * @param int idCategorias
 * @param array categorias_flatten
 * @return string
 */
function get_category_link(idCategorias, categorias_flatten) {
  var link = "";
  categorias_flatten.forEach(function(categoria) {
    if (categoria.idCategorias === idCategorias) {
      link = categoria.c_link_full;
    }
  });
  return link;
}

/**
 * Construyo los parametros reusables para productos
 * @param json producto
 * @param bool estado_mayorista
 * @param json categorias_flatten
 * @param string uri
 */
function product_get_params(
  producto,
  estado_mayorista,
  categorias_flatten = [],
  uri = ""
) {
  var link_producto = "";
  if (categorias_flatten.length) {
    var c_link_full = get_category_link(
      producto.Categorias_idCategorias,
      categorias_flatten
    );
    link_producto = uri + c_link_full + "/" + producto.p_link;
  }
  var data = {
    sale_off: null,
    precio: null,
    precio_anterior: null,
    stock: product_has_stock(producto.stock),
    link_producto: link_producto
  };

  if (producto.p_mostrar_precio || estado_mayorista) {
    if (estado_mayorista) {
      data.precio = producto.p_precio_mayorista;
    } else if (producto.p_oferta) {
      switch (producto.p_oferta) {
        case 1:
          data.precio = producto.p_precio_oferta;
          data.precio_anterior = producto.p_precio;
          data.sale_off = get_percent_off(
            producto.p_precio,
            producto.p_precio_oferta
          );
          break;
        case 2:
          if (compare_dates("now", producto.p_oferta_fecha)) {
            data.precio = producto.p_precio_oferta;
            data.precio_anterior = producto.p_precio;
            data.sale_off = get_percent_off(
              producto.p_precio,
              producto.p_precio_oferta
            );
          } else {
            data.precio = producto.p_precio;
          }
          break;
        case 3:
          if (
            compare_dates_between(
              "now",
              producto.p_oferta_fecha_inicio,
              producto.p_oferta_fecha
            )
          ) {
            data.precio = producto.p_precio_oferta;
            data.precio_anterior = producto.p_precio;
            data.sale_off = get_percent_off(
              producto.p_precio,
              producto.p_precio_oferta
            );
          } else {
            data.precio = producto.p_precio;
          }
          break;
      }
    } else {
      data.precio = producto.p_precio;
    }
  }
  return data;
}

/**
 * Imprimo un string parametrizado
 * @param string str
 * @param Object key_val
 */
function get_parameterized_lang_string(str, key_val) {
  Object.keys(key_val).forEach(key => {
    str = str.replace(":" + key, key_val[key].toString());
  });
  return str;
}

/**
 * Ordeno los medios de envio (usado en los productos)
 * @param array options
 * @return object
 */
function sort_shipping_options(options) {
  var shipping_options = {
    domicile: {
      featured: [],
      others: []
    },
    office: {
      featured: [],
      others: []
    },
    other: {
      featured: [],
      others: []
    },
    point: {
      featured: [],
      others: []
    }
  };
  var lookup;

  options.forEach(function(option) {
    switch (option.tipo) {
      //mercado envios
      case 1:
        shipping_options.domicile.featured.push(option);
        break;

      //local
      case 2:
        shipping_options.point.featured.push(option);
        break;

      //acordar
      case 3:
        shipping_options.other.featured.push(option);
        break;

      //envios personalizados
      case 5:
        shipping_options.other.featured.push(option);
        break;

      //oca
      case 6:
        if (option.subtipo == 2) {
          //domicilio
          shipping_options.domicile.featured.push(option);
        } else {
          //sucursal
          lookup = shipping_options.office.featured.filter(function(item) {
            return item.tipo === 6;
          });

          if (lookup.length) {
            shipping_options.office.others.push(option);
          } else {
            shipping_options.office.featured.push(option);
          }
        }
        break;

      //email
      case 7:
        break;

      //correo argentino
      case 8:
        if (option.subtipo >= 2) {
          //domicilio
          shipping_options.domicile.featured.push(option);
        } else {
          //sucursal
          lookup = shipping_options.office.featured.filter(function(item) {
            return item.tipo === 8;
          });

          if (lookup.length) {
            shipping_options.office.others.push(option);
          } else {
            shipping_options.office.featured.push(option);
          }
        }
        break;

      //e-pick
      case 9:
        //domicilio
        shipping_options.domicile.featured.push(option);
        break;
    }
  });

  return shipping_options;
}

/**
 * Ordeno los medios de envio (usado en los carritos)
 * @param array envios_disponibles
 * @param int envio_seleccionado
 * @return object
 */
function sort_shipping_cart_options(envios_disponibles, envio_seleccionado) {
  //guardo la posicion original
  envios_disponibles = envios_disponibles.map(function(option, index) {
    option.position = index;
    return option;
  });

  //organizo los envios
  envios_disponibles = sort_shipping_options(envios_disponibles);

  //reviso si el envio seleccionado se encuentra en la lista other de office
  var in_other_office = false;
  envios_disponibles.office.others.forEach(function(option) {
    if (option.position === envio_seleccionado) {
      in_other_office = true;
    }
  });

  return {
    in_other_office: in_other_office,
    options: envios_disponibles
  };
}

/**
 * Formateo los costos de envio
 * @param object option
 * @return object
 */
function format_shipment_price(option) {
  if (option.tipo === 5) {
    switch (option.cobro_envio) {
      case 0:
        return {
          price: 0,
          label: product_common.shipment_free_label,
          label_detail: number_format(0)
        };
        break;
      case 1:
        return {
          price: option.precio,
          label: number_format(option.precio),
          label_detail: number_format(option.precio)
        };
        break;
      case 2:
        return {
          price: 0,
          label:
            number_format(option.precio) +
            " (" +
            product_common.shipment_pay_on_destination +
            ")",
          label_detail:
            number_format(option.precio) +
            " (" +
            product_common.shipment_pay_on_destination +
            ")"
        };
        break;
      case 3:
        return {
          price: 0,
          label: "(" + product_common.shipment_pay_on_destination + ")",
          label_detail: "(" + product_common.shipment_pay_on_destination + ")"
        };
        break;
    }
  } else {
    if (option.precio > 0) {
      return {
        price: option.precio,
        label: number_format(option.precio),
        label_detail: number_format(option.precio)
      };
    } else {
      if (option.tipo === 3) {
        return {
          price: 0,
          label: "",
          label_detail: ""
        };
      } else {
        return {
          price: 0,
          label: product_common.shipment_free_label,
          label_detail: number_format(0)
        };
      }
    }
  }
}

/**
 * Formateo los dias de un envio
 * @param object option
 * @return object
 */
function format_shipment_days(option) {
  var days = option.dias;

  var resp = {
    label: "",
    after: product_common.shipment_days_after
  };

  if (days !== null) {
    resp.label = get_parameterized_lang_string(
      days === 1 ? product_common.shipment_day : product_common.shipment_days,
      {
        days: days
      }
    );
  }

  return resp;
}

/**
 * Devuelvo el label correspondiente al metodo de envio
 * @param int metodo_envio
 * @param json envio_seleccionado
 * @return string
 */
function label_shipment_method(metodo_envio, envio_seleccionado) {
  switch (metodo_envio) {
    case 1:
      return "Mercado Envíos";
      break;
    case 2:
      return envio_seleccionado.nombre;
      break;
    case 3:
      return "Acordar envío";
      break;
    case 5:
      return envio_seleccionado.nombre;
      break;
    case 6:
      return "OCA";
      break;
    case 7:
      return "Email";
      break;
    case 8:
      return "Correo Argentino";
      break;
    case 9:
      return "E-Pick";
      break;
  }
}

/**
 * Devuelvo el label correspondiente al metodo de pago
 * @param int metodo_pago
 * @return string
 */
function label_payment_method(metodo_pago) {
  switch (metodo_pago) {
    case 1:
      return "Mercado Pago";
      break;
    case 2:
      return "Efectivo";
      break;
    case 3:
      return "Acordar pago";
      break;
    case 4:
      return "Todo Pago";
      break;
    case 5:
      return "Transferencia / Depósito";
      break;
    case 6:
      return "Mobbex";
      break;
  }
}

/**
 * Devuelvo el label correspondiente al estado de pago
 * @param int estado_pago
 * @return string
 */
function label_payment_status(estado_pago) {
  switch (estado_pago) {
    case -1:
      return "Esperando pago";
      break;
    case 0:
      return "Pendiente";
      break;
    case 1:
      return "Finalizado";
      break;
    case 2:
      return "En proceso";
      break;
    case 3:
      return "En mediación";
      break;
    case 4:
      return "Rechazado";
      break;
    case 5:
      return "Cancelado";
      break;
    case 6:
      return "Pago devuelto";
      break;
    case 7:
      return "En contracargo";
      break;
  }
}

/**
 * Devuelvo el label correspondiente al estado de envio
 * @param int metodo_envio
 * @param int estado_envio
 * @return string
 */
function label_shipment_status(metodo_envio, estado_envio) {
  switch (estado_envio) {
    case 0:
      if (metodo_envio === 2) {
        return "Pendiente de retiro";
      } else {
        return "Pendiente";
      }
      break;
    case 1:
      if (metodo_envio === 2) {
        return "Esperando retiro";
      } else {
        return "En preparación";
      }
      break;
    case 2:
      if (metodo_envio === 2) {
        return "Retirado";
      } else {
        return "Enviado";
      }
      break;
  }
}

///////////////////////////
// DOM-RELATED FUNCTIONS //
///////////////////////////

/**
 * Valido un campo (input o textarea)
 * @param element field_id
 * @return bool
 */
function validate_field(field) {
  var value = trim(field.val());
  var regex_string = field.attr("data-regex");
  var error_message = field.attr("data-message");
  var required = parseInt(field.attr("data-required"));
  var id = field.attr("id");

  $(".field--" + id).removeClass("field--error");

  if (required || value) {
    if (validate_regex(value, regex_string)) {
      return true;
    } else {
      $(".field--" + id).addClass("field--error");
      $(".field__message--" + id).html(error_message);
      return false;
    }
  } else {
    return true;
  }
}

/**
 * Coloco un campo como invalido sin importar su valor
 * @param element field
 * @param string error_message
 * @return void
 */
function invalidate_field(field, error_message) {
  var id = field.attr("id");
  $(".field--" + id).addClass("field--error");
  $(".field__message--" + id).html(error_message);
}

/**
 * Reviso si dos campos tienen el mismo valor
 * @param element field1
 * @param element field2
 * @return bool
 */
function compare_fields(field1, field2) {
  var str1 = field1.val();
  var str2 = field2.val();
  return compare_strings(str1, str2);
}

/**
 * Valido un formulario completo
 * @param element form
 * @return bool
 */
function validate_form(form) {
  var flag = true;
  form.find(".field__input, .field__textarea").each(function() {
    var field = $(this);
    if (!validate_field(field)) {
      flag = false;
    }
  });
  return flag;
}

/**
 * Redirecciono a url
 * @param string url
 * @return void
 */
function redirect(url) {
  $(location).attr("href", url);
}

/**
 * Coloco el boton en estado de loading
 * @param element btn
 * @return void
 */
function set_loading_button(btn) {
  var ratio = btn.attr("data-spinner-ratio");
  btn.addClass("button--inactive");
  btn.attr("disabled", "disabled");
  btn.html('<div uk-spinner="ratio: ' + ratio + '"></div>');
}

/**
 * Quito el boton del estado loading
 * @param element btn
 * @param bool defer (for captcha reset)
 * @return void
 */
function unset_loading_button(btn, defer = false) {
  if (defer) {
    setTimeout(function() {
      var label = btn.attr("data-label");
      btn.removeClass("button--inactive");
      btn.removeAttr("disabled", "disabled");
      btn.html(label);
    }, 1000);
  } else {
    var label = btn.attr("data-label");
    btn.removeClass("button--inactive");
    btn.removeAttr("disabled", "disabled");
    btn.html(label);
  }
}

/**
 * Muestro un spinner de carga
 * @param element div (debe ser un grid)
 * @param float ratio
 */
function set_loading_spinner(div, ratio) {
  div.html(
    '<div class="uk-width-1-1 uk-flex-middle uk-flex-center"><div uk-spinner="ratio: ' +
      ratio +
      '"></div></div>'
  );
}

/**
 * Muestro una alerta
 * @param string type
 * @param string message
 * @return string
 */
function alert_message(type, message) {
  var html = '<div class="alert alert--' + type + '" uk-alert>';
  html +=
    '<a class="alert__close alert__close--' +
    type +
    ' uk-alert-close" uk-close></a>';
  html +=
    '<p class="alert__message alert__message--' +
    type +
    '">' +
    message +
    "</p>";
  html += "</div>";
  return html;
}

/**
 * Muestro una alerta de exito
 * @param element div
 * @param string message
 * @param bool append
 * @return void
 */
function success(div, message, append = false) {
  if (append) {
    div.append(alert_message("success", message));
  } else {
    div.html(alert_message("success", message));
  }
}

/**
 * Muestro una alerta de info
 * @param element div
 * @param string message
 * @param bool append
 * @return void
 */
function info(div, message, append = false) {
  if (append) {
    div.append(alert_message("info", message));
  } else {
    div.html(alert_message("info", message));
  }
}

/**
 * Muestro una alerta de info
 * @param element div
 * @param string message
 * @param bool append
 * @return void
 */
function error(div, message, append = false) {
  if (append) {
    div.append(alert_message("error", message));
  } else {
    div.html(alert_message("error", message));
  }
}

/**
 * Muestro varias alertas de mensaje en un div
 * @param {*} div
 * @param {*} messages
 */
function error_multiple(div, messages) {
  var newHtml = "";
  messages.forEach(function(message) {
    newHtml += alert_message("error", message);
  });
  div.html(newHtml);
}

/**
 * Muevo el scroll
 * @param element el
 * @param int speed ms
 * @param int scale offset
 * @return void
 */
function goTo(el, speed = 200, scale = 100) {
  $("html, body").animate(
    {
      scrollTop: el.offset().top - scale
    },
    speed
  );
}

/**
 * Muevo el scroll sobre un elemento base especifico
 * @param element el
 * @param int speed ms
 * @return void
 */
function goToSpecific(el, speed = 200) {
  el.animate(
    {
      scrollTop: 0
    },
    speed
  );
}

/**
 * Required vars:
 *      - estado_mayorista
 *      - uri
 *      - categorias_flatten
 *      - pago_online
 *      - products_feed
 *      - product_common
 */

var navbar = $(".header-menu");
var sticky = navbar.offset().top;
var header_announcement = $(".header-announcement");
var sitekey = "6LfXRycUAAAAAFQF69mKvgHerNR9oDrDYw1BM_Kw";
var csrf_token = $('meta[name="csrf-token"]').attr("content");
var cdn = get_cloudfront_url("productos");

var login_captcha = null;
var register_captcha = null;
var recover_captcha = null;
var wholesaler_captcha = null;
var contact_captcha = null;
var newsletter_captcha = null;
var regret_captcha = null;

window.onscroll = function() {
  stickyNavbar();
  searchPush();
};
function stickyNavbar() {
  if (window.pageYOffset > sticky) {
    if ($(".nav.first > li").length <= 10 || $("body").width() <= 960) {
      if (navbar.attr("class") !== "header-menu header-menu--sticky") {
        navbar.attr("class", "header-menu header-menu--sticky");
        $(".header-search__wrapper").addClass("header-search__wrapper--sticky");
      }
    } else {
      if (navbar.attr("class") !== "header-menu") {
        navbar.attr("class", "header-menu");
        $(".header-search__wrapper").removeClass(
          "header-search__wrapper--sticky"
        );
      }
    }
  } else {
    if (navbar.attr("class") !== "header-menu") {
      navbar.attr("class", "header-menu");
      $(".header-search__wrapper").removeClass(
        "header-search__wrapper--sticky"
      );
    }
  }
}
function searchPush() {
  if (header_announcement.length) {
    if (window.pageYOffset > header_announcement.offset().top) {
      $(".header-search").removeAttr("style");
    } else {
      $(".header-search").attr(
        "style",
        "top: " + header_announcement.height() + "px"
      );
    }
  }
}

function product_item(producto, product_css_prefix) {
  var p_producto = product_get_params(
    producto,
    estado_mayorista,
    categorias_flatten,
    uri
  );

  var html = "";
  html += '<div class="' + product_css_prefix + '-wrapper">';
  html += '<div class="' + product_css_prefix + '-media">';
  html +=
    '<a href="' +
    p_producto.link_producto +
    '" class="' +
    product_css_prefix +
    '-link">';
  if (p_producto.sale_off) {
    html +=
      '<span class="' +
      product_css_prefix +
      '-offer background--primary contrast_text--primary">';
    html += p_producto.sale_off + "% OFF";
    html += "</span>";
  }
  if (!p_producto.stock) {
    html += '<span class="' + product_css_prefix + '-out-stock">';
    html += product_common.out_of_stock;
    html += "</span>";
  }
  html +=
    '<img class="' +
    product_css_prefix +
    '-image" src="' +
    cdn +
    producto.imagenes[0].i_link +
    '" alt="Producto - ' +
    producto.p_nombre +
    '"/>';
  html += "</a>";
  html += "</div>";
  html += '<h3 class="' + product_css_prefix + '-name text--primary">';
  html += '<a href="' + p_producto.link_producto + '">';
  html += producto.p_nombre;
  html += "</a>";
  html += "</h3>";
  if (p_producto.precio) {
    html += '<p class="' + product_css_prefix + '-price text--primary">';
    if (p_producto.precio_anterior) {
      html += "<del>" + number_format(p_producto.precio_anterior) + "</del>";
    }
    html += number_format(p_producto.precio);
    html += "</p>";
  }
  if (pago_online) {
    if (products_feed.product_subtext_type === 0) {
      html += '<p class="' + product_css_prefix + '-additional text--primary">';
      html += products_feed.product_subtext;
      html + "</p>";
    } else if (products_feed.product_subtext_type == 1 && p_producto.precio) {
      html += '<p class="' + product_css_prefix + '-additional text--primary">';
      html += get_parameterized_lang_string(product_common.list_installments, {
        installments: products_feed.product_subtext_cuotas,
        amount: number_format(
          p_producto.precio / products_feed.product_subtext_cuotas
        )
      });
      html += "</p>";
    }
  }
  if (estado_mayorista) {
    html += '<p class="' + product_css_prefix + '-additional text--primary">';
    html += get_parameterized_lang_string(product_common.wholesale_min_qty, {
      qty: producto.p_cantidad_minima
    });
    html += "</p>";
  }
  html += "</div>";

  return html;
}

function input(
  id,
  type,
  label,
  regex,
  error_message,
  placeholder,
  value,
  required,
  extra_css
) {
  var newHtml = '<div class="field field--' + id + '">';
  if (label) {
    newHtml +=
      '<label class="field__label field__label--' +
      id +
      '" for="' +
      id +
      '">' +
      label +
      (required ? "" : " (opcional)") +
      "</label>";
  }
  if (value) {
    newHtml +=
      '<input type="' +
      type +
      '" name="' +
      id +
      '" id="' +
      id +
      '" class="field__input border-radius' +
      (extra_css ? " " + extra_css : "") +
      '" value="' +
      value +
      '" data-regex="' +
      regex +
      '" data-message="' +
      error_message +
      '" ' +
      (placeholder ? 'placeholder="' + placeholder + '"' : "") +
      (required ? 'data-required="1"' : 'data-required="0"') +
      "/>";
  } else {
    newHtml +=
      '<input type="' +
      type +
      '" name="' +
      id +
      '" id="' +
      id +
      '" class="field__input border-radius' +
      (extra_css ? " " + extra_css : "") +
      '" data-regex="' +
      regex +
      '" data-message="' +
      error_message +
      '" ' +
      (placeholder ? 'placeholder="' + placeholder + '"' : "") +
      (required ? 'data-required="1"' : 'data-required="0"') +
      "/>";
  }
  newHtml += '<p class="field__message field__message--' + id + '"></p>';
  newHtml += "</div>";
  return newHtml;
}
function button(
  id,
  label,
  size,
  type,
  disabled,
  loading,
  show,
  width,
  extra,
  extra_css
) {
  var ratio;
  switch (size) {
    case "small":
      ratio = "0.5";
      break;
    case "normal":
      ratio = "0.75";
      break;
    case "input":
      ratio = "0.75";
      break;
    case "large":
      ratio = "1";
      break;
  }
  extra_css = extra_css === "" ? "" : " " + extra_css;
  extra_css += loading || disabled ? " button--inactive" : "";
  extra_css += show ? "" : " button--hidden";
  extra_css += width ? " button--" + width : "";

  var newHtml =
    '<button type="' +
    type +
    '" id="' +
    id +
    '" class="button' +
    extra_css +
    " background--primary background--primary-hover contrast_text--primary contrast_text--primary-hover uk-button uk-button-" +
    size +
    ' border-radius" ' +
    (disabled ? "disabled" : "") +
    ' data-label="' +
    label +
    '" data-spinner-ratio="' +
    ratio +
    '" ' +
    extra +
    ">";
  if (loading) {
    newHtml += '<div uk-spinner="ratio: ' + ratio + '"></div>';
  } else {
    newHtml += label;
  }
  newHtml += "</button>";
  return newHtml;
}

//cart
var ca_carrito;
var ca_carrito_enable_cp_edit = false;
function get_cart() {
  $(".cart-sidenav__loader").show();
  $.ajax({
    headers: {
      "X-CSRF-TOKEN": csrf_token
    },
    url: "/v4/cart",
    dataType: "JSON",
    method: "GET"
  })
    .done(function(resp) {
      var ca_carrito_new = resp.data;
      build_cart(ca_carrito_new, open_cart, []);
    })
    .fail(function(err) {
      var resp = err.responseJSON;
      $(".cart-sidenav__loader").hide();
      if (resp && resp.message) {
        error($(".cart-sidenav__msg"), resp.message.description);
      }
    });
}
function build_single_shipping_cart_option(option, envio_seleccionado) {
  var price_obj = format_shipment_price(option);
  var days_obj = format_shipment_days(option);
  var image =
    "https://dk0k1i3js6c49.cloudfront.net/iconos-envio/costo-envio/" +
    option.icono;
  var newHtml = '<li class="cart-sidenav__shipment-result-list-item">';
  newHtml += '<div class="uk-grid-collapse uk-flex-middle" uk-grid>';
  newHtml += '<div class="uk-width-auto">';
  newHtml += '<label for="shipment_option_radio-' + option.position + '">';
  if (option.position === envio_seleccionado) {
    newHtml +=
      '<input type="radio" name="shipment_option_radio" class="shipment_option_radio with-gap" value="' +
      option.position +
      '" id="shipment_option_radio-' +
      option.position +
      '" checked="checked"/><span></span>';
  } else {
    newHtml +=
      '<input type="radio" name="shipment_option_radio" class="shipment_option_radio with-gap" value="' +
      option.position +
      '" id="shipment_option_radio-' +
      option.position +
      '"/><span></span>';
  }
  newHtml += "</label>";
  newHtml += "</div>";
  newHtml += '<div class="uk-width-expand">';
  newHtml += '<div class="uk-flex uk-flex-middle">';
  newHtml += '<div class="cart-sidenav__shipment-result-item-image-wrapper">';
  newHtml += '<label for="shipment_option_radio-' + option.position + '">';
  newHtml +=
    '<img src="' +
    image +
    '" class="cart-sidenav__shipment-result-item-image">';
  newHtml += "</label>";
  newHtml += "</div>";
  newHtml += '<div class="cart-sidenav__shipment-result-item-info-wrapper">';
  newHtml += '<p class="cart-sidenav__shipment-result-item-info-title">';
  newHtml += '<label for="shipment_option_radio-' + option.position + '">';
  newHtml += option.nombre;
  newHtml += "</label>";
  newHtml += "</p>";
  if (price_obj.label) {
    newHtml += '<p class="cart-sidenav__shipment-result-item-info-price">';
    newHtml += '<label for="shipment_option_radio-' + option.position + '">';
    newHtml +=
      '<span class="cart-sidenav__shipment-result-item-info-price-wrapper">' +
      price_obj.label +
      "</span>";
    if (days_obj.label) {
      newHtml += " - " + days_obj.label;
      newHtml +=
        ' <span class="cart-sidenav__shipment-result-item-info-after">(' +
        days_obj.after +
        ")</span>";
    }
    newHtml += "</label>";
    newHtml += "</p>";
  }
  if (option.descripcion.length) {
    newHtml +=
      '<p class="cart-sidenav__shipment-result-item-info-description">';
    newHtml += '<label for="shipment_option_radio-' + option.position + '">';
    newHtml += option.descripcion.join(" - ");
    newHtml += "</label>";
    newHtml += "</p>";
  }
  newHtml += "</div>";
  newHtml += "</div>";
  newHtml += "</div>";
  newHtml += "</div>";
  newHtml += "</li>";
  return newHtml;
}
function build_shipping_cart_options(
  key,
  envios_format,
  envio_seleccionado,
  in_other_office
) {
  var newHtml = "";
  if (envios_format[key].featured.length) {
    newHtml += "<div>";
    newHtml += '<p class="cart-sidenav__shipment-result-group-title">';
    newHtml += product_common["shipment_" + key];
    newHtml += "</p>";
    newHtml +=
      '<ul class="cart-sidenav__shipment-result-list cart-sidenav__shipment-result-list--' +
      key +
      ' border-radius">';

    envios_format[key].featured.forEach(function(option) {
      newHtml += build_single_shipping_cart_option(option, envio_seleccionado);
    });
    if (in_other_office && key === "office") {
      envios_format[key].others.forEach(function(option) {
        newHtml += build_single_shipping_cart_option(
          option,
          envio_seleccionado
        );
      });
    }
    newHtml += "</ul>";

    if (envios_format[key].others.length && !in_other_office) {
      newHtml +=
        '<p class="cart-sidenav__shipment-result-item-show-more"><a href="#" class="cart-sidenav__shipment-result-item-show-more-link" data-key="' +
        key +
        '">' +
        product_common.shipment_office_show_more +
        ' <span uk-icon="chevron-down"></span></a></p>';
    }

    newHtml += "</div>";
  }
  return newHtml;
}
function build_cart(ca_carrito_new, open_side, errors) {
  ca_carrito = ca_carrito_new;
  var productos = ca_carrito.productos;
  var subtotal = 0;
  var total = 0;
  var cantidad = 0;
  var html = "";

  $(".cart-sidenav__loader").show();
  $(".cart-sidenav__msg").html("");

  if (open_side) UIkit.offcanvas("#cart-sidenav").show();

  for (idStock in productos) {
    var producto = productos[idStock];
    var precio_item = producto.precio_unitario * producto.cantidad;
    subtotal += precio_item;
    cantidad += 1;

    if (cantidad === 1) {
      html +=
        '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
    }
    html += '<li class="cart-sidenav__item">';
    html += '<div class="cart-sidenav__left">';
    html +=
      '<img src="' +
      cdn +
      producto.img +
      '" class="cart-sidenav__product-image"/>';
    html += '<div class="cart-sidenav__product-info">';
    html +=
      '<p class="cart-sidenav__product-title">' + producto.nombre + "</p>";
    html += '<p class="cart-sidenav__product-subtitle">';
    for (var i in producto.info_atributos) {
      var atributos_keys = Object.keys(producto.info_atributos);
      var atributo = producto.info_atributos[i];
      if (parseInt(i) === parseInt(atributos_keys[atributos_keys.length - 1])) {
        html += atributo.at_nombre + ": " + atributo.vat_valor;
      } else {
        html += atributo.at_nombre + ": " + atributo.vat_valor + " - ";
      }
    }
    html += "</p>";
    html += '<div class="cart-sidenav__product-qty-container">';
    html +=
      '<button class="cart-sidenav__product-qty-button' +
      (producto.cantidad === 1
        ? " cart-sidenav__product-qty-button--disabled"
        : "") +
      '" data-action="remove" data-stk="' +
      idStock +
      '" data-product="' +
      producto.id +
      '"><i class="fas fa-minus"></i></button>';
    html +=
      '<span class="cart-sidenav__product-qty">' +
      producto.cantidad +
      "</span>";
    html +=
      '<button class="cart-sidenav__product-qty-button" data-action="add" data-stk="' +
      idStock +
      '" data-product="' +
      producto.id +
      '"><i class="fas fa-plus"></i></button>';
    html += "</div>";
    html += "</div>";
    html += "</div>";
    html += '<div class="cart-sidenav__right">';
    html +=
      '<p class="cart-sidenav__product-price">' +
      number_format(precio_item) +
      "</p>";
    html +=
      '<button class="cart-sidenav__product-delete-button" data-stk="' +
      idStock +
      '" data-product="' +
      producto.id +
      '"><i class="fas fa-trash"></i></button>';
    html += "</div>";
    html += "</li>";
  }

  if (cantidad === 0) {
    html +=
      '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
    html += '<li class="cart-sidenav__item cart-sidenav__item--centered">';
    html +=
      '<p class="cart-sidenav__info-text uk-text-center">' +
      cart_labels.sidenav_empty_cart +
      "</p>";
    html +=
      '<a href="#" class="cart-sidenav__button-link" uk-toggle="target: #cart-sidenav">' +
      cart_labels.sidenav_back_shop +
      "</a>";
    html += "</li>";
  }

  //subtotal
  if (cantidad > 0) {
    html +=
      '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
    html += '<li class="cart-sidenav__item cart-sidenav__item--detail">';
    html += '<div class="cart-sidenav__left">';
    html +=
      '<p class="cart-sidenav__detail-title">' +
      cart_labels.sidenav_subtotal +
      "</p>";
    html += "</div>";
    html += '<div class="cart-sidenav__right">';
    html +=
      '<p class="cart-sidenav__detail-price">' +
      number_format(subtotal) +
      "</p>";
    html += "</div>";
    html += "</li>";

    total = subtotal;

    //envios
    var envio = ca_carrito.envio;
    if (envio.requerido) {
      if (envio.codigo_postal && !ca_carrito_enable_cp_edit) {
        html +=
          '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
        html += '<li class="cart-sidenav__item cart-sidenav__item--detail">';
        html += '<div class="cart-sidenav__left">';
        html += '<div class="cart-sidenav__left-wrapper">';
        html +=
          '<p class="cart-sidenav__detail-title">' +
          cart_labels.sidenav_shipment_list_title +
          "</p>";
        html +=
          '<p class="cart-sidenav__shipment-zip-code">CP ' +
          envio.codigo_postal +
          "</p>";
        html += "</div>";
        html += "</div>";
        html += '<div class="cart-sidenav__right">';
        html +=
          '<p class="cart-sidenav__detail-action"><a href="#" class="change_shipment-btn">' +
          cart_labels.sidenav_shipment_list_change_zip_code +
          "</a></p>";
        html += "</div>";
        html += "</li>";

        //cargo los metodos de envio
        var envios = envio.envios_disponibles;
        var envios_format_obj = sort_shipping_cart_options(
          envio.envios_disponibles,
          envio.envio_seleccionado
        );
        var in_other_office = envios_format_obj.in_other_office;
        var envios_format = envios_format_obj.options;

        if (envios.length > 0) {
          html += '<li class="cart-sidenav__item">';
          html +=
            '<div class="cart-sidenav_shipment-results uk-grid-small uk-child-width-1-1" uk-grid>';
          html += build_shipping_cart_options(
            "domicile",
            envios_format,
            envio.envio_seleccionado,
            in_other_office
          );
          html += build_shipping_cart_options(
            "office",
            envios_format,
            envio.envio_seleccionado,
            in_other_office
          );
          html += build_shipping_cart_options(
            "other",
            envios_format,
            envio.envio_seleccionado,
            in_other_office
          );
          html += build_shipping_cart_options(
            "point",
            envios_format,
            envio.envio_seleccionado,
            in_other_office
          );
          html += "</div>";
          html += "</li>";

          var envio_seleccionado = format_shipment_price(
            envios[envio.envio_seleccionado]
          );
          total = total + envio_seleccionado.price;
          if (envios[envio.envio_seleccionado].tipo !== 3) {
            html +=
              '<li class="cart-sidenav__item cart-sidenav__item--detail">';
            html += '<div class="cart-sidenav__left">';
            html +=
              '<p class="cart-sidenav__detail-title">' +
              cart_labels.sidenav_shipment +
              "</p>";
            html += "</div>";
            html += '<div class="cart-sidenav__right">';
            html +=
              '<p class="cart-sidenav__detail-price">' +
              envio_seleccionado.label_detail +
              "</p>";
            html += "</div>";
            html += "</li>";
          }
        }
      } else {
        html +=
          '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
        html += '<li class="cart-sidenav__item cart-sidenav__item--detail">';
        html += '<form method="POST" id="search_shipments-form">';
        html +=
          '<p class="cart-sidenav__detail-title"><a href="#" class="search_shipments-toggle uk-flex uk-flex-between uk-flex-middle"><span>' +
          cart_labels.sidenav_shipment_title +
          '</span><span class="search_shipments-icon" uk-icon="chevron-down"></span></a></p>';
        html +=
          '<div class="search_shipments-grid uk-grid-collapse" style="display: none;" uk-grid>';
        if (alerta_envio) {
          html += '<div class="uk-width-1-1 uk-margin">';
          html += alert_message(
            "info",
            '<span uk-icon="icon: info; ratio: 0.9;"></span> <span class="cart-sidenav__alert-text">' +
              alerta_envio_mensaje +
              "</span>"
          );
          html += "</div>";
        }
        html += '<div class="uk-width-3-5">';
        html += input(
          "search_shipments-zip_code",
          "text",
          "",
          fields.zip_code.regex,
          fields.zip_code.error_message,
          fields.zip_code.placeholder,
          envio.codigo_postal,
          true,
          "field__input--right-button"
        );
        html += "</div>";
        html += '<div class="uk-width-2-5">';
        html += button(
          "search_shipments-btn",
          cart_labels.sidenav_shipment_button,
          "input",
          "submit",
          false,
          false,
          true,
          "full",
          "",
          "uk-button-input--no-radius uk-button-input-outline"
        );
        html += "</div>";
        html += "</div>";
        html += "</form>";
        html += "</li>";
      }
    }
    ca_carrito_enable_cp_edit = false;

    //cupon
    if (parseInt(cupones_descuento)) {
      var cupon = ca_carrito.cupon;
      if (cupon.id) {
        total = total - cupon.monto_descuento;

        html +=
          '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
        html += '<li class="cart-sidenav__item cart-sidenav__item--detail">';
        html += '<div class="cart-sidenav__left">';
        html += '<div class="cart-sidenav__left-wrapper">';
        html +=
          '<p class="cart-sidenav__detail-title">' +
          cart_labels.sidenav_coupon +
          "</p>";
        html +=
          '<p class="cart-sidenav__detail-action"><a href="#" class="remove_coupon-btn">' +
          cart_labels.sidenav_coupon_remove_button +
          "</a></p>";
        html += "</div>";
        html += "</div>";
        html += '<div class="cart-sidenav__right">';
        html +=
          '<p class="cart-sidenav__detail-price">-' +
          number_format(cupon.monto_descuento) +
          "</p>";
        html += "</div>";
        html += "</li>";
      } else {
        html +=
          '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
        html += '<li class="cart-sidenav__item cart-sidenav__item--detail">';
        html += '<form method="POST" id="add_coupon-form">';
        html +=
          '<p class="cart-sidenav__detail-title"><a href="#" class="uk-flex uk-flex-between uk-flex-middle add_coupon-toggle"><span>' +
          cart_labels.sidenav_coupon_add_title +
          '</span><span class="add_coupon-icon" uk-icon="chevron-down"></span></a></p>';
        html +=
          '<div class="add_coupon-grid uk-grid-collapse" style="display: none;" uk-grid>';
        html += '<div class="uk-width-3-5">';
        html += input(
          "add_coupon-coupon",
          "text",
          "",
          fields.discount_code.regex,
          fields.discount_code.error_message,
          fields.discount_code.placeholder,
          "",
          true,
          "field__input--right-button"
        );
        html += "</div>";
        html += '<div class="uk-width-2-5">';
        html += button(
          "add_coupon-btn",
          cart_labels.sidenav_coupon_button,
          "input",
          "submit",
          false,
          false,
          true,
          "full",
          "",
          "uk-button-input--no-radius uk-button-input-outline"
        );
        html += "</div>";
        html += "</div>";
        html += "</form>";
        html += "</li>";
      }
    }

    //total
    html +=
      '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
    html += '<li class="cart-sidenav__item cart-sidenav__item--detail">';
    html += '<div class="cart-sidenav__left">';
    html +=
      '<p class="cart-sidenav__detail-title">' +
      cart_labels.sidenav_total +
      "</p>";
    html += "</div>";
    html += '<div class="cart-sidenav__right">';
    html +=
      '<p class="cart-sidenav__detail-price">' + number_format(total) + "</p>";
    html += "</div>";
    html += "</li>";

    html +=
      '<li class="cart-sidenav__item cart-sidenav__item--divider"><div class="cart-sidenav__divider"></div></li>';
    html += '<li class="cart-sidenav__item cart-sidenav__item--button-area">';
    html += button(
      "start_checkout-btn",
      cart_labels.sidenav_checkout_start_button,
      "large",
      "button",
      false,
      false,
      true,
      "full",
      "",
      "border-radius--full"
    );
    html +=
      '<a href="#" class="cart-sidenav__button-link" uk-toggle="target: #cart-sidenav">' +
      cart_labels.sidenav_keep_buying_button +
      "</a>";
    html += "</li>";
  }

  //actualizo cantidades
  $(".cart-qty").html(cantidad);
  $(".cart-price").html(number_format(total));

  //saco el loader
  $(".cart-sidenav__loader").hide();

  //cargo el contenido
  $(".cart-sidenav__content").html(html);

  //muestro errores si hay
  if (errors.length) {
    error_multiple($(".cart-sidenav__msg"), errors);
    goToSpecific($(".cart-sidenav__offcanvas-bar"), 300);
  }
}
//end cart

$(function() {
  //input validation
  $(document).on("blur", ".field__input, .field__textarea", function() {
    var field = $(this);
    setTimeout(function() {
      validate_field(field);
    }, 20);
  });
  //end input validation

  //cart
  get_cart();
  $(".cart-sidenav__content").on(
    "click",
    ".cart-sidenav__product-qty-button",
    function(e) {
      e.preventDefault();
      $(".cart-sidenav__loader").show();
      var action = $(this).attr("data-action");
      var stock = $(this).attr("data-stk");
      var product = $(this).attr("data-product");
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/cart/qty",
        dataType: "JSON",
        method: "POST",
        data: {
          action: action,
          stock: stock,
          product: product
        }
      })
        .done(function(resp) {
          var errors = resp.message.description;
          var ca_carrito_new = resp.data;
          build_cart(ca_carrito_new, false, errors);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          if (resp && resp.message) {
            var code = resp.message.code;
            if (code) {
              var errors = resp.message.description;
              var ca_carrito_new = resp.data;
              build_cart(ca_carrito_new, false, errors);
            } else {
              build_cart(ca_carrito, false, [resp.message.description]);
            }
          } else {
            build_cart(ca_carrito, false, []);
          }
        });
    }
  );
  $(".cart-sidenav__content").on(
    "click",
    ".cart-sidenav__product-delete-button",
    function(e) {
      e.preventDefault();
      $(".cart-sidenav__loader").show();
      var stock = $(this).attr("data-stk");
      var product = $(this).attr("data-product");
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/cart/delete",
        dataType: "JSON",
        method: "POST",
        data: {
          stock: stock,
          product: product
        }
      })
        .done(function(resp) {
          var errors = resp.message.description;
          var ca_carrito_new = resp.data;
          build_cart(ca_carrito_new, false, errors);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          if (resp && resp.message) {
            var code = resp.message.code;
            if (code) {
              var errors = resp.message.description;
              var ca_carrito_new = resp.data;
              build_cart(ca_carrito_new, false, errors);
            } else {
              build_cart(ca_carrito, false, [resp.message.description]);
            }
          } else {
            build_cart(ca_carrito, false, []);
          }
        });
    }
  );
  $(".cart-sidenav__content").on("click", ".add_coupon-toggle", function(e) {
    e.preventDefault();
    $(".add_coupon-grid").toggle();
    var chevron = $(".add_coupon-icon").attr("uk-icon");
    $(".add_coupon-icon").attr(
      "uk-icon",
      chevron === "chevron-down" ? "chevron-up" : "chevron-down"
    );
  });
  $(".cart-sidenav__content").on("submit", "#add_coupon-form", function(e) {
    e.preventDefault();
    $(".cart-sidenav__loader").show();
    if (validate_field($("#add_coupon-coupon"))) {
      var discount_code = $("#add_coupon-coupon").val();
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/cart/add-discount",
        dataType: "JSON",
        method: "POST",
        data: {
          discount_code: discount_code
        }
      })
        .done(function(resp) {
          var errors = resp.message.description;
          var ca_carrito_new = resp.data;
          build_cart(ca_carrito_new, false, errors);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          if (resp && resp.message) {
            var code = resp.message.code;
            if (code) {
              var errors = resp.message.description;
              var ca_carrito_new = resp.data;
              build_cart(ca_carrito_new, false, errors);
            } else {
              build_cart(ca_carrito, false, [resp.message.description]);
            }
          } else {
            build_cart(ca_carrito, false, []);
          }
        });
    } else {
      $(".cart-sidenav__loader").hide();
    }
  });
  $(".cart-sidenav__content").on("click", ".remove_coupon-btn", function(e) {
    e.preventDefault();
    $(".cart-sidenav__loader").show();
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": csrf_token
      },
      url: "/v4/cart/remove-discount",
      dataType: "JSON",
      method: "POST"
    })
      .done(function(resp) {
        var errors = resp.message.description;
        var ca_carrito_new = resp.data;
        build_cart(ca_carrito_new, false, errors);
      })
      .fail(function(err) {
        var resp = err.responseJSON;
        if (resp && resp.message) {
          var code = resp.message.code;
          if (code) {
            var errors = resp.message.description;
            var ca_carrito_new = resp.data;
            build_cart(ca_carrito_new, false, errors);
          } else {
            build_cart(ca_carrito, false, [resp.message.description]);
          }
        } else {
          build_cart(ca_carrito, false, []);
        }
      });
  });
  $(".cart-sidenav__content").on("click", ".search_shipments-toggle", function(
    e
  ) {
    e.preventDefault();
    $(".search_shipments-grid").toggle();
    var chevron = $(".search_shipments-icon").attr("uk-icon");
    $(".search_shipments-icon").attr(
      "uk-icon",
      chevron === "chevron-down" ? "chevron-up" : "chevron-down"
    );
  });
  $(".cart-sidenav__content").on("submit", "#search_shipments-form", function(
    e
  ) {
    e.preventDefault();
    $(".cart-sidenav__loader").show();
    var zip_code = parseInt($("#search_shipments-zip_code").val());
    if (zip_code >= 1000 && zip_code <= 9421) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/cart/shipment-methods",
        dataType: "JSON",
        method: "GET",
        data: {
          zip_code: zip_code
        }
      })
        .done(function(resp) {
          var errors = resp.message.description;
          var ca_carrito_new = resp.data;
          build_cart(ca_carrito_new, false, errors);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          if (resp && resp.message) {
            var code = resp.message.code;
            if (code) {
              var errors = resp.message.description;
              var ca_carrito_new = resp.data;
              build_cart(ca_carrito_new, false, errors);
            } else {
              build_cart(ca_carrito, false, [resp.message.description]);
            }
          } else {
            build_cart(ca_carrito, false, []);
          }
        });
    } else {
      $(".cart-sidenav__loader").hide();
      invalidate_field(
        $("#search_shipments-zip_code"),
        fields.zip_code.error_message
      );
    }
  });
  $(".cart-sidenav__content").on("click", ".change_shipment-btn", function(e) {
    e.preventDefault();
    ca_carrito_enable_cp_edit = true;
    build_cart(ca_carrito, false, []);
    $(".search_shipments-grid").show();
    $(".search_shipments-icon").attr("uk-icon", "chevron-up");
  });
  $(".cart-sidenav__content").on("click", ".shipment_option_radio", function() {
    var envio_seleccionado = parseInt(
      $("input[name=shipment_option_radio]:checked").val()
    );
    ca_carrito.envio.envio_seleccionado = envio_seleccionado;
    build_cart(ca_carrito, false, []);
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": csrf_token
      },
      url: "/v4/cart/change-shipment-option",
      dataType: "JSON",
      method: "POST",
      data: {
        shipment_option: envio_seleccionado
      }
    })
      .done(function(resp) {})
      .fail(function(err) {});
  });
  $(".cart-sidenav__content").on(
    "click",
    ".cart-sidenav__shipment-result-item-show-more-link",
    function(e) {
      e.preventDefault();
      var key = $(this).attr("data-key");
      var envio = ca_carrito.envio;
      var envio_seleccionado = envio.envio_seleccionado;
      var envios_format_obj = sort_shipping_cart_options(
        envio.envios_disponibles,
        envio.envio_seleccionado
      );
      var envios_format = envios_format_obj.options;
      $(".cart-sidenav__shipment-result-item-show-more").hide();
      var newHtml = "";
      envios_format[key].others.forEach(function(option) {
        newHtml += build_single_shipping_cart_option(
          option,
          envio_seleccionado
        );
      });
      $(".cart-sidenav__shipment-result-list--" + key).append(newHtml);
    }
  );
  $(".cart-sidenav__content").on("click", "#start_checkout-btn", function() {
    set_loading_button($("#start_checkout-btn"));
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": csrf_token
      },
      url: "/v4/cart/start-checkout",
      dataType: "JSON",
      method: "POST"
    })
      .done(function(resp) {
        redirect("/checkout");
        unset_loading_button($("#start_checkout-btn"), false);
      })
      .fail(function(err) {
        var resp = err.responseJSON;
        if (resp && resp.message) {
          var code = resp.message.code;
          if (code) {
            var errors = resp.message.description;
            var ca_carrito_new = resp.data;
            build_cart(ca_carrito_new, false, errors);
          } else {
            build_cart(ca_carrito, false, [resp.message.description]);
          }
        } else {
          build_cart(ca_carrito, false, []);
        }
        unset_loading_button($("#start_checkout-btn"), false);
      });
  });
  //end cart

  //login
  $("#login-modal").on({
    "show.uk.modal": function() {
      if (login_captcha === null) {
        login_captcha = grecaptcha.render("login-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#login-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(login_captcha);
          }
        });
      } else {
        grecaptcha.reset(login_captcha);
      }
    }
  });
  $("#login-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#login-btn"));
    $("#login-alert").html("");
    if (validate_form($("#login-form"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/login",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          redirect(window.location.href);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          grecaptcha.reset(login_captcha);
          if (resp && resp.message) {
            error($("#login-alert"), resp.message.description);
          }
          unset_loading_button($("#login-btn"), true);
        });
    } else {
      grecaptcha.reset(login_captcha);
      unset_loading_button($("#login-btn"), true);
    }
  });
  //end login

  //register
  $("#register-modal").on({
    "show.uk.modal": function() {
      if (register_captcha === null) {
        register_captcha = grecaptcha.render("register-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#register-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(register_captcha);
          }
        });
      } else {
        grecaptcha.reset(register_captcha);
      }
    }
  });
  $("#register-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#register-btn"));
    $("#register-alert").html("");
    if (validate_form($("#register-form"))) {
      if (
        compare_fields($("#register_password"), $("#register_repeat_password"))
      ) {
        $.ajax({
          headers: {
            "X-CSRF-TOKEN": csrf_token
          },
          url: "/v4/register",
          dataType: "JSON",
          method: "POST",
          data: new FormData($(this)[0]),
          cache: false,
          contentType: false,
          processData: false
        })
          .done(function(resp) {
            redirect(window.location.href);
          })
          .fail(function(err) {
            var resp = err.responseJSON;
            grecaptcha.reset(register_captcha);
            if (resp && resp.message) {
              error($("#register-alert"), resp.message.description);
            }
            unset_loading_button($("#register-btn"), true);
          });
      } else {
        grecaptcha.reset(register_captcha);
        unset_loading_button($("#register-btn"), true);
        invalidate_field(
          $("#register_repeat_password"),
          fields.repeat_password.error_message_repeat
        );
      }
    } else {
      grecaptcha.reset(register_captcha);
      unset_loading_button($("#register-btn"), true);
    }
  });
  //end register

  //wholesaler
  $("#wholesaler-modal").on({
    "show.uk.modal": function() {
      if (wholesaler_captcha === null) {
        wholesaler_captcha = grecaptcha.render("wholesaler-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#wholesaler-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(wholesaler_captcha);
          }
        });
      } else {
        grecaptcha.reset(wholesaler_captcha);
      }
    }
  });
  $("#wholesaler-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#wholesaler-btn"));
    $("#wholesaler-alert").html("");
    if (validate_form($("#wholesaler-form"))) {
      if (
        compare_fields(
          $("#wholesaler_password"),
          $("#wholesaler_repeat_password")
        )
      ) {
        $.ajax({
          headers: {
            "X-CSRF-TOKEN": csrf_token
          },
          url: "/v4/wholesaler",
          dataType: "JSON",
          method: "POST",
          data: new FormData($(this)[0]),
          cache: false,
          contentType: false,
          processData: false
        })
          .done(function(resp) {
            $("#wholesaler-modal-content").remove();
            $("#wholesaler-modal-footer").remove();
            success($("#wholesaler-alert"), resp.message.description);
          })
          .fail(function(err) {
            var resp = err.responseJSON;
            grecaptcha.reset(wholesaler_captcha);
            if (resp && resp.message) {
              error($("#wholesaler-alert"), resp.message.description);
            }
            unset_loading_button($("#wholesaler-btn"), true);
          });
      } else {
        grecaptcha.reset(wholesaler_captcha);
        unset_loading_button($("#wholesaler-btn"), true);
        invalidate_field(
          $("#wholesaler_repeat_password"),
          fields.repeat_password.error_message_repeat
        );
      }
    } else {
      grecaptcha.reset(wholesaler_captcha);
      unset_loading_button($("#wholesaler-btn"), true);
    }
  });
  //end wholesaler

  //recover
  $("#recover-modal").on({
    "show.uk.modal": function() {
      if (recover_captcha === null) {
        recover_captcha = grecaptcha.render("recover-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#recover-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(recover_captcha);
          }
        });
      } else {
        grecaptcha.reset(recover_captcha);
      }
    }
  });
  $("#recover-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#recover-btn"));
    $("#recover-alert").html("");
    if (validate_form($("#recover-form"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/recover",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          $("#recover-modal-content").remove();
          $("#recover-modal-footer").remove();
          success($("#recover-alert"), resp.message.description);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          grecaptcha.reset(recover_captcha);
          if (resp && resp.message) {
            error($("#recover-alert"), resp.message.description);
          }
          unset_loading_button($("#recover-btn"), true);
        });
    } else {
      grecaptcha.reset(recover_captcha);
      unset_loading_button($("#recover-btn"), true);
    }
  });
  //end recover

  //newsletter
  $("#newsletter_email").on("focus", function() {
    if (newsletter_captcha === null) {
      newsletter_captcha = grecaptcha.render("newsletter-btn", {
        sitekey: sitekey,
        callback: function() {
          $("#newsletter-form").submit();
        },
        "error-callback": function() {
          grecaptcha.reset(newsletter_captcha);
        }
      });
    } else {
      grecaptcha.reset(newsletter_captcha);
    }
  });
  $("#newsletter-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#newsletter-btn"));
    if (validate_form($("#newsletter-form"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/newsletter",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          success($("#newsletter-form"), resp.message.description);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          grecaptcha.reset(newsletter_captcha);
          if (resp && resp.message) {
            invalidate_field($("#newsletter_email"), resp.message.description);
          }
          unset_loading_button($("#newsletter-btn"), true);
        });
    } else {
      grecaptcha.reset(newsletter_captcha);
      unset_loading_button($("#newsletter-btn"), true);
    }
  });
  //end newsletter

  //contact
  $("#contact-modal").on({
    "show.uk.modal": function() {
      if (contact_captcha === null) {
        contact_captcha = grecaptcha.render("contact-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#contact-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(contact_captcha);
          }
        });
      } else {
        grecaptcha.reset(contact_captcha);
      }
    },
    "hidden.uk.modal": function() {
      $("#contact_product_id").val(0);
      $("#contact_stk_id").val(0);
      $("#contact__show-product").html("");
    }
  });
  $("#contact-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#contact-btn"));
    $("#contact-alert").html("");
    if (validate_form($("#contact-form"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/contact",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          success($("#contact-modal-content"), resp.message.description);
          $("#contact-modal-footer").remove();
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          grecaptcha.reset(contact_captcha);
          if (resp && resp.message) {
            error($("#contact-alert"), resp.message.description);
          }
          unset_loading_button($("#contact-btn"), true);
        });
    } else {
      grecaptcha.reset(contact_captcha);
      unset_loading_button($("#contact-btn"), true);
    }
  });
  //end contact

  //regret
  $("#regret-modal").on({
    "show.uk.modal": function() {
      if (regret_captcha === null) {
        regret_captcha = grecaptcha.render("regret-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#regret-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(regret_captcha);
          }
        });
      } else {
        grecaptcha.reset(regret_captcha);
      }
    }
  });
  $("#regret-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#regret-btn"));
    $("#regret-alert").html("");
    if (validate_form($("#regret-form"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/regret",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          success($("#regret-modal-content"), resp.message.description);
          $("#regret-modal-footer").remove();
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          console.log(resp);
          grecaptcha.reset(regret_captcha);
          if (resp && resp.message) {
            error($("#regret-alert"), resp.message.description);
          }
          unset_loading_button($("#regret-btn"), true);
        });
    } else {
      grecaptcha.reset(regret_captcha);
      unset_loading_button($("#regret-btn"), true);
    }
  });
  //end regret

  //search bar
  searchPush();
  $(".header-search__input").devbridgeAutocomplete({
    serviceUrl: "/v4/search-product",
    type: "GET",
    deferRequestBy: 300,
    triggerSelectOnValidInput: false,
    formatResult: function(suggestion) {
      var producto = suggestion.data;
      var searchHtml = '<div class="autocomplete-suggestion__wrapper">';
      searchHtml += '<div class="autocomplete-suggestion__image-box">';
      searchHtml +=
        '<img src="' +
        cdn +
        producto.imagenes[0].i_link +
        '" class="autocomplete-suggestion__image" alt="producto"/>';
      searchHtml += "</div>";
      searchHtml += '<div class="autocomplete-suggestion__product-container">';
      searchHtml +=
        '<p class="autocomplete-suggestion__product-name">' +
        producto.p_nombre +
        "</p>";
      if (estado_mayorista) {
        searchHtml +=
          '<p class="autocomplete-suggestion__product-price">' +
          number_format(producto.p_precio_mayorista) +
          " - Min. " +
          producto.p_cantidad_minima +
          "</p>";
      } else {
        if (producto.p_mostrar_precio) {
          if (producto.p_oferta) {
            switch (producto.p_oferta) {
              case 1:
                searchHtml +=
                  '<p class="autocomplete-suggestion__product-price"><del>' +
                  number_format(producto.p_precio) +
                  "</del> " +
                  number_format(producto.p_precio_oferta) +
                  "</p>";
                break;
              case 2:
                if (compare_dates("now", producto.p_oferta_fecha)) {
                  searchHtml +=
                    '<p class="autocomplete-suggestion__product-price"><del>' +
                    number_format(producto.p_precio) +
                    "</del> " +
                    number_format(producto.p_precio_oferta) +
                    "</p>";
                } else {
                  searchHtml +=
                    '<p class="autocomplete-suggestion__product-price">' +
                    number_format(producto.p_precio) +
                    "</p>";
                }
                break;
              case 3:
                if (
                  compare_dates_between(
                    "now",
                    producto.p_oferta_fecha_inicio,
                    producto.p_oferta_fecha
                  )
                ) {
                  searchHtml +=
                    '<p class="autocomplete-suggestion__product-price"><del>' +
                    number_format(producto.p_precio) +
                    "</del> " +
                    number_format(producto.p_precio_oferta) +
                    "</p>";
                } else {
                  searchHtml +=
                    '<p class="autocomplete-suggestion__product-price">' +
                    number_format(producto.p_precio) +
                    "</p>";
                }
                break;
            }
          } else {
            searchHtml +=
              '<p class="autocomplete-suggestion__product-price">' +
              number_format(producto.p_precio) +
              "</p>";
          }
        }
      }
      searchHtml += "</div>";
      searchHtml += "</div>";
      return searchHtml;
    },
    onSelect: function(suggestion) {
      var producto = suggestion.data;
      var c_link_full = get_category_link(
        producto.Categorias_idCategorias,
        categorias_flatten
      );
      var link_producto = uri + c_link_full + "/" + producto.p_link;
      redirect(link_producto);
    },
    showNoSuggestionNotice: true,
    noSuggestionNotice:
      '<p class="autocomplete-suggestion__no-results">' +
      search_autocomplete_not_found +
      "</p>"
  });
  $(".search-bar-open").on("click", function(e) {
    e.preventDefault();
    $(".header-search").addClass("header-search--visible");
    $(".header-search__input").focus();
  });
  $(".search-bar-close").on("click", function(e) {
    e.preventDefault();
    $(".header-search").removeClass("header-search--visible");
  });
  $(".header-search__input").on("keyup", function(e) {
    if (e.keyCode === 27) {
      $(".header-search").removeClass("header-search--visible");
    }
  });
  //end search bar

  //chat bubble
  $(".chat__bubble--dispatcher").on("click", function(e) {
    $(".chat-bubbles__list").toggleClass("chat-bubbles__list--active");
    $(".chat-bubbles__list-item").toggleClass(
      "chat-bubbles__list-item--active"
    );
  });
  //end chat bubble
});

var stock_elegido;
var shipping_options;

function hay_compatibilidad(arr1, arr2) {
  var flag = false;
  arr1.forEach(function(n1) {
    if (in_array(n1, arr2)) {
      flag = true;
    }
  });
  return flag;
}
function intersec(arr1, arr2) {
  var intersection = [];
  arr1.forEach(function(n1) {
    if (in_array(n1, arr2)) {
      intersection.push(n1);
    }
  });
  return intersection;
}

function build_single_shipping_option(option) {
  var price_obj = format_shipment_price(option);
  var days_obj = format_shipment_days(option);
  var image =
    "https://dk0k1i3js6c49.cloudfront.net/iconos-envio/costo-envio/" +
    option.icono;

  var newHtml = '<li class="product-vip_shipment-result-list-item">';
  newHtml += '<div class="uk-flex uk-flex-middle">';
  newHtml +=
    '<div class="product-vip__shipment-result-item-image-wrapper"><img src="' +
    image +
    '" class="product-vip__shipment-result-item-image"></div>';
  newHtml += '<div class="product-vip__shipment-result-item-info-wrapper">';
  newHtml +=
    '<p class="product-vip__shipment-result-item-info-title text--primary">' +
    option.nombre +
    "</p>";
  if (price_obj.label) {
    newHtml +=
      '<p class="product-vip__shipment-result-item-info-price text--primary">';
    newHtml +=
      '<span class="product-vip__shipment-result-item-info-price-wrapper">' +
      price_obj.label +
      "</span>";
    if (days_obj.label) {
      newHtml += " - " + days_obj.label;
      newHtml +=
        ' <span class="product-vip__shipment-result-item-info-after text--primary">(' +
        days_obj.after +
        ")</span>";
    }
    newHtml += "</p>";
  }
  if (option.descripcion.length) {
    newHtml +=
      '<p class="product-vip__shipment-result-item-info-description">' +
      option.descripcion.join(" - ") +
      "</p>";
  }
  newHtml += "</div>";
  newHtml += "</div>";
  newHtml += "</li>";
  return newHtml;
}
function build_shipping_options(key) {
  var newHtml = "";
  if (shipping_options[key].featured.length) {
    newHtml += "<div>";
    newHtml +=
      '<p class="product-vip__shipment-result-group-title text--primary">';
    newHtml += product_common["shipment_" + key];
    newHtml += "</p>";
    newHtml +=
      '<ul class="product-vip_shipment-result-list product-vip_shipment-result-list--' +
      key +
      ' border-radius">';

    shipping_options[key].featured.forEach(function(option) {
      newHtml += build_single_shipping_option(option);
    });
    newHtml += "</ul>";

    if (shipping_options[key].others.length) {
      newHtml +=
        '<p class="product-vip__shipment-result-item-show-more text--primary"><a href="#" class="product-vip__shipment-result-item-show-more-link" data-key="' +
        key +
        '">' +
        product_common.shipment_office_show_more +
        ' <span uk-icon="chevron-down"></span></a></p>';
    }

    newHtml += "</div>";
  }
  return newHtml;
}

function get_mobbex_installments(callback) {
  if (medios_pago.mobbex) {
    $.ajax({
      url:
        "https://api.mobbex.com/p/sources/list/arg/" + medios_pago.mobbex_cuit,
      dataType: "JSON",
      method: "GET",
      data: { total: s_producto.precio }
    })
      .done(function(resp) {
        if (resp.result) {
          var data = resp.data;
          var installments = data.map(function(item) {
            var payments = [];

            if (item.installments.enabled) {
              payments = item.installments.list.map(function(i_item) {
                return {
                  name: i_item.name,
                  installment: i_item.totals.installment,
                  total: i_item.totals.total
                };
              });
            } else {
              payments = [
                {
                  name: "1 Pago",
                  installment: {
                    amount: s_producto.precio,
                    count: 1
                  },
                  total: s_producto.precio
                }
              ];
            }

            return {
              name: item.source.name,
              icon:
                "https://res.mobbex.com/images/sources/" +
                item.source.reference +
                ".png",
              id: item.source.reference,
              payments: payments
            };
          });
          return callback(false, installments);
        } else {
          return callback(true, null);
        }
      })
      .fail(function(err) {
        return callback(true, null);
      });
  } else {
    return callback(false, null);
  }
}
function get_mercadopago_installments(callback) {
  if (medios_pago.mercadopago) {
    var installments = {
      "3": {
        setup: false,
        free: []
      },
      "6": {
        setup: false,
        free: []
      },
      "9": {
        setup: false,
        free: []
      },
      "12": {
        setup: false,
        free: []
      },
      "18": {
        setup: false,
        free: []
      }
    };

    $.ajax({
      url:
        "https://www.mercadopago.com/mla/credit_card_promos.json?marketplace=NONE",
      dataType: "JSONP"
    })
      .done(function(resp) {
        if (resp[0] === 200) {
          var response_a = resp[2];
          response_a.forEach(function(item) {
            var cuotas = parseInt(item.max_installments);
            if (cuotas > 1) {
              installments[cuotas.toString()].free.push({
                icon:
                  "https://dk0k1i3js6c49.cloudfront.net/iconos-promociones/issuer-" +
                  item.issuer.id +
                  ".png"
              });
            }
          });

          Mercadopago.setPublishableKey(medios_pago.mercadopago_public_key);
          Mercadopago.getInstallments(
            {
              bin: "480543",
              amount: s_producto.precio
            },
            function(status, response_b) {
              if (status === 200) {
                if (response_b.length) {
                  response_b[0].payer_costs.forEach(function(item) {
                    if (item.installment_rate === 0 && item.installments > 1) {
                      installments[item.installments.toString()].setup = true;
                    }
                  });

                  return callback(false, installments);
                } else {
                  return callback(true, null);
                }
              } else {
                return callback(true, null);
              }
            }
          );
        } else {
          return callback(true, null);
        }
      })
      .fail(function(err) {
        return callback(true, null);
      });
  } else {
    return callback(false, null);
  }
}

function build_mercadopago_installments(data) {
  if (data !== null) {
    var newHtml = '<div class="uk-child-width-1-1 uk-grid-small" uk-grid>';
    newHtml += "<div>";
    newHtml +=
      '<p class="promotions__title"><strong>Tarjetas de crédito</strong></p>';
    newHtml += '<ul class="promotions__list border-radius">';
    ["18", "12", "9", "6", "3"].forEach(function(installment) {
      if (data[installment].setup || data[installment].free.length) {
        newHtml += '<li class="promotions__list-item">';
        newHtml += '<div class="promotions__promotion-head">';
        newHtml +=
          '<p class="promotions__installments"><strong>' +
          installment +
          "</strong> cuotas sin interés de <strong>" +
          number_format(s_producto.precio / parseInt(installment)) +
          "</strong></p>";
        newHtml +=
          '<p class="promotions__detail">Total: <strong>' +
          number_format(s_producto.precio) +
          "</strong> - Precio en 1 pago: <strong>" +
          number_format(s_producto.precio) +
          "</strong></p>";
        newHtml +=
          '<p class="promotions__cft">C.F.T: <strong>0.00%</strong></p>';
        newHtml += "</div>";
        newHtml += '<div class="promotions__promotion-icons">';
        if (data[installment].setup) {
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/visa.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/mastercard.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/american-express.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cabal.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/tarjeta-shopping.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/nativa.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/argencard.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/diners-club.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cordobesa.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/mercadopago-patagonia.png" class="promotions__promotion-icon border-radius"/>';
          newHtml +=
            '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/tarjeta-walmart.png" class="promotions__promotion-icon border-radius"/>';
        } else {
          data[installment].free.forEach(function(icon) {
            newHtml +=
              '<img src="' +
              icon.icon +
              '" class="promotions__promotion-icon border-radius"/>';
          });
        }
        newHtml += "</div>";
        newHtml += "</li>";
      }
    });
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-head">';
    newHtml += '<p class="promotions__installments">Hasta 18 cuotas</p>';
    newHtml +=
      '<p class="promotions__detail">O 1 pago de: <strong>' +
      number_format(s_producto.precio) +
      "</strong></p>";
    newHtml += "</div>";
    newHtml += '<div class="promotions__promotion-icons">';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/visa.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/mastercard.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/american-express.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cabal.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/naranja.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/tarjeta-shopping.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/nativa.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/argencard.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/diners-club.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cordobesa.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/mercadopago-patagonia.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/tarjeta-walmart.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cmr-falabella.png" class="promotions__promotion-icon border-radius"/>';
    newHtml += "</div>";
    newHtml += '<div class="uk-grid-collapse" uk-grid>';
    newHtml += '<div class="uk-width-1-1">';
    newHtml +=
      '<h3 class="promotions__get-installments-title">Calculá la financiación de tu tarjeta</h3>';
    newHtml +=
      '<p class="promotions__get-installments-subtitle">Ingresa los primeros 6 números de tu tarjeta</p>';
    newHtml += "</div>";
    newHtml += '<div class="uk-width-2-3@s uk-width-1-2">';
    newHtml += input(
      "promotions__bin",
      "text",
      "",
      fields.number.regex,
      "Por favor, ingresa los primeros 6 numeros de tu tarjeta",
      "XXXXXX",
      "",
      true,
      "field__input--right-button"
    );
    newHtml += "</div>";
    newHtml += '<div class="uk-width-1-3@s uk-width-1-2">';
    newHtml += button(
      "promotions__get-installments-button",
      "CALCULAR",
      "input",
      "submit",
      false,
      false,
      true,
      "full",
      "",
      "uk-button-input--no-radius"
    );
    newHtml += "</div>";
    newHtml +=
      '<div class="uk-width-1-1" id="promotions__get-installments-container">';
    newHtml += "</div>";
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";
    newHtml += "<div>";
    newHtml +=
      '<p class="promotions__title"><strong>Tarjetas de débito</strong></p>';
    newHtml += '<ul class="promotions__list border-radius">';
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-head">';
    newHtml +=
      '<p class="promotions__detail">Precio: <strong>' +
      number_format(s_producto.precio) +
      "</strong></p>";
    newHtml += "</div>";
    newHtml += '<div class="promotions__promotion-icons">';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/visa-debito.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/mastercard-debito.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/maestro.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cabal-debito.png" class="promotions__promotion-icon border-radius"/>';
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";
    newHtml += "<div>";
    newHtml +=
      '<p class="promotions__title"><strong>Efectivo o transferencia</strong></p>';
    newHtml += '<ul class="promotions__list border-radius">';
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-head">';
    newHtml +=
      '<p class="promotions__detail">Precio: <strong>' +
      number_format(s_producto.precio) +
      "</strong></p>";
    newHtml += "</div>";
    newHtml += '<div class="promotions__promotion-icons">';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/pagofacil.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/rapipago.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cobroexpress.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/carga-virtual.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/link.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/provincia-pagos.png" class="promotions__promotion-icon border-radius"/>';
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";

    newHtml += "</div>";
    $("#promotions__mercadopago").html(newHtml);
  }
}
function build_mobbex_installments(data) {
  if (data !== null) {
    var newHtml = '<div class="uk-child-width-1-1 uk-grid-small" uk-grid>';
    data.forEach(function(item) {
      newHtml += "<div>";
      newHtml += '<p class="promotions__title">';
      newHtml +=
        '<img src="' + item.icon + '" class="promotions__title-icon"/>';
      if (item.name === "Mastercard Debit") {
        newHtml += "<strong>Mastercard Débito</strong>";
      } else if (item.name === "Rapipago/PagoFácil") {
        newHtml += "<strong>Rapipago / Pago Fácil</strong>";
      } else {
        newHtml += "<strong>" + item.name + "</strong>";
      }
      newHtml += "</p>";
      newHtml += '<ul class="promotions__list border-radius">';
      item.payments.forEach(function(item_payments) {
        newHtml += '<li class="promotions__list-item">';
        newHtml += '<div class="promotions__promotion-head">';
        if (
          item_payments.name !== "1 Pago" &&
          item_payments.name !== "Debito"
        ) {
          newHtml +=
            '<p class="promotions__other"><strong>' +
            item_payments.name +
            "</strong></p>";
        }
        if (item_payments.installment.count === 1) {
          newHtml +=
            '<p class="promotions__detail">Precio en 1 pago: <strong>' +
            number_format(s_producto.precio) +
            "</strong></p>";
        } else {
          newHtml +=
            '<p class="promotions__installments"><strong>' +
            item_payments.installment.count +
            "</strong> cuotas de <strong>" +
            number_format(item_payments.installment.amount) +
            "</strong></p>";
          newHtml +=
            '<p class="promotions__detail">Total: <strong>' +
            number_format(item_payments.total) +
            "</strong> - Precio en 1 pago: <strong>" +
            number_format(s_producto.precio) +
            "</strong></p>";
        }
        newHtml += "</div>";
        newHtml += "</li>";
      });
      newHtml += "</ul>";
      newHtml += "</div>";
    });
    newHtml += "</div>";
    $("#promotions__mobbex").html(newHtml);
  }
}
function build_todopago() {
  if (medios_pago.todopago) {
    var newHtml = '<div class="uk-child-width-1-1 uk-grid-small" uk-grid>';
    newHtml += "<div>";
    newHtml +=
      '<p class="promotions__title"><strong>Tarjetas de crédito</strong></p>';
    newHtml += '<ul class="promotions__list border-radius">';
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-icons">';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/visa.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/mastercard.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/american-express.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/naranja.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cabal.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/diners-club.png" class="promotions__promotion-icon border-radius"/>';
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";
    newHtml += "<div>";
    newHtml +=
      '<p class="promotions__title"><strong>Tarjetas de débito</strong></p>';
    newHtml += '<ul class="promotions__list border-radius">';
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-icons">';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/visa-debito.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/mastercard-debito.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/maestro.png" class="promotions__promotion-icon border-radius"/>';
    newHtml +=
      '<img src="https://dk0k1i3js6c49.cloudfront.net/iconos-pago/cabal-debito.png" class="promotions__promotion-icon border-radius"/>';
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";
    newHtml += "</div>";
    $("#promotions__todopago").html(newHtml);
  }
}
function build_transfer() {
  if (medios_pago.transferencia) {
    var newHtml = '<div class="uk-child-width-1-1 uk-grid-small" uk-grid>';
    newHtml += "<div>";
    newHtml +=
      '<p class="promotions__title"><strong>Transferencia bancaria</strong></p>';
    newHtml +=
      '<p class="promotions__subtitle">Al finalizar la compra te va a llegar un email con los datos bancarios para realizar la transferencia</p>';
    newHtml += '<ul class="promotions__list border-radius">';
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-head">';
    if (medios_pago.transferencia_descuento) {
      newHtml +=
        '<p class="promotions__detail"><strong>' +
        medios_pago.transferencia_porcentaje +
        "%</strong> de descuento pagando con transferencia bancaria</p>";
      newHtml +=
        '<p class="promotions__detail">Total: <strong><del style="color: #555;">' +
        number_format(s_producto.precio) +
        "</del> " +
        number_format(
          sub_percent_amount(
            s_producto.precio,
            medios_pago.transferencia_porcentaje
          )
        ) +
        "</strong></p>";
    } else {
      newHtml +=
        '<p class="promotions__detail">Total: <strong>' +
        number_format(s_producto.precio) +
        "</strong></p>";
    }
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";
    newHtml += "</div>";
    $("#promotions__transfer").html(newHtml);
  }
}
function build_cash() {
  if (medios_pago.efectivo) {
    var newHtml = '<div class="uk-child-width-1-1 uk-grid-small" uk-grid>';
    newHtml += "<div>";
    newHtml += '<p class="promotions__title"><strong>Efectivo</strong></p>';
    newHtml +=
      '<p class="promotions__subtitle">Podés pagar en efectivo retirando personalmente en nuestros puntos de retiro</p>';
    newHtml += '<ul class="promotions__list border-radius">';
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-head">';
    if (medios_pago.efectivo_descuento) {
      newHtml +=
        '<p class="promotions__detail"><strong>' +
        medios_pago.efectivo_porcentaje +
        "%</strong> de descuento pagando con efectivo</p>";
      newHtml +=
        '<p class="promotions__detail">Total: <strong><del style="color: #555;">' +
        number_format(s_producto.precio) +
        "</del> " +
        number_format(
          sub_percent_amount(s_producto.precio, medios_pago.efectivo_porcentaje)
        ) +
        "</strong></p>";
    } else {
      newHtml +=
        '<p class="promotions__detail">Total: <strong>' +
        number_format(s_producto.precio) +
        "</strong></p>";
    }
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";
    newHtml += "</div>";
    $("#promotions__cash").html(newHtml);
  }
}
function build_agree() {
  if (medios_pago.acordar) {
    var newHtml = '<div class="uk-child-width-1-1 uk-grid-small" uk-grid>';
    newHtml += "<div>";
    newHtml += '<p class="promotions__title"><strong>Acordar</strong></p>';
    newHtml +=
      '<p class="promotions__subtitle">Acordamos el medio de pago al finalizar la compra</p>';
    newHtml += '<ul class="promotions__list border-radius">';
    newHtml += '<li class="promotions__list-item">';
    newHtml += '<div class="promotions__promotion-head">';
    newHtml +=
      '<p class="promotions__detail">Total: <strong>' +
      number_format(s_producto.precio) +
      "</strong></p>";
    newHtml += "</div>";
    newHtml += "</li>";
    newHtml += "</ul>";
    newHtml += "</div>";
    newHtml += "</div>";
    $("#promotions__agree").html(newHtml);
  }
}

$(function() {
  //selects
  $(".product-vip__attribute").on("change", function() {
    //obtengo los idstock del item seleccionado habilitados
    var select_fired = $(this);
    var key_selected = parseInt(select_fired.attr("id").split("-")[1]);
    var valor_selected = select_fired.val();
    var idstk_selected = atributos[key_selected].valores[valor_selected];

    //busco los stock compatibles
    var intersection = idstk_selected;
    var j;
    for (j = 0; j < key_selected; j++) {
      intersection = intersec(
        intersection,
        atributos[j].valores[$("#atributos-" + j).val()]
      );
    }

    //debo ver que las demas opciones contemplen lo valores seleccionados
    $(".product-vip__attribute").each(function() {
      var select_next = $(this);
      var key_external = parseInt(select_next.attr("id").split("-")[1]);

      //agarro un select mayor al seleccionado
      if (key_external > key_selected) {
        var valores_select_next = Object.entries(
          atributos[key_external].valores
        );
        var html_to_gen = "";
        var i = null;
        valores_select_next.forEach(function(valor, idx) {
          var vat_valor = valor[0];
          var idstk_external = valor[1];
          if (hay_compatibilidad(intersection, idstk_external)) {
            i = i === null ? idx : i;
            html_to_gen +=
              '<option value="' + vat_valor + '">' + vat_valor + "</option>";
          }
        });
        select_next.empty();
        select_next.html(html_to_gen);
        intersection = intersec(intersection, valores_select_next[i][1]);
      }
    });

    //actualizo el stock seleccionado y tomo el stock elegido
    idStock_seleccionado = intersection[0];
    stock_elegido = stock.filter(function(item_stock) {
      return item_stock.idStock === idStock_seleccionado;
    })[0];
    $("input[name=stock]").val(idStock_seleccionado);

    //cambio imagen de ser necesario
    var img_key = 0;
    imagenes.forEach(function(imagen, position) {
      if (imagen.idImagenes === stock_elegido.s_imagen) {
        coimagenp = cdn + imagen.i_link;
        img_key = position;
      }
    });
    if (products_detail.photo_display === 0) {
      //carrousel
      UIkit.slider("#product-vip__carrousel").show(img_key);
    } else {
      //grilla
      $(".product-vip__images-grid-full-image").attr(
        "src",
        cdn + imagenes[img_key].i_link
      );
      $(".product-vip__images-grid-full-image-lightbox").attr(
        "href",
        cdn + imagenes[img_key].i_link
      );
    }

    //cambio precio de ser necesario
    s_producto = product_get_params(
      {
        p_precio: stock_elegido.s_precio,
        p_precio_oferta: stock_elegido.s_precio_oferta,
        p_precio_mayorista: stock_elegido.s_precio_mayorista,
        p_mostrar_precio: stock_elegido.s_mostrar_precio,
        p_oferta: stock_elegido.s_oferta,
        p_oferta_fecha: stock_elegido.s_oferta_fecha,
        p_oferta_fecha_inicio: stock_elegido.s_oferta_fecha_inicio,
        stock: []
      },
      estado_mayorista
    );

    if (stock_elegido.s_mostrar_precio || estado_mayorista) {
      var producto_precio_html = '<span class="product-vip__price-value">';
      if (s_producto.precio_anterior) {
        producto_precio_html +=
          "<del>" + number_format(s_producto.precio_anterior) + "</del> ";
      }
      producto_precio_html += number_format(s_producto.precio);
      producto_precio_html += "</span>";
      if (s_producto.sale_off) {
        producto_precio_html += '<span class="product-vip__off">';
        producto_precio_html +=
          '<span class="product-vip__off-value">' +
          s_producto.sale_off +
          "</span>% OFF";
        producto_precio_html += "</span>";
      }
      $(".product-vip__price").html(producto_precio_html);

      //cambio las cuotas de ser necesario (si existen)
      if (pago_online && products_feed.product_subtext_type === 1) {
        var producto_cuotas_html = get_parameterized_lang_string(
          product_common.list_installments,
          {
            installments:
              "<strong>" + products_feed.product_subtext_cuotas + "</strong>",
            amount:
              "<strong>" +
              number_format(
                s_producto.precio / products_feed.product_subtext_cuotas
              ) +
              "</strong>"
          }
        );
        $(".product-vip__promo-installments-value").html(producto_cuotas_html);
      }

      //reviso si debo cambiar los mensajes mayoristas
      if (estado_mayorista) {
        var mayorista_mensaje_html = "";
        if (stock_elegido.s_cantidad_minima > 1) {
          mayorista_mensaje_html = get_parameterized_lang_string(
            product_common.wholesale_min_qty_vip_product_extra,
            {
              qty: p_cantidad_minima,
              qty_selected: stock_elegido.s_cantidad_minima
            }
          );
        } else {
          mayorista_mensaje_html = get_parameterized_lang_string(
            product_common.wholesale_min_qty_vip_product,
            {
              qty: p_cantidad_minima
            }
          );
        }
        $(".product-vip__wholesale-min-qty-value").html(mayorista_mensaje_html);
      }
    }

    //cambio el tiempo de produccion de ser necesario
    if (p_producto_digital === 2) {
      var tiempo_produccion_html = get_parameterized_lang_string(
        product_common.production_time,
        {
          time: stock_elegido.s_producto_digital_plazo
        }
      );
      $(".product-vip__production-time-value").html(tiempo_produccion_html);
    }
  });
  if (atributos_cantidad > 1) {
    $("#atributos-0").trigger("change");
  }
  //end selects

  //agregar al carrito
  $("#add_to_cart-form").on("submit", function(e) {
    e.preventDefault();
    $("#add_to_cart-msg").html("");
    set_loading_button($("#add_to_cart-btn"));
    $.ajax({
      headers: {
        "X-CSRF-TOKEN": csrf_token
      },
      url: "/v4/cart/add",
      dataType: "JSON",
      method: "POST",
      data: new FormData($(this)[0]),
      cache: false,
      contentType: false,
      processData: false
    })
      .done(function(resp) {
        var errors = resp.message.description;
        var ca_carrito_new = resp.data;
        build_cart(ca_carrito_new, true, errors);
        unset_loading_button($("#add_to_cart-btn"));
      })
      .fail(function(err) {
        var resp = err.responseJSON;
        switch (resp.message.code) {
          case 0:
            error($("#add_to_cart-msg"), resp.message.description);
            break;
          case 1:
            info($("#add_to_cart-msg"), resp.message.description);
            break;
          case 2:
            invalidate_field($("#quantity"), resp.message.description);
            break;
          case 3:
            var ca_carrito_new = resp.data;
            build_cart(ca_carrito_new, true, resp.message.description);
            break;
        }
        unset_loading_button($("#add_to_cart-btn"));
      });
  });
  //end agregar al carrito

  //image grill
  $(".product-vip__images-grid-list-item").on("click", function() {
    var img_key = parseInt($(this).attr("data-index"));
    $(".product-vip__images-grid-full-image").attr(
      "src",
      cdn + imagenes[img_key].i_link
    );
    $(".product-vip__images-grid-full-image-lightbox").attr(
      "href",
      cdn + imagenes[img_key].i_link
    );
  });
  //end image grill

  //consultar
  $("#contact_product-form").on("submit", function(e) {
    e.preventDefault();
    var idProducto_seleccionado = parseInt($("input[name=product]").val());
    var atributos_labels = [];
    var stock_seleccionado_contactoArr = stock.filter(function(item) {
      return item.idStock === idStock_seleccionado;
    });
    if (stock_seleccionado_contactoArr.length) {
      var stock_seleccionado_contacto = stock_seleccionado_contactoArr[0];
      idStock_seleccionado = stock_seleccionado_contacto.idStock;

      stock_seleccionado_contacto.valoratributo.forEach(function(item) {
        atributos_labels.push({
          label: item.at_nombre,
          value: item.valor.vat_valor
        });
      });
    }
    $("#contact_product_id").val(idProducto_seleccionado);
    $("#contact_stk_id").val(idStock_seleccionado);

    var contact_html = '<div class="uk-grid-medium" uk-grid>';
    contact_html += '<div class="uk-width-1-4">';
    contact_html +=
      '<img src="' + i_link_principal + '" class="product-vip__contact-image">';
    contact_html += "</div>";
    contact_html += '<div class="uk-width-3-4">';
    contact_html +=
      '<h4 class="product-vip__contact-message text--primary uk-h4">' +
      product_common.contact_product_title +
      "</h4>";
    contact_html +=
      '<h5 class="product-vip__contact-title text--primary uk-h5">' +
      p_nombre +
      "</h5>";
    atributos_labels.forEach(function(item) {
      contact_html +=
        '<p class="product-vip__contact-attribute">' +
        item.label +
        ": " +
        item.value +
        "</p>";
    });
    contact_html += "</div>";
    contact_html += "</div>";
    $("#contact__show-product").html(contact_html);

    UIkit.modal("#contact-modal").show();
  });

  //calcular costo envio
  $("#shipment_cost-form").on("submit", function(e) {
    e.preventDefault();
    var zip_code = parseInt($("input[name=shipment_cost-cp]").val());
    if (zip_code >= 1000 && zip_code <= 9421) {
      var qty = parseInt($("input[name=quantity]").val());
      var product = parseInt($("input[name=product]").val());
      var stock = parseInt($("input[name=stock]").val());
      if (qty > 0 && product > 0) {
        set_loading_button($("#shipment_cost-btn"));
        $(".product-vip__shipping-results").html("");

        $.ajax({
          headers: {
            "X-CSRF-TOKEN": csrf_token
          },
          url: "/v4/shipping-cost",
          dataType: "JSON",
          method: "GET",
          timeout: 60000,
          data: {
            product: product,
            stock: stock,
            qty: qty,
            zip_code: zip_code
          }
        })
          .done(function(resp) {
            var options = resp.data;
            var newHtml = "";
            if (options.length) {
              shipping_options = sort_shipping_options(options);
              newHtml += build_shipping_options("domicile");
              newHtml += build_shipping_options("office");
              newHtml += build_shipping_options("other");
              newHtml += build_shipping_options("point");
            }
            $(".product-vip__shipping-results").html(newHtml);
            unset_loading_button($("#shipment_cost-btn"));
          })
          .fail(function(err) {
            var resp = err.responseJSON;
            if (resp && resp.message) {
              invalidate_field(
                $("#shipment_cost-cp"),
                resp.message.description
              );
            }
            unset_loading_button($("#shipment_cost-btn"));
          });
      } else {
        invalidate_field($("#shipment_cost-cp"), fields.zip_code.error_message);
      }
    } else {
      invalidate_field($("#shipment_cost-cp"), fields.zip_code.error_message);
    }
  });
  $(".product-vip__shipping-results").on(
    "click",
    ".product-vip__shipment-result-item-show-more-link",
    function(e) {
      e.preventDefault();
      var key = $(this).attr("data-key");
      $(".product-vip__shipment-result-item-show-more").hide();
      var newHtml = "";
      shipping_options[key].others.forEach(function(option) {
        newHtml += build_single_shipping_option(option);
      });
      $(".product-vip_shipment-result-list--" + key).append(newHtml);
    }
  );
  //end calcular costo envio

  //obtengo los medios de pago
  $(".product-vip__show-payment-offers-link").on("click", function(e) {
    e.preventDefault();
    $(".product-vip__show-payment-offers-loading").html(
      '<i class="fas fa-sync fa-spin"></i>'
    );

    get_mobbex_installments(function(err1, mobbex_data) {
      get_mercadopago_installments(function(err2, mercadopago_data) {
        $(".product-vip__show-payment-offers-loading").html("");

        if (!(err1 || err2)) {
          build_mercadopago_installments(mercadopago_data);
          build_mobbex_installments(mobbex_data);
          build_todopago();
          build_transfer();
          build_cash();
          build_agree();
          UIkit.modal("#promotions-modal").show();
        }
      });
    });
  });
  $("#promotions-form").on("submit", function(e) {
    e.preventDefault();
    if (validate_field($("#promotions__bin"))) {
      var bin = parseInt($("#promotions__bin").val());
      if (bin >= 100000 && bin <= 999999) {
        set_loading_button($("#promotions__get-installments-button"));
        Mercadopago.setPublishableKey(medios_pago.mercadopago_public_key);
        Mercadopago.getInstallments(
          {
            bin: bin,
            amount: s_producto.precio
          },
          function(status, response) {
            if (status === 200) {
              if (response.length) {
                var newHtml = '<ul class="promotions__list border-radius">';
                response[0].payer_costs.forEach(function(item) {
                  newHtml += '<li class="promotions__list-item">';
                  newHtml += '<div class="promotions__promotion-head">';
                  newHtml += '<p class="promotions__installments">';
                  if (item.installments === 1) {
                    newHtml +=
                      "<strong>1</strong> cuota de <strong>" +
                      number_format(item.installment_amount) +
                      "</strong>";
                  } else {
                    newHtml +=
                      "<strong>" +
                      item.installments +
                      "</strong> cuotas de <strong>" +
                      number_format(item.installment_amount) +
                      "</strong>";
                  }
                  newHtml += "</p>";
                  newHtml +=
                    '<p class="promotions__detail">Total: <strong>' +
                    number_format(item.total_amount) +
                    "</strong> - Precio en 1 pago: <strong>" +
                    number_format(s_producto.precio) +
                    "</strong></p>";
                  item.labels.forEach(function(label) {
                    if (label.indexOf("CFT_") !== -1) {
                      var cft_tea_arr = label.split("|");
                      var cft = cft_tea_arr[0].replace("_", ": ");
                      var tea = cft_tea_arr[1].replace("_", ": ");
                      newHtml += '<p class="promotions__cft">' + cft + "</p>";
                      newHtml += '<p class="promotions__cft">' + tea + "</p>";
                    }
                  });
                  newHtml += "</div>";
                  newHtml += "</li>";
                });
                newHtml += "</ul>";
                $("#promotions__get-installments-container").html(newHtml);
                unset_loading_button($("#promotions__get-installments-button"));
              } else {
                unset_loading_button($("#promotions__get-installments-button"));
                invalidate_field(
                  $("#promotions__bin"),
                  "Ha ocurrido un error, intente con otra tarjeta"
                );
              }
            } else {
              unset_loading_button($("#promotions__get-installments-button"));
              invalidate_field(
                $("#promotions__bin"),
                "Ha ocurrido un error, intente con otra tarjeta"
              );
            }
          }
        );
      } else {
        invalidate_field(
          $("#promotions__bin"),
          "Por favor, ingresa los primeros 6 numeros de tu tarjeta"
        );
      }
    }
  });
  //end obtengo los medios de pago
});

/**
 * Required vars:
 *      - attribute_filter
 *      - filter_order
 *      - ids
 */

var params = [];

function get_filter_params() {
  params = [];
  $(".checkbox__check:checked").each(function() {
    var value = $(this).val();
    var filter = $(this).attr("data-filter");
    if (!(filter in params)) {
      params[filter] = [];
    }
    if (!in_array(value, params[filter])) {
      params[filter].push(value);
    }
  });
}

function get_products_category_params(filter_page) {
  var get_params = {
    filter_page: filter_page,
    filter_order: filter_order,
    filter_categories: ids
  };
  if (attribute_filter) {
    Object.keys(params).forEach(function(key) {
      get_params[key] = params[key].join("|");
    });
  }
  return get_params;
}

function add_product_list(productos, page) {
  var html = "";
  productos.forEach(function(producto) {
    html +=
      '<div class="products-feed__product uk-width-1-4@l uk-width-1-3@m uk-width-1-2">';
    html += product_item(producto, "products-feed__product");
    html += "</div>";
  });

  if (page > 0) {
    $(".products-feed__products").append(html);
  } else {
    $(".products-feed__products").html(html);
  }
}

function get_products_category(filter_page) {
  $.ajax({
    beforeSend: function() {
      if (filter_page > 0) {
        set_loading_button($("#products_feed-btn"));
      } else {
        set_loading_spinner($(".products-feed__products"), 1.5);
        $("#products_feed-btn").hide();
      }
    },
    headers: {
      "X-CSRF-TOKEN": csrf_token
    },
    url: "/v4/product/category",
    dataType: "JSON",
    method: "GET",
    data: get_products_category_params(filter_page)
  })
    .done(function(resp) {
      var productos = resp.data;
      var sz = productos.length;

      if (sz > 0) {
        add_product_list(productos, filter_page);
        if (sz === 12) {
          if (filter_page > 0) {
            $("#products_feed-btn").attr("data-page", filter_page + 1);
            unset_loading_button($("#products_feed-btn"), false);
            $("#products_feed-btn").show();
          } else {
            $("#products_feed-btn").attr("data-page", 1);
            unset_loading_button($("#products_feed-btn"), false);
            $("#products_feed-btn").show();
          }
        } else {
          unset_loading_button($("#products_feed-btn"), false);
          $("#products_feed-btn").hide();
        }
      } else {
        unset_loading_button($("#products_feed-btn"), false);
        $("#products_feed-btn").hide();
      }
    })
    .fail(function(err) {
      if (filter_page > 0) {
        unset_loading_button($("#products_feed-btn"), false);
      } else {
        $(".products-feed__products").html("");
      }
    });
}

$(function() {
  if (attribute_filter) get_filter_params();

  $(".checkbox__check").on("change", function() {
    get_filter_params();
    var url = get_url_without_query_strings();
    var query_string = get_filter_query_string(params);
    redirect(url + query_string);
  });

  $(".products-feed__filter-remove-all").on("click", function(e) {
    e.preventDefault();
    var url = get_url_without_query_strings();
    redirect(url);
  });

  $(".products-feed__filter-remove").on("click", function(e) {
    e.preventDefault();
    var attribute = $(this).attr("data-attribute");
    var value = $(this).attr("data-value");
    params[attribute] = params[attribute].filter(function(item) {
      return item !== value;
    });
    var url = get_url_without_query_strings();
    var query_string = get_filter_query_string(params);
    redirect(url + query_string);
  });

  $("#products_feed-btn").on("click", function(e) {
    e.preventDefault();
    var filter_page = parseInt($(this).attr("data-page"));
    get_products_category(filter_page);
  });

  $("#product_order").on("change", function() {
    filter_order = parseInt($(this).val());
    get_products_category(0);
  });
});

var s_producto = {
  sale_off: null,
  precio: "660.00",
  precio_anterior: null,
  stock: true,
  link_producto: ""
};
var p_nombre = "Set Gigante";
var i_link_principal =
  "https://d22fxaf9t8d39k.cloudfront.net/733014a3bc635dbd101b500ca4ffb564ca02617c4a0aba6b18ed3f2f5f29761b36405.jpeg";
var p_producto_digital = 0;
var p_cantidad_minima = 1;
var idStock_seleccionado = 3163952;
var imagenes = [
  {
    idImagenes: 3494049,
    i_link:
      "733014a3bc635dbd101b500ca4ffb564ca02617c4a0aba6b18ed3f2f5f29761b36405.jpeg"
  }
];
var medios_pago = {
  acordar: 1,
  mercadopago: 1,
  mercadopago_public_key: "APP_USR-79e5a249-89ae-41f5-be6e-4ab52e0211c3",
  mobbex: 0,
  mobbex_cuit: "",
  todopago: 0,
  efectivo: 0,
  efectivo_descuento: 0,
  efectivo_porcentaje: 0,
  transferencia: 0,
  transferencia_descuento: 0,
  transferencia_porcentaje: 0
};
var stock = [
  {
    idStock: 3163952,
    s_cantidad: 0,
    s_ilimitado: 1,
    s_precio: "660.00",
    s_oferta: 0,
    s_precio_oferta: null,
    s_oferta_fecha_inicio: null,
    s_oferta_fecha: null,
    s_precio_mayorista: "0.00",
    s_cantidad_minima: 1,
    s_mostrar_precio: 1,
    s_imagen: 3494049,
    s_producto_digital_plazo: "",
    s_producto_digital_observacion: "",
    valoratributo: [
      {
        idAtributos: 681959,
        at_nombre: "Esquema",
        valor: {
          Stock_idStock: 3163952,
          Atributos_idAtributos: 681959,
          vat_valor: "Gigante 1"
        }
      }
    ]
  },
  {
    idStock: 3163953,
    s_cantidad: 0,
    s_ilimitado: 1,
    s_precio: "660.00",
    s_oferta: 0,
    s_precio_oferta: null,
    s_oferta_fecha_inicio: null,
    s_oferta_fecha: null,
    s_precio_mayorista: "0.00",
    s_cantidad_minima: 1,
    s_mostrar_precio: 1,
    s_imagen: 3494049,
    s_producto_digital_plazo: "",
    s_producto_digital_observacion: "",
    valoratributo: [
      {
        idAtributos: 681959,
        at_nombre: "Esquema",
        valor: {
          Stock_idStock: 3163953,
          Atributos_idAtributos: 681959,
          vat_valor: "Gigante 2"
        }
      }
    ]
  },
  {
    idStock: 3163954,
    s_cantidad: 0,
    s_ilimitado: 1,
    s_precio: "660.00",
    s_oferta: 0,
    s_precio_oferta: null,
    s_oferta_fecha_inicio: null,
    s_oferta_fecha: null,
    s_precio_mayorista: "0.00",
    s_cantidad_minima: 1,
    s_mostrar_precio: 1,
    s_imagen: 3494049,
    s_producto_digital_plazo: "",
    s_producto_digital_observacion: "",
    valoratributo: [
      {
        idAtributos: 681959,
        at_nombre: "Esquema",
        valor: {
          Stock_idStock: 3163954,
          Atributos_idAtributos: 681959,
          vat_valor: "Gigante 3"
        }
      }
    ]
  },
  {
    idStock: 3163955,
    s_cantidad: 0,
    s_ilimitado: 1,
    s_precio: "660.00",
    s_oferta: 0,
    s_precio_oferta: null,
    s_oferta_fecha_inicio: null,
    s_oferta_fecha: null,
    s_precio_mayorista: "0.00",
    s_cantidad_minima: 1,
    s_mostrar_precio: 1,
    s_imagen: 3494049,
    s_producto_digital_plazo: "",
    s_producto_digital_observacion: "",
    valoratributo: [
      {
        idAtributos: 681959,
        at_nombre: "Esquema",
        valor: {
          Stock_idStock: 3163955,
          Atributos_idAtributos: 681959,
          vat_valor: "Gigante 4"
        }
      }
    ]
  }
];
var atributos = [
  {
    idAtributos: 681959,
    at_nombre: "Esquema",
    valores: {
      "Gigante 1": [3163952],
      "Gigante 2": [3163953],
      "Gigante 3": [3163954],
      "Gigante 4": [3163955]
    }
  }
];
var atributos_cantidad = 1;
var products_detail = { photo_zoom: 0, photo_display: 1 };

var uri = "https://quieromisello.empretienda.com.ar";
var cart_labels = {
  sidenav_title: "Mi carrito",
  sidenav_empty_cart: "El carrito de compras est\u00e1 vacio",
  sidenav_back_shop: "Volver a la tienda",
  sidenav_subtotal: "Subtotal",
  sidenav_total: "Total",
  sidenav_coupon_add_title: "\u00bfTen\u00e9s un descuento?",
  sidenav_coupon: "Descuento",
  sidenav_coupon_remove_button: "Remover cup\u00f3n",
  sidenav_coupon_button: "Aplicar",
  sidenav_shipment_title: "Calcul\u00e1 el costo de env\u00edo",
  sidenav_shipment_button: "Calcular",
  sidenav_shipment_list_title: "M\u00e9todos de env\u00edo",
  sidenav_shipment: "Env\u00edo",
  sidenav_checkout_start_button: "Iniciar compra",
  sidenav_keep_buying_button: "Seguir comprando",
  sidenav_shipment_list_change_zip_code: "Cambiar c\u00f3digo postal"
};
var open_cart = 0;
var pago_online = 1;
var products_feed = {
  order: 0,
  filter: 0,
  masonry: 0,
  product_subtext: "HASTA 12 CUOTAS",
  product_subtext_type: 0,
  product_subtext_cuotas: 3
};
var product_common = {
  list_installments: ":installments cuotas sin inter\u00e9s de :amount",
  list_transfer: ":discount de descuento pagando por transferencia bancaria",
  list_cash: ":discount de descuento pagando con efectivo",
  list_cash_and_transfer:
    ":discount de descuento pagando por transferencia bancaria \u00f3 efectivo",
  list_cash_and_transfer_not_eq:
    "Descuento pagando por transferencia bancaria \u00f3 efectivo",
  wholesale_min_qty: "Cantidad m\u00ednima :qty",
  wholesale_min_qty_vip_product:
    "La cantidad m\u00ednima de compra mayorista para este producto es :qty.",
  wholesale_min_qty_vip_product_extra:
    "La cantidad m\u00ednima de compra mayorista para este producto es :qty. Adicionalmente, la cantidad m\u00ednima para la opci\u00f3n seleccionada es :qty_selected.",
  out_of_stock: "SIN STOCK",
  production_time: "Plazo de entrega aproximado: :time",
  add_to_cart: "Agregar al carrito",
  contact_product: "Consultar",
  show_payment_offers: "Ver formas de pago",
  contact_product_title: "Consultar por:",
  shipment_cost_title: "Calcul\u00e1 el costo de env\u00edo",
  shipment_cost_button: "Calcular",
  shipment_free_label: "Gratis",
  shipment_pay_on_destination: "Pago en destino",
  shipment_day: ":days d\u00eda h\u00e1bil",
  shipment_days: ":days d\u00edas h\u00e1biles",
  shipment_days_after: "luego de ser despachado",
  shipment_domicile: "Env\u00edo a domicilio",
  shipment_office: "Retirar en sucursal del correo",
  shipment_office_show_more: "Ver todas las sucursales",
  shipment_point: "Retirar en nuestros puntos",
  shipment_other: "Otros medios de env\u00edo"
};
var cupones_descuento = 0;
var alerta_envio = 0;
var alerta_envio_mensaje =
  "Debido a la situación de cuarentena total en el país, los tiempos previstos de entrega pueden sufrir demoras.";
var estado_mayorista = 0;
var categorias_flatten = [
  {
    idCategorias: 349188,
    c_nombre: "Sets de Correcci\u00f3n",
    c_prefix: "-",
    c_link_full: "/sets-de-correccion",
    c_link: "sets-de-correccion",
    c_nombre_full: "Sets de Correcci\u00f3n",
    c_padre: null,
    padres: [],
    hijos: []
  },
  {
    idCategorias: 353773,
    c_nombre: "Emprendedores",
    c_prefix: "-",
    c_link_full: "/emprendedores",
    c_link: "emprendedores",
    c_nombre_full: "Emprendedores",
    c_padre: null,
    padres: [],
    hijos: [354170, 354168, 355186]
  },
  {
    idCategorias: 354170,
    c_nombre: "Sets",
    c_prefix: "Emprendedores",
    c_link_full: "/emprendedores/sets",
    c_link: "sets",
    c_nombre_full: "Emprendedores \u003E Sets",
    c_padre: 353773,
    padres: [353773],
    hijos: []
  },
  {
    idCategorias: 354168,
    c_nombre: "Sellos sueltos",
    c_prefix: "Emprendedores",
    c_link_full: "/emprendedores/sellos-sueltos",
    c_link: "sellos-sueltos",
    c_nombre_full: "Emprendedores \u003E Sellos sueltos",
    c_padre: 353773,
    padres: [353773],
    hijos: []
  },
  {
    idCategorias: 355186,
    c_nombre: "Sellos con tu marca",
    c_prefix: "Emprendedores",
    c_link_full: "/emprendedores/sellos-con-tu-marca",
    c_link: "sellos-con-tu-marca",
    c_nombre_full: "Emprendedores \u003E Sellos con tu marca",
    c_padre: 353773,
    padres: [353773],
    hijos: []
  },
  {
    idCategorias: 355187,
    c_nombre: "Sellos Personales",
    c_prefix: "-",
    c_link_full: "/sellos-personales",
    c_link: "sellos-personales",
    c_nombre_full: "Sellos Personales",
    c_padre: null,
    padres: [],
    hijos: [355188]
  },
  {
    idCategorias: 355188,
    c_nombre: "Sellos autom\u00e1ticos",
    c_prefix: "Sellos Personales",
    c_link_full: "/sellos-personales/sellos-automaticos",
    c_link: "sellos-automaticos",
    c_nombre_full: "Sellos Personales \u003E Sellos autom\u00e1ticos",
    c_padre: 355187,
    padres: [355187],
    hijos: []
  },
  {
    idCategorias: 353775,
    c_nombre: "Almohadillas y tintas",
    c_prefix: "-",
    c_link_full: "/almohadillas-y-tintas",
    c_link: "almohadillas-y-tintas",
    c_nombre_full: "Almohadillas y tintas",
    c_padre: null,
    padres: [],
    hijos: []
  }
];
var currency = {
  symbol: "$",
  country_code: "ARS",
  thousand_separator: ".",
  decimal_separator: ",",
  code: "de-DE"
};
var fields = {
  name: {
    label: "Nombre",
    error_message: "Ingrese un nombre v\u00e1lido",
    regex:
      "/^(?=.{2,45}$)([a-zA-Z\u00e1-\u00fa\u00c1-\u00da](\\s[a-zA-Z\u00e1-\u00fa\u00c1-\u00da])?(\\s)?)*$/",
    placeholder: "Su nombre"
  },
  surname: {
    label: "Apellido",
    error_message: "Ingrese un apellido v\u00e1lido",
    regex:
      "/^(?=.{2,45}$)([a-zA-Z\u00e1-\u00fa\u00c1-\u00da](\\s[a-zA-Z\u00e1-\u00fa\u00c1-\u00da])?(\\s)?)*$/",
    placeholder: "Su apellido"
  },
  full_name: {
    label: "Nombre completo",
    error_message: "Ingrese un nombre y apellido v\u00e1lidos",
    regex:
      "/^(?=.{5,90}$)([a-zA-Z\u00e1-\u00fa\u00c1-\u00da](\\s[a-zA-Z\u00e1-\u00fa\u00c1-\u00da])?(\\s)?)*$/",
    placeholder: "Su nombre completo"
  },
  email: {
    label: "Email",
    error_message: "Ingrese un email v\u00e1lido",
    regex: "/^[\\w\\-\\.\\+]+\\@[a-zA-Z0-9\\.\\-]+\\.[a-zA-z]{2,4}$/",
    placeholder: "Su email"
  },
  password: {
    label: "Contrase\u00f1a",
    error_message: "Ingrese una contrase\u00f1a v\u00e1lida",
    regex: "/^.{6,12}$/",
    placeholder: "Su contrase\u00f1a"
  },
  repeat_password: {
    label: "Repetir contrase\u00f1a",
    error_message: "Ingrese una contrase\u00f1a v\u00e1lida",
    error_message_repeat: "Las contrase\u00f1as no coinciden",
    regex: "/^.{6,12}$/",
    placeholder: "Repetir contrase\u00f1a"
  },
  phone: {
    label: "Tel\u00e9fono",
    error_message: "Ingrese un n\u00famero de tel\u00e9fono v\u00e1lido",
    regex:
      "/^(?=.{6,25}$)((\\+)?(\\(|\\s\\(\\s|\\s\\()?[0-9](\\)|\\s\\)\\s|\\s\\))?((\\s|\\-|\\s\\-\\s)[0-9])?)*$/",
    placeholder: "Su tel\u00e9fono"
  },
  identification_number: {
    label: "DNI",
    error_message: "Ingrese un n\u00famero de documento v\u00e1lido ",
    regex: "/^[0-9]{7,15}$/",
    placeholder: "Su documento"
  },
  tax_identification_type: {
    label: "Tipo de documento",
    error_message: "Seleccione un n\u00famero de documento",
    options: ["DNI", "CUIT"]
  },
  tax_identification_number: {
    label: "DNI / CUIT / CUIL",
    error_message:
      "Ingrese un n\u00famero de documento, CUIT o CUIL v\u00e1lido",
    regex: "/^[0-9]{0,2}[\\-]{0,1}[0-9]{7,8}[\\-]{0,1}[0-9]{0,1}$/",
    placeholder: "Su documento"
  },
  tax_company_name: {
    label: "Raz\u00f3n social",
    error_message: "Ingrese una raz\u00f3n social v\u00e1lida",
    regex:
      "/^[a-z\u00e1-\u00fa\u00c1-\u00da\u00e4-\u00fc\u00c4-\u00dcA-Z0-9\\s\\!\\.\\-\\,\\?\\\u0022\u0027\\:\\\u00a1\\\u00bf\\%\\(\\)\\[\\]\\$\\+\\~\\#\\_\\\u0026\\/]{2,90}$/",
    placeholder: "Su raz\u00f3n social"
  },
  street_name: {
    label: "Calle",
    error_message: "Ingrese una calle v\u00e1lida",
    regex:
      "/^(?=.{1,30}$)(([a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9](\\.|\\s|\\,|\\,\\s)?)((\\s|\\s\\-\\s|\\-|\\.|\\s\\.|\\s\\.\\s)[a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9])?(\\s)?)*$/",
    placeholder: ""
  },
  street_number: {
    label: "N\u00famero",
    error_message: "Ingrese un n\u00famero de calle v\u00e1lido",
    regex:
      "/^(?=.{1,10}$)(([a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9](\\.|\\s|\\,|\\,\\s|\\/)?)((\\s|\\s\\-\\s|\\-|\\.|\\s\\.|\\s\\.\\s|\\s\\/|\\s\\/\\s)[a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9])?(\\s)?)*$/",
    placeholder: ""
  },
  floor: {
    label: "Piso",
    error_message: "Ingrese un piso v\u00e1lido",
    regex:
      "/^(?=.{1,10}$)(([a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9](\\.|\\s|\\,|\\,\\s|\\/)?)((\\s|\\s\\-\\s|\\-|\\.|\\s\\.|\\s\\.\\s|\\s\\/|\\s\\/\\s)[a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9])?(\\s)?)*$/",
    placeholder: ""
  },
  apartment: {
    label: "Dpto",
    error_message: "Ingrese un departamento v\u00e1lido",
    regex:
      "/^(?=.{1,10}$)(([a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9](\\.|\\s|\\,|\\,\\s|\\/)?)((\\s|\\s\\-\\s|\\-|\\.|\\s\\.|\\s\\.\\s|\\s\\/|\\s\\/\\s)[a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9])?(\\s)?)*$/",
    placeholder: ""
  },
  city: {
    label: "Ciudad",
    error_message: "Ingrese una ciudad v\u00e1lida",
    regex:
      "/^(?=.{1,30}$)(([a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9](\\.|\\s|\\,|\\,\\s)?)((\\s|\\s\\-\\s|\\-|\\.|\\s\\.|\\s\\.\\s)[a-z\u00e1-\u00fa\u00c1-\u00daA-Z0-9])?(\\s)?)*$/",
    placeholder: ""
  },
  zip_code: {
    label: "C\u00f3digo postal",
    error_message: "Ingrese un c\u00f3digo postal v\u00e1lido",
    regex: "/^[1-9]{1}[0-9]{3}$/",
    placeholder: "C\u00f3digo postal"
  },
  state: {
    label: "Provincia",
    error_message: "Seleccione una provincia",
    options: {
      "BUENOS AIRES": "Buenos Aires",
      "CAPITAL FEDERAL": "Capital Federal",
      CATAMARCA: "Catamarca",
      CHACO: "Chaco",
      CHUBUT: "Chubut",
      CORDOBA: "C\u00f3rdoba",
      CORRIENTES: "Corrientes",
      "ENTRE RIOS": "Entre Rios",
      FORMOSA: "Formosa",
      JUJUY: "Jujuy",
      "LA PAMPA": "La Pampa",
      "LA RIOJA": "La Rioja",
      MENDOZA: "Mendoza",
      MISIONES: "Misiones",
      NEUQUEN: "Neuqu\u00e9n",
      "RIO NEGRO": "R\u00edo Negro",
      SALTA: "Salta",
      "SAN JUAN": "San Juan",
      "SAN LUIS": "San Luis",
      "SANTA CRUZ": "Santa Cruz",
      "SANTA FE": "Santa Fe",
      "SGO. DEL ESTERO": "Santiago del Estero",
      "TIERRA DEL FUEGO": "Tierra del Fuego",
      TUCUMAN: "Tucum\u00e1n"
    }
  },
  discount_code: {
    label: "Cup\u00f3n de descuento",
    error_message: "Ingrese un cup\u00f3n v\u00e1lido",
    regex: "/^[0-9a-zA-Z\\-]{2,30}$/",
    placeholder: "C\u00f3digo"
  },
  message: {
    label: "Mensaje",
    label_alt: "Observaciones adicionales",
    error_message:
      "Ingrese un mensaje v\u00e1lido [Debe contener al menos 20 caracteres]",
    regex: "/^[\\s\\S]{20,5000}$/",
    placeholder: ""
  },
  quantity: {
    label: "Cantidad",
    error_message: "Ingrese una cantidad v\u00e1lida",
    regex: "/^[1-9]{1}([0-9]?)*$/",
    placeholder: ""
  },
  number: {
    label: "N\u00famero",
    error_message: "Ingrese un n\u00famero v\u00e1lido",
    regex: "/^[1-9]{1}([0-9]?)*$/",
    placeholder: ""
  }
};
var timezone_math = -3;
var search_autocomplete_not_found =
  "No se han encontrado productos, intente con otro nombre";

var sitekey = "6LfXRycUAAAAAFQF69mKvgHerNR9oDrDYw1BM_Kw";
var csrf_token = $('meta[name="csrf-token"]').attr("content");
var cdn = get_cloudfront_url("productos");
var detalle = [];
var descuento_metodo_pago = 0;
var descuento_metodo_pago_porcentaje = 0;

var login_captcha = null;
var register_captcha = null;
var recover_captcha = null;

function toggle_shipment_fields() {
  var envio = detalle.envio;
  var envio_seleccionado = envio.envios_disponibles[envio.envio_seleccionado];
  $("input[name=s2tipo]").val(envio_seleccionado.tipo);
  $("input[name=s2subtipo]").val(envio_seleccionado.subtipo);
  $("input[name=s2subsubtipo]").val(envio_seleccionado.subsubtipo);
  var not_required_fields = ["s2calle", "s2numero", "s2ciudad"];
  if (envio_seleccionado.tipo === 2) {
    $(".address-field__wrapper").hide();
    not_required_fields.forEach(function(field) {
      $("input[name=" + field + "]").attr("data-required", 0);
    });
  } else {
    $(".address-field__wrapper").show();
    not_required_fields.forEach(function(field) {
      $("input[name=" + field + "]").attr("data-required", 1);
    });
  }
  $("#step2-2").show();
}

function get_cart_detail() {
  $.ajax({
    beforeSend: function() {
      var htmlLoader =
        '<li class="checkout__detail-list-item"><div class="checkout__detail-divider"></div></li>';
      htmlLoader +=
        '<li class="checkout__detail-list-item uk-flex uk-flex-center">';
      htmlLoader += "<div uk-spinner></div>";
      htmlLoader += "</li>";
      $(".checkout__detail-list").html(htmlLoader);
    },
    headers: {
      "X-CSRF-TOKEN": csrf_token
    },
    url: "/v4/cart",
    dataType: "JSON",
    method: "GET",
    cache: false
  })
    .done(function(resp) {
      if (resp.status === 200) {
        detalle = resp.data;
        build_cart_detail();

        //debo revisar si hay envios y los cargo
        if (detalle.envio.requerido) {
          if (detalle.envio.codigo_postal) {
            $("input[name=s2postal]").val(detalle.envio.codigo_postal);
          }
          build_shipments(false);
        }
      }
    })
    .fail(function() {});
}
function build_cart_detail() {
  var i, j;
  var precio = 0;
  var total = 0;
  var productos = detalle.productos;
  var newHtml =
    '<li class="checkout__detail-list-item"><div class="checkout__detail-divider"></div></li>';
  for (i in productos) {
    precio = productos[i].precio_unitario * productos[i].cantidad;
    total += precio;
    newHtml += '<li class="checkout__detail-list-item">';
    newHtml += '<div class="checkout__detail-product uk-flex uk-flex-middle">';
    newHtml +=
      '<img src="' +
      cdn +
      productos[i].img +
      '" class="checkout__detail-product-image">';
    newHtml += '<div class="checkout__detail-product-info">';
    newHtml +=
      '<p class="checkout__detail-product-title">' +
      productos[i].cantidad +
      "x " +
      productos[i].nombre +
      "</p>";
    if (productos[i].atributos) {
      var atributosArr = [];
      for (j in productos[i].info_atributos) {
        atributosArr.push(
          productos[i].info_atributos[j].at_nombre +
            ": " +
            productos[i].info_atributos[j].vat_valor
        );
      }
      newHtml += '<p class="checkout__detail-product-subtitle">';
      newHtml += atributosArr.join(" - ");
      newHtml += "</p>";
    }
    newHtml +=
      '<p class="checkout__detail-product-price">' +
      number_format(precio) +
      "</p>";
    newHtml += "</div>";
    newHtml += "</div>";
    newHtml += "</li>";
  }

  //subtotal
  newHtml +=
    '<li class="checkout__detail-list-item"><div class="checkout__detail-divider"></div></li>';
  newHtml +=
    '<li class="checkout__detail-list-item checkout__detail-list-item--resume">';
  newHtml +=
    '<p class="checkout__detail-resume-title">' +
    checkout_labels.detail_subtotal +
    "</p>";
  newHtml +=
    '<p class="checkout__detail-resume-price">' + number_format(total) + "</p>";
  newHtml += "</li>";

  //descuento
  if (detalle.cupon.id) {
    newHtml +=
      '<li class="checkout__detail-list-item checkout__detail-list-item--resume">';
    newHtml +=
      '<p class="checkout__detail-resume-title">' +
      checkout_labels.detail_discount +
      "</p>";
    newHtml +=
      '<p class="checkout__detail-resume-price">-' +
      number_format(detalle.cupon.monto_descuento) +
      "</p>";
    newHtml += "</li>";
    total -= detalle.cupon.monto_descuento;
  }

  //reviso si hay descuento por metodo de pago
  if (descuento_metodo_pago) {
    var dcto = get_discount_price(total, descuento_metodo_pago_porcentaje);

    newHtml +=
      '<li class="checkout__detail-list-item checkout__detail-list-item--resume">';
    newHtml +=
      '<p class="checkout__detail-resume-title">' +
      checkout_labels.detail_discount +
      " " +
      descuento_metodo_pago_porcentaje +
      "%</p>";
    newHtml +=
      '<p class="checkout__detail-resume-price">-' +
      number_format(dcto) +
      "</p>";
    newHtml += "</li>";
    total -= dcto;
  }

  //costo de envio
  if (detalle.envio.requerido && detalle.envio.envio_seleccionado !== null) {
    var envio_seleccionado =
      detalle.envio.envios_disponibles[detalle.envio.envio_seleccionado];
    switch (envio_seleccionado.tipo) {
      case 2:
        break;
      case 3:
        break;
      case 5:
        switch (envio_seleccionado.cobro_envio) {
          case 0:
            newHtml +=
              '<li class="checkout__detail-list-item checkout__detail-list-item--resume">';
            newHtml +=
              '<p class="checkout__detail-resume-title">' +
              checkout_labels.detail_shipment +
              "</p>";
            newHtml +=
              '<p class="checkout__detail-resume-price">' +
              checkout_labels.detail_shipment_free +
              "</p>";
            newHtml += "</li>";
            break;
          case 1:
            newHtml +=
              '<li class="checkout__detail-list-item checkout__detail-list-item--resume">';
            newHtml +=
              '<p class="checkout__detail-resume-title">' +
              checkout_labels.detail_shipment +
              "</p>";
            newHtml +=
              '<p class="checkout__detail-resume-price">' +
              number_format(envio_seleccionado.precio) +
              "</p>";
            newHtml += "</li>";
            total += envio_seleccionado.precio;
            break;
        }
        break;
        break;
      default:
        newHtml +=
          '<li class="checkout__detail-list-item checkout__detail-list-item--resume">';
        newHtml +=
          '<p class="checkout__detail-resume-title">' +
          checkout_labels.detail_shipment +
          "</p>";
        newHtml +=
          '<p class="checkout__detail-resume-price">' +
          number_format(envio_seleccionado.precio) +
          "</p>";
        newHtml += "</li>";
        if (envio_seleccionado.precio > 0) {
          total += envio_seleccionado.precio;
        }
        break;
    }
  }

  //total
  newHtml +=
    '<li class="checkout__detail-list-item"><div class="checkout__detail-divider"></div></li>';
  newHtml +=
    '<li class="checkout__detail-list-item checkout__detail-list-item--resume">';
  newHtml +=
    '<p class="checkout__detail-resume-total-title">' +
    checkout_labels.detail_total +
    "</p>";
  newHtml +=
    '<p class="checkout__detail-resume-total-price">' +
    number_format(total) +
    "</p>";
  newHtml += "</li>";

  $(".checkout__detail-cart-qty").html(Object.keys(detalle.productos).length);
  $(".checkout__detail-list").html(newHtml);
}

function build_single_shipping_cart_option(option, envio_seleccionado) {
  var price_obj = format_shipment_price(option);
  var days_obj = format_shipment_days(option);
  var image =
    "https://dk0k1i3js6c49.cloudfront.net/iconos-envio/costo-envio/" +
    option.icono;
  var newHtml = '<li class="checkout__shipment-result-list-item">';
  newHtml += '<div class="uk-grid-collapse uk-flex-middle" uk-grid>';
  newHtml += '<div class="uk-width-auto">';
  newHtml += '<label for="shipment_option_radio-' + option.position + '">';
  if (option.position === envio_seleccionado) {
    newHtml +=
      '<input type="radio" name="shipment_option_radio" class="shipment_option_radio with-gap" value="' +
      option.position +
      '" id="shipment_option_radio-' +
      option.position +
      '" checked="checked"/><span></span>';
  } else {
    newHtml +=
      '<input type="radio" name="shipment_option_radio" class="shipment_option_radio with-gap" value="' +
      option.position +
      '" id="shipment_option_radio-' +
      option.position +
      '"/><span></span>';
  }
  newHtml += "</label>";
  newHtml += "</div>";
  newHtml += '<div class="uk-width-expand">';
  newHtml += '<div class="uk-flex uk-flex-middle">';
  newHtml += '<div class="checkout__shipment-result-item-image-wrapper">';
  newHtml += '<label for="shipment_option_radio-' + option.position + '">';
  newHtml +=
    '<img src="' + image + '" class="checkout__shipment-result-item-image">';
  newHtml += "</label>";
  newHtml += "</div>";
  newHtml += '<div class="checkout__shipment-result-item-info-wrapper">';
  newHtml += '<p class="checkout__shipment-result-item-info-title">';
  newHtml += '<label for="shipment_option_radio-' + option.position + '">';
  newHtml += option.nombre;
  newHtml += "</label>";
  newHtml += "</p>";
  if (price_obj.label) {
    newHtml += '<p class="checkout__shipment-result-item-info-price">';
    newHtml += '<label for="shipment_option_radio-' + option.position + '">';
    newHtml +=
      '<span class="checkout__shipment-result-item-info-price-wrapper">' +
      price_obj.label +
      "</span>";
    if (days_obj.label) {
      newHtml += " - " + days_obj.label;
      newHtml +=
        ' <span class="checkout__shipment-result-item-info-after">(' +
        days_obj.after +
        ")</span>";
    }
    newHtml += "</label>";
    newHtml += "</p>";
  }
  if (option.descripcion.length) {
    newHtml += '<p class="checkout__shipment-result-item-info-description">';
    newHtml += '<label for="shipment_option_radio-' + option.position + '">';
    newHtml += option.descripcion.join(" - ");
    newHtml += "</label>";
    newHtml += "</p>";
  }
  newHtml += "</div>";
  newHtml += "</div>";
  newHtml += "</div>";
  newHtml += "</div>";
  newHtml += "</li>";
  return newHtml;
}
function build_shipping_cart_options(
  key,
  envios_format,
  envio_seleccionado,
  in_other_office
) {
  var newHtml = "";
  if (envios_format[key].featured.length) {
    newHtml += "<div>";
    newHtml += '<p class="checkout__shipment-result-group-title">';
    newHtml += product_common["shipment_" + key];
    newHtml += "</p>";
    newHtml +=
      '<ul class="checkout__shipment-result-list checkout__shipment-result-list--' +
      key +
      ' border-radius">';

    envios_format[key].featured.forEach(function(option) {
      newHtml += build_single_shipping_cart_option(option, envio_seleccionado);
    });
    if (in_other_office && key === "office") {
      envios_format[key].others.forEach(function(option) {
        newHtml += build_single_shipping_cart_option(
          option,
          envio_seleccionado
        );
      });
    }
    newHtml += "</ul>";

    if (envios_format[key].others.length && !in_other_office) {
      newHtml +=
        '<p class="checkout__shipment-result-item-show-more"><a href="#" class="checkout__shipment-result-item-show-more-link">' +
        product_common.shipment_office_show_more +
        ' <span uk-icon="chevron-down"></span></a></p>';
    }

    newHtml += "</div>";
  }
  return newHtml;
}
function build_shipments(show_all) {
  var html = "";
  var envio = detalle.envio;
  if (envio.envios_disponibles.length) {
    if (show_all) {
      var envios_format_obj = sort_shipping_cart_options(
        envio.envios_disponibles,
        envio.envio_seleccionado
      );
      var in_other_office = envios_format_obj.in_other_office;
      var envios_format = envios_format_obj.options;

      html += build_shipping_cart_options(
        "domicile",
        envios_format,
        envio.envio_seleccionado,
        in_other_office
      );
      html += build_shipping_cart_options(
        "office",
        envios_format,
        envio.envio_seleccionado,
        in_other_office
      );
      html += build_shipping_cart_options(
        "other",
        envios_format,
        envio.envio_seleccionado,
        in_other_office
      );
      html += build_shipping_cart_options(
        "point",
        envios_format,
        envio.envio_seleccionado,
        in_other_office
      );
    } else {
      var option = envio.envios_disponibles[envio.envio_seleccionado];
      option.position = envio.envio_seleccionado;
      html += "<div>";
      html +=
        '<ul class="checkout__shipment-result-list checkout__shipment-result-list--0 border-radius">';
      html += build_single_shipping_cart_option(
        option,
        envio.envio_seleccionado
      );
      html += "</ul>";
      html +=
        '<p class="checkout__shipment-result-item-show-all"><a href="#" class="checkout__shipment-result-item-show-all-link">' +
        checkout_labels.shipment_show_all +
        ' <span uk-icon="chevron-down"></span></a></p>';
      html += "</div>";
    }

    toggle_shipment_fields();
  }
  $(".checkout_shipment-results").html(html);
}
function display_payment_methods(envio_seleccionado_tipo) {
  switch (envio_seleccionado_tipo) {
    //mercado envios
    case 1:
      //solo se puede usar mercado pago
      $(".checkout__payment-result-list-item").hide();
      $("#checkout__payment-result-list-item-1").show();
      break;

    //retiro en local
    case 2:
      //se puede usar todo
      $(".checkout__payment-result-list-item").show();
      break;

    //oca, correo argentino, epick, acordar, envios personalizado, email
    default:
      //puede usar todo excepto efectivo (2)
      $(".checkout__payment-result-list-item").show();
      $("#checkout__payment-result-list-item-2").hide();
      break;
  }
}

function change_step(to, goto = true) {
  $(".checkout__tab").removeClass("uk-active");
  $(".checkout__tab--" + to).addClass("uk-active");
  $(".checkout__fields-step").addClass("uk-hidden");
  $(".checkout__fields-step--" + to).removeClass("uk-hidden");
  if (goto) {
    goTo($("body"), 0, 0);
  }
  if (to === "shipment") {
    $(".step2-grid").attr("uk-grid", "masonry: false;");
    $(".step2-2-grid").attr("uk-grid", "masonry: false;");
  } else if (to === "finish") {
    $(".step3-grid").attr("uk-grid", "masonry: false;");
  }
}

function lack_stock_error() {
  var newHtml = '<div class="checkout_lack-stock-container">';
  newHtml +=
    '<h4 class="checkout_lack-stock-title">' +
    checkout_labels.lack_stock_detail +
    "</h4>";
  newHtml +=
    '<h5 class="checkout_lack-stock-text">' +
    checkout_labels.lack_stock_todo +
    "</h5>";
  newHtml +=
    '<a href="/?open_cart=1" class="button button--full-mobile background--primary background--primary-hover contrast_text--primary contrast_text--primary-hover uk-button uk-button-large border-radius" data-label="Siguiente" data-spinner-ratio="1">' +
    checkout_labels.lack_stock_refresh_btn +
    "</a>";
  newHtml += "</div>";
  $(".checkout__fields-container").html(newHtml);
  $("#checkout__side-btn").hide();
  goTo($("body"), 0, 0);
}

$(function() {
  //load tabs
  change_step(customer_in ? "shipment" : "contact", false);

  //input validation
  $(document).on("blur", ".field__input, .field__textarea", function() {
    var field = $(this);
    setTimeout(function() {
      validate_field(field);
    }, 20);
  });
  //end input validation

  //cargo detalle de carrito
  get_cart_detail();

  //step contact
  $("#step1").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#step1-btn"));
    $("#step1-alert").html("");
    if (validate_form($("#step1"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/cart/checkout/check-email",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          if (detalle.envio.requerido) {
            build_shipments(false);
          } else {
            $(".checkout__shipment-digital-email").html(
              $("input[name=s1email]").val()
            );
          }
          change_step("shipment");
          unset_loading_button($("#step1-btn"), false);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          if (resp && resp.message) {
            if (resp.message.code) {
              invalidate_field(
                $("input[name=s1email]"),
                resp.message.description
              );
              unset_loading_button($("#step1-btn"), false);
            } else {
              error(
                $("#step1-alert"),
                checkout_labels.error_something_went_wrong
              );
              unset_loading_button($("#step1-btn"), false);
            }
          } else {
            error(
              $("#step1-alert"),
              checkout_labels.error_something_went_wrong
            );
            unset_loading_button($("#step1-btn"), false);
          }
        });
    } else {
      unset_loading_button($("#step1-btn"), false);
    }
  });
  //end step contact

  //step shipment
  $("input[name=s2postal]").on("keyup", function() {
    $("#step2-2").hide();
  });
  $("#step2").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#step2-btn"));
    $("#step2-alert").html("");
    $("#step2-2").hide();
    if (validate_field($("input[name=s2postal]"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/cart/checkout/shipment-methods",
        dataType: "JSON",
        method: "POST",
        timeout: 60000,
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          detalle = resp.data;
          build_cart_detail();
          build_shipments(true);
          unset_loading_button($("#step2-btn"), false);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          if (resp && resp.message) {
            error($("#step2-alert"), resp.message.description);
          } else {
            error(
              $("#step2-alert"),
              checkout_labels.error_something_went_wrong
            );
          }
          unset_loading_button($("#step2-btn"), false);
        });
    } else {
      unset_loading_button($("#step2-btn"), false);
    }
  });
  //step shipment

  //step shipment-2
  $(".checkout_shipment-results").on(
    "change",
    ".shipment_option_radio",
    function() {
      var envio_seleccionado_nuevo = parseInt(
        $("input[name=shipment_option_radio]:checked").val()
      );
      var envio_seleccionado_viejo = detalle.envio.envio_seleccionado;

      if (envio_seleccionado_nuevo !== envio_seleccionado_viejo) {
        if ($("input[name=s3tipo]").is(":checked")) {
          descuento_metodo_pago = 0;
          descuento_metodo_pago_porcentaje = 0;
          $("input[name=s3tipo]:checked").prop("checked", false);
        }
        detalle.envio.envio_seleccionado = envio_seleccionado_nuevo;
        build_cart_detail();
        build_shipments(false);
        goTo($("body"), 0, 0);

        $.ajax({
          headers: {
            "X-CSRF-TOKEN": csrf_token
          },
          url: "/v4/cart/change-shipment-option",
          dataType: "JSON",
          method: "POST",
          data: {
            shipment_option: envio_seleccionado_nuevo
          }
        })
          .done(function(resp) {})
          .fail(function(err) {});
      }
    }
  );
  $(".checkout_shipment-results").on(
    "click",
    ".checkout__shipment-result-item-show-all-link",
    function(e) {
      e.preventDefault();
      build_shipments(true);
    }
  );
  $(".checkout_shipment-results").on(
    "click",
    ".checkout__shipment-result-item-show-more-link",
    function(e) {
      e.preventDefault();
      var envio = detalle.envio;
      var envio_seleccionado = envio.envio_seleccionado;
      var envios_format_obj = sort_shipping_cart_options(
        envio.envios_disponibles,
        envio.envio_seleccionado
      );
      var envios_format = envios_format_obj.options;
      $(".checkout__shipment-result-item-show-more").hide();
      var newHtml = "";
      envios_format["office"].others.forEach(function(option) {
        newHtml += build_single_shipping_cart_option(
          option,
          envio_seleccionado
        );
      });
      $(".checkout__shipment-result-list--office").append(newHtml);
    }
  );
  $("#step2-2").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#step2-2-btn"));
    if (validate_form($("#step2-2"))) {
      var envio_seleccionado_tipo = parseInt($("input[name=s2tipo]").val());
      unset_loading_button($("#step2-2-btn"), false);
      display_payment_methods(envio_seleccionado_tipo);
      change_step("finish");
    } else {
      unset_loading_button($("#step2-2-btn"), false);
      goTo($(".field--error"), 200, 100);
    }
  });
  //end step shipment-2

  //step finish
  $(".payment_option_radio").on("change", function() {
    var pago_tipo = parseInt($("input[name=s3tipo]:checked").val());
    if (pago_tipo === 2 && medios_pago.efectivo_descuento > 0) {
      descuento_metodo_pago = pago_tipo;
      descuento_metodo_pago_porcentaje = medios_pago.efectivo_porcentaje;
    } else if (pago_tipo === 5 && medios_pago.transferencia_descuento > 0) {
      descuento_metodo_pago = pago_tipo;
      descuento_metodo_pago_porcentaje = medios_pago.transferencia_porcentaje;
    } else {
      descuento_metodo_pago = 0;
      descuento_metodo_pago_porcentaje = 0;
    }
    build_cart_detail();
  });
  $("input[name=checkbox_use_shipment_data]").on("change", function() {
    if ($(this).is(":checked")) {
      var envio_tipo = parseInt($("input[name=s2tipo]").val());
      if (envio_tipo !== 7) {
        var shipment_fields = ["postal", "nombre", "apellido", "documento"];
        if (envio_tipo !== 2) {
          shipment_fields = shipment_fields.concat([
            "calle",
            "numero",
            "piso",
            "dpto",
            "ciudad",
            "provincia"
          ]);
        }
        shipment_fields.forEach(function(shipment_field) {
          if (shipment_field === "provincia") {
            var value = $("select[name=s2provincia]").val();
            $("select[name=s3provincia]").val(value);
          } else {
            var value = $("input[name=s2" + shipment_field + "]").val();
            $("input[name=s3" + shipment_field + "]").val(value);
            validate_field($("input[name=s3" + shipment_field + "]"));
          }
        });
      }
    }
  });
  $("#step3").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#step3-btn"));
    $("#step3-alert-1").html("");
    $("#step3-alert-2").html("");
    if (validate_form($("#step3"))) {
      if ($("input[name=s3tipo]").is(":checked")) {
        var flag = true;
        if (customer_in === 0) {
          if (
            $("input[name=s3clave]").val() !== $("input[name=s3reclave]").val()
          ) {
            flag = false;
          }
        }

        if (flag) {
          $.ajax({
            headers: {
              "X-CSRF-TOKEN": csrf_token
            },
            url: "/v4/cart/checkout/finish",
            dataType: "JSON",
            method: "POST",
            data: $("#step1,#step2,#step2-2,#step3").serialize()
          })
            .done(function(resp) {
              redirect(resp.data.uri);
            })
            .fail(function(err) {
              var resp = err.responseJSON;
              if (resp && resp.message) {
                switch (resp.message.code) {
                  case 0:
                    unset_loading_button($("#step3-btn"), false);
                    error($("#step3-alert-2"), resp.message.description);
                    goTo($("#step3-alert-2"), 200, 100);
                    break;
                  case 1: //email registrado
                    invalidate_field(
                      $("input[name=s1email]"),
                      resp.message.description
                    );
                    change_step("contact");
                    unset_loading_button($("#step3-btn"), false);
                    break;
                  case 2:
                    lack_stock_error();
                    break;
                  default:
                    unset_loading_button($("#step3-btn"), false);
                    error(
                      $("#step3-alert-2"),
                      checkout_labels.error_something_went_wrong + " COD-1"
                    );
                    goTo($("#step3-alert-2"), 200, 100);
                    break;
                }
              } else {
                unset_loading_button($("#step3-btn"), false);
                error(
                  $("#step3-alert-2"),
                  checkout_labels.error_something_went_wrong + " COD-1"
                );
                goTo($("#step3-alert-2"), 200, 100);
              }
            });
        } else {
          invalidate_field($("input[name=s3reclave]"), repeat_password_error);
          unset_loading_button($("#step3-btn"), false);
        }
      } else {
        unset_loading_button($("#step3-btn"), false);
        error(
          $("#step3-alert-1"),
          checkout_labels.payment_method_no_select_error
        );
        goTo($("#step3-alert-1"), 200, 100);
      }
    } else {
      unset_loading_button($("#step3-btn"), false);
      goTo($(".field--error"), 200, 100);
    }
  });
  //end step finish

  //step change
  $(".checkout__step-change-btn").on("click", function(e) {
    e.preventDefault();
    var to = $(this).attr("data-to");
    change_step(to);
  });
  //step change

  //login
  $("#login-modal").on({
    "show.uk.modal": function() {
      if (login_captcha === null) {
        login_captcha = grecaptcha.render("login-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#login-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(login_captcha);
          }
        });
      } else {
        grecaptcha.reset(login_captcha);
      }
    }
  });
  $("#login-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#login-btn"));
    $("#login-alert").html("");
    if (validate_form($("#login-form"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/login",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          redirect(window.location.href);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          grecaptcha.reset(login_captcha);
          if (resp && resp.message) {
            error($("#login-alert"), resp.message.description);
          }
          unset_loading_button($("#login-btn"), true);
        });
    } else {
      grecaptcha.reset(login_captcha);
      unset_loading_button($("#login-btn"), true);
    }
  });
  //end login

  //register
  $("#register-modal").on({
    "show.uk.modal": function() {
      if (register_captcha === null) {
        register_captcha = grecaptcha.render("register-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#register-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(register_captcha);
          }
        });
      } else {
        grecaptcha.reset(register_captcha);
      }
    }
  });
  $("#register-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#register-btn"));
    $("#register-alert").html("");
    if (validate_form($("#register-form"))) {
      if (
        compare_fields($("#register_password"), $("#register_repeat_password"))
      ) {
        $.ajax({
          headers: {
            "X-CSRF-TOKEN": csrf_token
          },
          url: "/v4/register",
          dataType: "JSON",
          method: "POST",
          data: new FormData($(this)[0]),
          cache: false,
          contentType: false,
          processData: false
        })
          .done(function(resp) {
            redirect(window.location.href);
          })
          .fail(function(err) {
            var resp = err.responseJSON;
            grecaptcha.reset(register_captcha);
            if (resp && resp.message) {
              error($("#register-alert"), resp.message.description);
            }
            unset_loading_button($("#register-btn"), true);
          });
      } else {
        grecaptcha.reset(register_captcha);
        unset_loading_button($("#register-btn"), true);
        invalidate_field(
          $("#register_repeat_password"),
          fields.repeat_password.error_message_repeat
        );
      }
    } else {
      grecaptcha.reset(register_captcha);
      unset_loading_button($("#register-btn"), true);
    }
  });
  //end register

  //recover
  $("#recover-modal").on({
    "show.uk.modal": function() {
      if (recover_captcha === null) {
        recover_captcha = grecaptcha.render("recover-btn", {
          sitekey: sitekey,
          callback: function() {
            $("#recover-form").submit();
          },
          "error-callback": function() {
            grecaptcha.reset(recover_captcha);
          }
        });
      } else {
        grecaptcha.reset(recover_captcha);
      }
    }
  });
  $("#recover-form").on("submit", function(e) {
    e.preventDefault();
    set_loading_button($("#recover-btn"));
    $("#recover-alert").html("");
    if (validate_form($("#recover-form"))) {
      $.ajax({
        headers: {
          "X-CSRF-TOKEN": csrf_token
        },
        url: "/v4/recover",
        dataType: "JSON",
        method: "POST",
        data: new FormData($(this)[0]),
        cache: false,
        contentType: false,
        processData: false
      })
        .done(function(resp) {
          $("#recover-modal-content").remove();
          $("#recover-modal-footer").remove();
          success($("#recover-alert"), resp.message.description);
        })
        .fail(function(err) {
          var resp = err.responseJSON;
          grecaptcha.reset(recover_captcha);
          if (resp && resp.message) {
            error($("#recover-alert"), resp.message.description);
          }
          unset_loading_button($("#recover-btn"), true);
        });
    } else {
      grecaptcha.reset(recover_captcha);
      unset_loading_button($("#recover-btn"), true);
    }
  });
  //end recover
});
