import axios from "axios";

import Vue from "vue";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

var vm = new Vue({
    el: "#mayoristas",
    data: {
        form: {
            nombre: "",
            ciudad: "",
            email: "",
            telefono: "",
            showForm: true,
            html: null,
            success: null,
            error: false,
            errortxt: "Ocurrió un problema al enviar el formulario.",
            errorValidation: {
                nombre,
                ciudad,
                email,
                telefono
            },
            textButton: "Enviar"
        }
    },
    methods: {
        created: function() {},
        enviarForm: function(event) {
            this.form.textButton =
                ' <svg width="1em" height="1em" class="mr-2 animate__animated animate__heartBeat animate__infinite	infinite" viewBox="0 0 16 16" class="bi bi-asterisk" fill="currentColor" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/> </svg> Enviando';
            axios
                .post("/mayoristas", {
                    nombre: this.form.nombre,
                    ciudad: this.form.ciudad,
                    email: this.form.email,
                    telefono: this.form.telefono
                })
                .then(function(response) {
                    console.log(response);
                    vm.$data.form.error = false;
                    vm.$data.form.html = response.data.html;
                    vm.$data.form.success = true;
                    this.form.textButton = "Enviado";
                })
                .catch(error => {
                    this.form.textButton = "Enviar";
                    if (error.response) {
                        vm.$data.form.error = true;
                        vm.$data.form.errorValidation = error;
                        vm.$data.form.html = "";
                        console.log(error.response);
                    }
                })
                .catch(function(error) {
                    vm.$data.form.error = true;
                    vm.$data.form.errorValidation = error;
                    vm.$data.form.html = "";
                    console.log(error);
                });
        }
    }
});
