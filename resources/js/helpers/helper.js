/**
 * Limpio el string de espacios al inicio y fin
 * @param string cadena
 */
function trim(str) {
    return str.replace(/^\s+|\s+$/gm, "");
}
/**
 * Valido si un string cumple una expresion regular
 * @param string value
 * @param string regex_string
 * @return bool
 */
function validate_regex(value, regex_string) {
    var regex = new RegExp(regex_string.slice(1, -1));
    return regex.test(value);
}

/**
 * Valido un campo (input o textarea)
 * @param element field_id
 * @return bool
 */
let validate_field = function(field) {
    var value = trim(field.value);
    var regex_string = field.getAttribute("data-regex");
    var error_message = field.getAttribute("data-message");
    var required = parseInt(field.getAttribute("data-required"));
    var id = field.getAttribute("id");
    var parent = field.parentElement;
    console.log(required);
    // this.parent.parent remove class

    field.classList.remove("is-invalid");
    parent.classList.remove("is-invalid");
    // field.classList.remove("is-invalid");
    // parent.classList.remove("is-invalid");

    // $("#" + id).classList.remove("field--error");

    if (required || value) {
        if (validate_regex(value, regex_string)) {
            return true;
        } else {
            field.classList.add("is-invalid");
            parent.classList.add("is-invalid");
            //   $(".field--" + id).classList.add("field--error");
            $("#" + id + "-msg").html(error_message);
            $(".field__message--" + id).html(error_message);
            return false;
        }
    } else {
        return true;
    }
};

/**
 * Coloco un campo como invalido sin importar su valor
 * @param element field
 * @param string error_message
 * @return void
 */
let invalidate_field = function(field, error_message) {
    var id = field.attr("id");
    $(".field--" + id).classList.add("field--error");
    $(".field__message--" + id).html(error_message);
};

export { validate_field, invalidate_field };
