// Calcular envio cuando toco boton
// mostrar envio si ya lo calcule
axios.defaults.headers.common["X-CSRF-TOKEN"] = document
  .querySelector('meta[name="csrf-token"]')
  .getAttribute("content");

function popular_form() {
  let codigoPostalInput = $("input[name=shipment_cost-cp]");
  let storageCodigoPostal = localStorage.getItem("codigoPostal");
  if (storageCodigoPostal) {
    codigoPostalInput.val(storageCodigoPostal);
  }
}

popular_form();

//calcular costo envio
$("#buscar-envios").on("submit", function(e) {
  e.preventDefault();
  var zip_code = parseInt($("input[name=shipment_cost-cp]").val());
  if (zip_code >= 1000 && zip_code <= 9421) {
    axios
      .get("/api/envio", {
        params: { cp: zip_code }
      })
      .then(function(response) {
        $("#envios-resultados").html(response.data.html);

        localStorage.setItem("codigoPostal", response.data.cp);
      })
      .catch(function(error) {
        // todo mostrar mensaje de error
        console.log(error.response);
      });
  } else {
    let input = $("input[name=shipment_cost-cp]");

    input.addClass("is-invalid");
    input.parent().addClass("is-invalid");

    input
      .parent()
      .parent()
      .append(
        '<div class="invalid-feedback"> Ingrese un código postal válido </div>'
      );
  }
});
