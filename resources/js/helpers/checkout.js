axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

import * as helper from "./helper.js";

const isEmpty = str => !str.trim().length;

$(document).on("blur", ".form-control", function() {
    var field = $(this);
    console.log(isEmpty(field.val()));
    if (field.val() != null) {
        setTimeout(function() {
            var validado = helper.validate_field(field);
            if (validado) {
                field.addClass("is-valid");
            }
        }, 20);
        habilitarBoton();
    }
});

// Habilitar boton de siguiente si todos
//los cambios requeridos estan completos
function habilitarBoton() {
    console.log("habilitar boton");
}
