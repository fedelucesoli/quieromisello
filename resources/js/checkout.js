window.Vue = require("vue");

import Vue from "vue";
import * as helper from "./helpers/helper";
// import * as helper from "./helper.js";

Vue.component(
    "envios-component",
    require("./components/EnviosComponent.vue").default
);

axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

var vm = new Vue({
    el: "#app",
    data: {
        contacto: {},
        envio: {
            tipoEnvio: "",
            direccion: {},
            datosEnvio: {}
        },
        showFormDomicilio: false,
        finalizar: false,
        btnDisabled: false,
        idpedido: ""
    },
    methods: {
        selectEnvio(value) {
            // Al seleccionar un envio se cambian los requeridos y la visibilidad de los inputs de direccion
            this.envio.datosEnvio = value;
            this.showFormDomicilio =
                this.envio.datosEnvio.tipoEnvio == "domicilio" ? true : false;
        },

        validar(event) {
            let isEmpty = str => str.trim().length;
            let field = event.target;
            if (isEmpty(field.value)) {
                setTimeout(function() {
                    var validado = helper.validate_field(field);
                    if (validado) {
                        field.classList.add("is-valid");
                    }
                }, 20);
            }
        },
        enviarPedido: function() {
            // Validar que los inputs que son requeridos esten completos
            this.btnDisabled = true;
            let datos = {
                contacto: this.contacto,
                tipo_envio: this.envio.datosEnvio.tipoEnvio,
                envio: this.envio
            };
            let _this = this;

            axios
                .post("/postCheckout", datos)
                .then(response => {
                    _this.idpedido = response.data.id;
                    _this.finalizar = true;
                })
                .catch(function(error) {
                    console.log(error);
                });
            console.log(datos);
        }
    },
    mounted: function() {}
});
