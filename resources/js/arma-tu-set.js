import Vue from "vue";

axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

var vm = new Vue({
    el: "#app",

    data: {
        catalogo: "",
        sellos: {},
        esquemas: {},
        esquemaSeleccionado: {},
        pasos: {
            actual: 2,
            cargando: false
        },
        contenido: [],
        datosParaEnviar: {
            nombre: ""
        }
    },
    methods: {
        siguientePaso: function(e) {
            e.preventDefault();

            this.pasos.actual++;
        },
        irPaso: function(paso) {
            this.pasos.actual = paso;
        },
        selectEsquema: function(index) {
            this.esquemaSeleccionado = this.esquemas[index];
            this.esquemas[index].datos = { seleccionado: true };
        },
        selectSello: function(sello, index, tamanio) {
            //   this.catalogo[tamanio][index].datos = { seleccionado: true };

            var cantidad_x = "cantidad_" + tamanio.toLowerCase();
            let restantes = this.esquemaSeleccionado[cantidad_x];
            // var sello = this.catalogo[tamanio][index];

            if (sello.datos) {
                sello.datos = false;
                this.esquemaSeleccionado[cantidad_x]++;
            } else {
                if (restantes > 0) {
                    sello.datos = true;
                    this.esquemaSeleccionado[cantidad_x]--;
                } else {
                    alert(
                        "no te quedan mas sellos " +
                            tamanio +
                            " para seleccionar"
                    );
                }
            }

            //   this.esquemaSeleccionado.cantidad_l--;
        }
    },
    mounted: function() {
        let producto = document.getElementById("producto");
        let d = document.getElementById("addToCart");
        if (d) {
            d.remove();
        }
        axios.get("/api/sellos").then(response => {
            this.sellos = response.data.data;
            this.catalogo = this.sellos["Español"];
        });
        axios
            .get(
                "/api/esquemas?search=" +
                    producto.dataset.id +
                    "&searchFields=id_producto"
            )
            .then(response => {
                this.esquemas = response.data.data;
            });
    },
    watch: {
        // lastName: function(val) {
        //   this.fullName = this.firstName + " " + val;
        // }
    }
});
