window.Vue = require("vue");

import { isEmpty } from "lodash";

import Vue from "vue";

axios.defaults.headers.common["X-CSRF-TOKEN"] = document
    .querySelector('meta[name="csrf-token"]')
    .getAttribute("content");

var vm = new Vue({
    el: "#app",

    data: {
        catalogos: {},
        sellos: {},
        esquemas: {},
        esquemaSeleccionado: {
            contador: function(tamaño) {
                let cantidad_x = "cantidad_" + tamaño.toLowerCase();
                let restantes_x = "restantes_" + tamaño.toLowerCase();

                let seleccionados = this[cantidad_x] - this[restantes_x];
                return seleccionados + " / " + this[cantidad_x];
            }
        },
        wizard: {
            alert: {
                texto: ""
            },
            esquemaComplete: false,
            pasoSeleccionado: "",
            pasos: {
                "Sellos L": {
                    nombre: "Sellos L",
                    completado: false,
                    modal: "sellos",
                    tamaño: "L",
                    contador: true
                },
                "Sellos M": {
                    nombre: "Sellos M",
                    completado: false,
                    modal: "sellos",
                    tamaño: "M",
                    contador: true
                },
                "Sellos S": {
                    nombre: "Sellos S",
                    completado: false,
                    modal: "sellos",
                    tamaño: "S",
                    contador: true
                }
            }
        },
        sellosSeleccionados: {}
    },
    methods: {
        abrirModal: function(paso) {
            this.wizard.pasoSeleccionado = paso;
            this.sellos = this.catalogos["Español"][paso.tamaño];
            this.wizard.queSellos = this.catalogos["Español"][paso.tamaño];

            $(".modal-loading").removeClass("cargado");
            $(".modal-loading").addClass("cargando");
            $("#" + paso.modal).modal("show");

            setTimeout(() => {
                $(".modal-loading").removeClass("cargando");
                $(".modal-loading").addClass("cargado");
            }, 200);
        },
        siguientePaso: function(e) {
            e.preventDefault();
            $(".modal").modal("hide");
        },
        selectEsquema: function(index) {
            this.esquemaSeleccionado = {
                ...this.esquemaSeleccionado,
                ...this.esquemas[index]
            };
            this.esquemaSeleccionado["restantes_l"] = this.esquemas[
                index
            ].cantidad_l;
            this.esquemaSeleccionado["restantes_m"] = this.esquemas[
                index
            ].cantidad_m;
            this.esquemaSeleccionado["restantes_s"] = this.esquemas[
                index
            ].cantidad_s;
            let precioHtml = document.getElementById("precio");
            precioHtml.innerHTML = this.esquemaSeleccionado.precio;
            if (
                this.wizard.esquemaComplete & !isEmpty(this.sellosSeleccionados)
            ) {
                for (let sello in this.sellosSeleccionados) {
                    this.sellosSeleccionados[sello].datos = false;
                    delete this.sellosSeleccionados[sello];
                }
            }
            this.wizard.esquemaComplete = true;
        },
        selectSello: function(sello, index, tamanio) {
            if (!this.wizard.esquemaComplete) {
                this.showAlert("Tenes que seleccionar un esquema", "error");
                return;
            }

            let restantes_x = "restantes_" + tamanio.toLowerCase();
            let restantes = this.esquemaSeleccionado[restantes_x];

            if (this.sellosSeleccionados[sello.id]) {
                sello.datos = false;
                delete this.sellosSeleccionados[sello.id];
                this.esquemaSeleccionado[restantes_x]++;
                console.log("deseleccionar");
                return;
            } else {
                if (restantes > 0) {
                    this.sellosSeleccionados[sello.id] = sello;
                    sello.datos = true;
                    this.esquemaSeleccionado[restantes_x]--;
                    if (this.esquemaSeleccionado[restantes_x] === 0) {
                        let paso = this.wizard.pasoSeleccionado.nombre;
                        this.wizard.pasos[paso].completado = true;
                    }
                } else {
                    this.showAlert(
                        "¡No quedan mas sellos para seleccionar!",
                        "error"
                    );
                }
            }
        },
        showAlert: function(mensaje, tipo) {
            this.wizard.alert.texto = mensaje;
            $(".d-relative").show();
            setTimeout(() => {
                $(".d-relative").hide();
            }, 3000);
        },
        enviarPedido: function() {
            let producto = document.getElementById("producto");
            let datos = {
                productoId: producto.dataset.id,
                precio: this.esquemaSeleccionado.precio,
                cantidad: 1,
                esquema: this.esquemaSeleccionado,
                sellos: this.sellosSeleccionados
            };
            axios
                .post("/carrito/agregar", datos)
                .then(function(response) {
                    console.log(response);
                    window.location.replace("/carrito");
                })
                .catch(function(error) {
                    console.log(error);
                });
            console.log(datos);
        }
    },
    mounted: function() {
        let producto = document.getElementById("producto");
        let d = document.getElementById("addToCart");

        if (d) {
            d.remove();
        }

        axios.get("/api/sellos").then(response => {
            this.catalogos = response.data.data;
            this.sellos = this.catalogos["Español"]["L"];
        });
        axios
            .get(
                "/api/esquemas?search=" +
                    producto.dataset.id +
                    "&searchFields=id_producto"
            )
            .then(response => {
                this.esquemas = response.data.data;
            });
    }
});
