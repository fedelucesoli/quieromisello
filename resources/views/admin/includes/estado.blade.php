@if(isset($estado))
    @if ($update)
    <form action="{{ route($ruta, $id) }}" method="post" class="form-horizontal">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        @if($estado == 1)
            <input type="hidden" name="estado" value="0">
            <span style="display: none; visibility: hidden">1</span>
            <button type="submit" class="btn btn-outline-success btn-sm">
                <i class="fa fa-check"></i> Publicado
            </button>
            @else
            <input type="hidden" name="estado" value="1">
            <span style="display: none; visibility: hidden">0</span>
            <button type="submit" class="btn btn-outline-danger btn-sm">
                <i class="fa fa-times"></i> Borrador
            </button>
        @endif

        </form>


        @else
        <label for="estado">Estado</label>
        <select name="estado" id="" class="form-control">
            <option value="1" @if($estado) selected="selected" @endif>Publicado</option>
            <option value="0" @if(!$estado) selected="selected" @endif>Borrador</option>
        </select>


    @endif



@endif
