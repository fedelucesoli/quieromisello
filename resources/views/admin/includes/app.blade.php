@extends('layouts.app')

@section('subnav')
    <div class="container mt-1">
        <div class="row">
            <div class="col">
                <ul class="nav nav-pills ">
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}"
                            href="{{route('admin.index')}}"><i class="bi bi-house-fill"></i> Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/pedidos*')) ? 'active' : '' }}"
                            href="{{route('admin.pedidos.index')}}">Pedidos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/productos*')) ? 'active' : '' }}"
                            href="{{route('admin.productos.index')}}">Productos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/sellos*')) ? 'active' : '' }}" href="#">Sellos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ (request()->is('admin/categorias*')) ? 'active' : '' }}"
                            href="{{route('admin.categorias.index')}}">Categorias</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
@endsection


@section('content')
   <div class="container mt-4">
       @yield('contenido')
   </div>
@endsection
