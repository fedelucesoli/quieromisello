<div class="row galeria d-flex" >
    <div class="col">
        {{-- if - Tengo portada o imagenes --}}
        @if($item->portada)

            <img src="{{asset('imagenes/thumbs/'.$item->portada)}}" alt="..." class="img-thumbnail lazyload">
            <div class="thumb-links mt-2">
                <a href=""> <i class="fa fa-trash mr-2"></i>Eliminar</a>
            </div>

        @endif

    </div>

</div>

