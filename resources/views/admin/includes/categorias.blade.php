<ul class="checkbox-list">
    @foreach($categorias as $categoria)
    <li>
        <div class="checkbox">
            <label>
                <input type="checkbox" @if(isset($selectedIds) && in_array($categoria->id,
                $selectedIds))checked="checked" @endif
                name="categorias[]"
                value="{{ $categoria->id }}">
                {{ $categoria->nombre }}
            </label>
        </div>
    </li>
    @if($categoria->children->count() >= 1)
    @include('admin.includes.categorias', ['categorias' => $categoria->children, 'selectedIds' => $selectedIds])
    @endif
    @endforeach
</ul>
