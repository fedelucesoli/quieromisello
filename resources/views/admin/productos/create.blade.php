@extends('admin.includes.app')

@section('contenido')
<!-- Main contenido -->
<section class="contenido">
    @include('admin.includes.mensajero')
    <div class="card">
        <form action="{{ route('admin.productos.store') }}" method="post" class="form" enctype="multipart/form-data">
            <div class="card-body row">
                {{ csrf_field() }}
                <div class="col-md-8">
                    <h2>Producto Nuevo</h2>

                    <div class="form-group">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" name="nombre" id="nombre" placeholder="" class="form-control"
                            value="{{ old('nombre') }}">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="precio">Precio <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" name="precio" id="precio" placeholder="" class="form-control"
                                    value="{{ old('precio') }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="precio">Precio promocion <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" name="precio_promocion" id="precio" placeholder="" class="form-control"
                                    value="{{ old('precio') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripcion </label>
                        <input type="hidden" class="form-control" name="descripcion"></input>
                        <div class="form-control " id="descripcion"></div>
                    </div>

                    <div class="form-group">
                        <label for="categoria">Categoria </label>

                        <select class="custom-select" name="categoria">
                        <option selected>Categoria</option>
                        @if(!$categorias->isEmpty())
                            @foreach ($categorias as $categoria)
                                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                            @endforeach
                        @endif
                    </select>

                    </div>

                    {{-- @include('admin.shared.status-select', ['status' => 0])
                    @include('admin.shared.attribute-select', [compact('default_weight')]) --}}
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">

                    <a href="{{ route('admin.productos.index') }}" class="btn btn-default">Cancelar</a>
                    <button type="submit" class="btn btn-primary">GUARDAR</button>

            </div>
        </form>
    </div>
    <!-- /.card -->

</section>
<!-- /.contenido -->
@endsection
@push('scripts')
<!-- Main Quill library -->
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

<!-- Theme included stylesheets -->
<link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
<script>
var quill = new Quill('#descripcion', {
  modules: {
    toolbar: [
      [{ header: [1, 2, false] }],
      ['bold', 'italic', 'underline'],
    //   ['image', 'code-block'],
      [{ list: 'ordered' }, { list: 'bullet' }]
    ]
  },
  theme: 'bubble'  // or 'bubble'
});
var form = document.querySelector('.form');
console.log(form);
form.onsubmit = function(e) {
    e.preventDefault();
  var descripcion = document.querySelector('input[name=descripcion]');
  descripcion.value = quill.root.innerHTML.trim();
  form.submit();

};
</script>
@endpush
