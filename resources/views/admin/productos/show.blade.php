@extends('admin.includes.app')

@section('contenido')
<!-- Main contenido -->
<section class="contenido">

    @include('admin.includes.mensajero')
    <!-- Default box -->
    @if($producto)
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">

                    <img src="" alt="" class="img-fluid" height="120px">
                    <h4>{{$producto->nombre}} <small>{{$producto->precio}}</small></h4>
                    <div class="row">
                        <div class="col">
                            <p class="text-muted">DESCRIPCION</p>
                            <p>{{$producto->descripcion}}</p>
                        </div>
                        <div class="col">
                            <p class="text-muted">CANTIDAD</p>
                            <p>{{$producto->stock_publico}}</p>
                        </div>
                        <div class="col"></div>
                    </div>

                    <a href="{{route('admin.productos.edit', $producto->id)}}" class=""> Editar </a>

                    <hr>
                    <h4>Esquemas</h4>
                    {{-- Foreach esquema --}}
                    <div class="row">
                        @foreach ($producto->esquemas as $esquema)
                        <div class="col">
                            <p>
                                <strong>L : {{$esquema->cantidad_l}}</strong>
                                <strong>M : {{$esquema->cantidad_m}}</strong>
                                <strong>S : {{$esquema->cantidad_s}}</strong>
                            </p>
                            <p>Precio: <strong>{{$esquema->precio}}</strong></p>
                            <form action="{{ route('admin.esquemas.destroy', $esquema->id) }}" method="post" class="form-horizontal">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <div class="btn-group">

                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i
                                            class="fa fa-times"></i>
                                        Eliminar</button>
                                </div>
                            </form>
                        </div>
                        @endforeach
                    </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.productos.index') }}" class="btn btn-default btn-sm">Volver</a>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <div class="col-4">
            @include('admin.esquemas.create',[
                'producto' => $producto->id,
                'nombre' => $producto->nombre,

            ])
        </div>
    </div>

    @endif

</section>
<!-- /.contenido -->
@endsection
