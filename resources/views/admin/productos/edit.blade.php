@extends('admin.includes.app')

@section('contenido')
<!-- Main contenido -->
<section class="contenido">
    @include('admin.includes.mensajero')
    <div class="card">

        <div class="card-header">

            <h4 class="d-inline">{{ ucfirst($producto->nombre) }}</h4>

        </div>
        <form action="{{ route('admin.productos.update', $producto->id) }}" method="post" class="form"
            enctype="multipart/form-data">
            <div class="card-body row">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">
                <div class="col-12">
                    @include('admin.includes.imagenes', ['item' => $producto])
                    <hr>

                </div>
                <div class="col-md-8">

                    <div class="form-group">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" name="nombre" id="nombre" placeholder="" class="form-control"
                            value="{!! $producto->nombre !!}">
                    </div>

                    <div class="form-group">


                        <label for="descripcion">Descripcion </label>
                        <input type="hidden" class="form-control" name="descripcion"></input>
                        <div class="form-control " id="descripcion">{!! $producto->descripcion !!}</div>
                    </div>

                    <div class="form-group">
                        <label for="stock_publico">Stock_publico <span class="text-danger">*</span></label>
                        <input type="text" name="stock_publico" id="stock_publico" placeholder="stock_publico"
                            class="form-control" value="{!! $producto->stock_publico !!}">
                    </div>

                    <div class="form-group">
                        <label for="precio">Precio <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" name="precio" id="precio" placeholder="precio" class="form-control"
                                value="{!! $producto->precio !!}">
                        </div>
                    </div>

                    <div class="form-group">
                        @include('admin.includes.estado',[
                        'id' => $producto->id,
                        'estado' => $producto->estado,
                        'ruta' => 'admin.productos.update',
                        'update' => false
                        ])
                    </div>

                    <label for="destacado">Destacado en portada </label>
                    <select name="destacado" id="" class="form-control">
                        <option value="1" @if($producto->destacado) selected="selected" @endif>En portada</option>
                        <option value="0" @if(!$producto->destacado) selected="selected" @endif>Sin destacar</option>
                    </select>

                    {{-- @include('admin.shared.status-select', ['status' => 0]) --}}

                </div>

                <div class="col-md-4">

                    <h2>Categoria</h2>

                    <div class="form-group">
                        <select class="custom-select" name="categoria">
                            <option selected>Categoria</option>
                            @if(!$categorias->isEmpty())
                                @foreach ($categorias as $categoria)
                                    <option
                                    value="{{$categoria->id}}"
                                    @if(
                                    isset($producto->categorias)
                                    && $producto->categorias->contains($categoria->id))
                                    selected="selected"
                                    @endif
                                        >{{$categoria->nombre}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <hr>

                    <h2>Imagenes</h2>

                    {{--
                        <div class="form-group">
                            @foreach($images as $image)
                            <div class="col-md-3">
                                <div class="row">
                                    <img src="{{ asset("storage/$image->src") }}" alt="" class="img-responsive img-thumbnail"> <br /> <br>
                                    <a onclick="return confirm('Are you sure?')" href="{{ route('product.remove.thumb', ['src' => $image->src]) }}" class="btn btn-danger btn-sm btn-block">Remove?</a><br />
                                </div>
                            </div>
                            @endforeach
                        div>
                     --}}

                    <div class="form-group">
                        <label for="portada">Portada </label>
                        <input type="file" name="portada_file" id="portada" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="image">Galeria</label>
                        <input type="file" name="image[]" id="image" class="form-control" multiple>
                        <small class="text-muted">Podes elegir mas de una imagen</small>
                    </div>

                </div>

            </div><!-- /.card-body -->

            <div class="card-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.productos.index') }}" class="btn btn-default">Atras</a>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </div>
        </form>
    </div><!-- /.card -->


</section>
<!-- /.contenido -->
@endsection

@push('scripts')
<!-- Main Quill library -->
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

<!-- Theme included stylesheets -->
<link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
<script>
var quill = new Quill('#descripcion', {
  modules: {
    toolbar: [
      [{ header: [1, 2, false] }],
      ['bold', 'italic', 'underline'],
    //   ['image', 'code-block'],
      [{ list: 'ordered' }, { list: 'bullet' }]
    ]
  },
  theme: 'bubble'  // or 'bubble'
});
var form = document.querySelector('.form');
console.log(form);
form.onsubmit = function(e) {
    e.preventDefault();
  var descripcion = document.querySelector('input[name=descripcion]');
  descripcion.value = quill.root.innerHTML.trim();
  form.submit();

};
</script>
@endpush
