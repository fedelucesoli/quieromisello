@extends('admin.includes.app')

@section('contenido')
<!-- Main contenido -->
<section class="contenido">

    <div class="card">
        <div class="card-header">
            <ul class="nav">

                <li class="nav-item">
                    <a class="nav-link dropdown-toggle" href="#" role="button" id="categorias" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Categorias
                    </a>
                    <div class="dropdown-menu" aria-labelledby="categorias">
                        @foreach ($categorias as $item)
                        <a class="dropdown-item"
                            href="{{ route('admin.productos.index', ['categoria' => $item->id]) }}">{{$item->nombre}}</a>
                        @endforeach
                    </div>
                </li>

                <li class="nav-item ">
                    <a class="nav-link " href="{{route('admin.categorias.index')}}">Editar Categorias</a>
                </li>

                <li class="nav-item ml-auto">
                    <a class="nav-link btn btn-sm rounded btn-outline-primary"
                        href="{{route('admin.productos.create')}}">CREAR PRODUCTO</a>
                </li>
            </ul>
            {{--
            <div class="row">
                <div class="col">
                    <a href="{{ route('admin.productos.index', ['orderBy' => 'created_at']) }}">Mas
            nuevos</a>
        </div>
        <div class="col text-right">
            <a href="{{ route('admin.productos.create') }}" class="btn btn-success">Nuevo</a>
        </div>
    </div> --}}
    {{-- Filtros y demas --}}
    </div>
    <div class="card-body">
        @if(!$productos->isEmpty())

        <table class="table">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Nombre</td>
                    <td>Precio</td>
                    <td>Estado</td>
                    <td>Acciones</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                <tr>
                    <td>{{ $producto->id }}</td>
                    <td>
                        <a href="{{ route('admin.productos.show', $producto->id) }}">{{ $producto->nombre }}</a>
                    </td>
                    <td>{{ $producto->precio }}</td>
                    <td>
                        @include('admin.includes.estado',[
                        'id' => $producto->id,
                        'estado' => $producto->estado,
                        'ruta' => 'admin.productos.update',
                        'update' => true
                        ])
                    </td>
                    <td>
                        <form action="{{ route('admin.productos.destroy', $producto->id) }}" method="post"
                            class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="delete">
                            <div class="btn-group">
                                <a href="{{ route('admin.productos.edit', $producto->id) }}"
                                    class="btn btn-outline-dark btn-sm">
                                    <i class="fa fa-edit"></i> Editar</a>
                                <button onclick="return confirm('Seguro?')" type="submit"
                                    class="btn btn-outline-dark btn-sm">
                                    <i class="fa fa-times"></i> Eliminar</button>
                            </div>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        @else


        <div class="row justify-contenido-center">
            <div class="col text-center py-5">
                <h2>No hay productos cargados</h2>
            </div>
        </div>

        @endif

    </div>
    <div class="card-footer"></div>
    </div>



</section>



@endsection
