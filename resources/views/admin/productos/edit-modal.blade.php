
        <form action="{{ route('admin.productos.update', $producto->id) }}" method="post" class="form"
            enctype="multipart/form-data">
            <div class="row">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">
                <div class="col-12">
                <div class="form-group">
                        @include('admin.includes.estado',[
                        'id' => $producto->id,
                        'estado' => $producto->estado,
                        'ruta' => 'admin.productos.update',
                        'update' => false
                        ])
                    </div>
                    </div>
                <div class="col-12">
                    @include('admin.includes.imagenes', ['item' => $producto])
                    <hr>
                </div>
                <div class="col-md-8">

                    <div class="form-group">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" name="nombre" id="nombre" placeholder="" class="form-control"
                            value="{!! $producto->nombre !!}">
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Descripcion </label>
                        <textarea class="form-control" name="descripcion" id="descripcion" rows="5"
                            placeholder="Descripcion">{!! $producto->descripcion !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="stock_publico">Stock_publico <span class="text-danger">*</span></label>
                        <input type="text" name="stock_publico" id="stock_publico" placeholder="stock_publico"
                            class="form-control" value="{!! $producto->stock_publico !!}">
                    </div>

                    <div class="form-group">
                        <label for="precio">Precio <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" name="precio" id="precio" placeholder="precio" class="form-control"
                                value="{!! $producto->precio !!}">
                        </div>
                    </div>

                    

                    {{-- @include('admin.shared.status-select', ['status' => 0]) --}}

                </div>

                <div class="col-md-4">

                    <h2>Categoria</h2>

                    <div class="form-group">
                        <select class="custom-select" name="categoria">
                            <option selected>Categoria</option>
                            @if(!$categorias->isEmpty())
                                @foreach ($categorias as $categoria)
                                    <option
                                    value="{{$categoria->id}}"
                                    @if(
                                    isset($producto->categorias)
                                    && $producto->categorias->contains($categoria->id))
                                    selected="selected"
                                    @endif
                                        >{{$categoria->nombre}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <hr>

                    <h2>Imagenes</h2>

                    {{--
                        <div class="form-group">
                            @foreach($images as $image)
                            <div class="col-md-3">
                                <div class="row">
                                    <img src="{{ asset("storage/$image->src") }}" alt="" class="img-responsive img-thumbnail"> <br /> <br>
                                    <a onclick="return confirm('Are you sure?')" href="{{ route('product.remove.thumb', ['src' => $image->src]) }}" class="btn btn-danger btn-sm btn-block">Remove?</a><br />
                                </div>
                            </div>
                            @endforeach
                        div>
                     --}}

                    <div class="form-group">
                        <label for="portada">Portada </label>
                        <input type="file" name="portada_file" id="portada" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="image">Galeria</label>
                        <input type="file" name="image[]" id="image" class="form-control" multiple>
                        <small class="text-muted">Podes elegir mas de una imagen</small>
                    </div>

                </div>

            </div><!-- /.card-body -->

            <div class="card-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.productos.index') }}" class="btn btn-default">Atras</a>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                </div>
            </div>
        </form>
    </div><!-- /.card -->


</section>