@extends('admin.includes.app')

@section('contenido')
<!-- Main contenido -->
<section class="contenido">
    @include('admin.includes.mensajero')
    <div class="card">
        <form action="{{ route('admin.categorias.store') }}" method="post" class="form" enctype="multipart/form-data">
            <div class="card-body row">
                {{ csrf_field() }}
                <div class="col-md-8">
                    <h2>Categoria Nueva</h2>

                    <div class="form-group">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" name="nombre" id="nombre" placeholder="" class="form-control"
                            value="{{ old('nombre') }}">
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Descripcion </label>
                        <textarea class="form-control" name="descripcion" id="descripcion" rows="5"
                            placeholder="Descripcion">{{ old('descripcion') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="parent"> Parent_ID <span class="text-danger">*</span></label>
                        <input type="text" name="parent" id="parent" placeholder="" class="form-control" value="{{ old('parent_id') }}">
                    </div>
                    <div class="form-group">
                        <label for="peso"> Peso <span class="text-danger">*</span></label>
                        <input type="text" name="peso" id="peso" placeholder="0" class="form-control" value="{{ old('peso') }}">
                    </div>

                </div>
                <div class="col-md-4">
                    <h2>Imagenes</h2>
                    <div class="form-group">
                        <label for="portada">Portada </label>
                        <input type="file" name="portada" id="portada" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="image">Galeria</label>
                        <input type="file" name="image[]" id="image" class="form-control" multiple>
                        <small class="text-muted">Podes elegir mas de una imagen</small>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.categorias.index') }}" class="btn btn-default">Atras</a>
                    <button type="submit" class="btn btn-primary">Crear</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.card -->

</section>
<!-- /.contenido -->
@endsection
