@extends('admin.includes.app')

@section('contenido')
<div class="card">
<div class="card-header">
            <div class="row">
                <div class="col">
                    <h4>Categorias</h4>
                </div>
                <div class="col text-right"> <a href="{{route('admin.categorias.create')}}" class="btn btn-success">Nuevo</a></div>
            </div>
            {{-- Filtros y demas --}}
        </div>
        <div class="card-body">
        @if(!$categorias->isEmpty())
<table class="table">
    <thead>
        <tr>
            <td>ID</td>
            <td>Nombre</td>
            <td>Parent</td>
            <td>Estado</td>
            <td>Acciones</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($categorias as $categoria)
        <tr>
            <td>{{ $categoria->id }}</td>
            <td>
                <a href="{{ route('admin.categorias.show', $categoria->id) }}">{{ $categoria->nombre }}</a>
          </td>
            <td> PARENT </td>
            <td>
                @include('admin.includes.estado',[
                'id' => $categoria->id,
                'estado' => $categoria->estado,
                'ruta' => 'admin.categorias.update',
                'update' => true
                ])
            </td>
            <td>
                <form action="{{ route('admin.categorias.destroy', $categoria->id) }}" method="post"
                    class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="delete">
                    <div class="btn-group">
                        <a
                            href="{{ route('admin.categorias.edit', $categoria->id) }}" class="btn btn-primary btn-sm"><i
                                class="fa fa-edit"></i> Editar</a>
                        <button onclick="return confirm('Are you sure?')"
                            type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>
                            Eliminar</button>
                    </div>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@else

<div class="row justify-contenido-center">
    <div class="col text-center py-5">
        <h2>No hay categorias cargadas</h2>
    </div>
</div>

@endif</div>
</div>



@endsection
