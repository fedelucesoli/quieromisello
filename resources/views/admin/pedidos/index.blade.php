@extends('admin.includes.app')

@section('contenido')
<div class="row">

    <div class="col">
        <div class="card">
            <div class="card-header">
                <ul class="nav">
                    <li class="nav-item pt-2">
                        <p>Pedidos</p>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" href="#" role="button" id="Estado" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            Estado
                        </a>
                        <div class="dropdown-menu" aria-labelledby="Estado">
                            <a class="dropdown-item"
                                href="
                                                            {{ route('admin.pedidos.index', ['estado' => 'pago']) }}">Pagados</a>
                            <a class="dropdown-item"
                                href="{{ route('admin.pedidos.index', ['estado' => 'sinpagar']) }}">Sin
                                pagar</a>
                            <a class="dropdown-item"
                                href="{{ route('admin.pedidos.index', ['estado' => 'enviado']) }}">Enviado</a>
                        </div>

                    </li>
                </ul>
            </div>

            <div class="card-body">
                <pedidos-component></pedidos-component>
            </div>
        </div>
    </div>
    <div class="d-none d-md-block col-md-4">
        <div class="">
            <ul class="list-group">
                <li class="list-group-item"></li>
                <li class="list-group-item"></li>
                <li class="list-group-item"></li>
                <li class="list-group-item"></li>
            </ul>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('js/admin/pedidos.js') }}"></script>
@endpush
