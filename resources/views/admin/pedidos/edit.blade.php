@extends('admin.includes.app')

@section('contenido')
<!-- Main contenido -->
<section class="contenido">
    @include('admin.includes.mensajero')
    <div class="card">
        <div class="card-header">
            <h2 class="d-inline">{{ ucfirst($categoria->nombre) }}</h2>

        </div>
        <form action="{{ route('admin.categorias.update', $categoria->id) }}" method="post" id="form" class="form"
            enctype="multipart/form-data">
            <div class="card-body row">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="put">
                <div class="col-12">
                    @include('admin.includes.imagenes', ['item' => $categoria])
                    <hr>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" name="nombre" id="nombre" placeholder="xxxxx" class="form-control"
                            value="{!! $categoria->nombre !!}">
                    </div>
                    <div class="form-group">
                        <label for="slug">slug <span class="text-danger">*</span></label>
                        <input type="text" name="slug" id="slug" placeholder="xxxxx" class="form-control"
                            value="{!! $categoria->slug !!}">
                    </div>

                    <div class="form-group">
                        <label for="descripcion">Descripcion </label>
                        <textarea class="form-control" name="descripcion" id="descripcion" rows="5"
                            placeholder="Descripcion">{!! $categoria->descripcion !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="parent">Parent <span class="text-danger">*</span></label>
                        <input type="text" name="parent" id="parent" placeholder="parent" class="form-control"
                            value="{!! $categoria->parent !!}">
                    </div>

                    <div class="form-group">
                        @include('admin.includes.estado',[
                        'id' => $categoria->id,
                        'estado' => $categoria->estado,
                        'ruta' => 'admin.categorias.update',
                        'update' => false
                        ])
                    </div>

                </div>
                <div class="col-md-4">

                    <h2>Imagenes</h2>
                    {{-- <div class="form-group">
                            @foreach($images as $image)
                            <div class="col-md-3">
                                <div class="row">
                                    <img src="{{ asset("storage/$image->src") }}" alt="" class="img-responsive img-thumbnail"> <br /> <br>
                                    <a onclick="return confirm('Are you sure?')" href="{{ route('product.remove.thumb', ['src' => $image->src]) }}" class="btn btn-danger btn-sm btn-block">Remove?</a><br />
                                </div>
                            </div>
                            @endforeach
                        </div>  --}}



                    <div class="form-group">
                        <label for="portada_file">Portada </label>
                        <input type="file" name="portada_file" id="portada_file" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="image">Galeria</label>
                        <input type="file" name="image[]" id="image" class="form-control" multiple>
                        <small class="text-muted">Podes elegir mas de una imagen</small>
                    </div>

                </div>
            </div>
        </form>
        <div class="card-footer">
            <div class="btn-group">
                <a href="{{ route('admin.categorias.index') }}" class="btn btn-default">Atras</a>
                <button type="submit" onclick="event.preventDefault(); document.getElementById('form').submit();"class="btn btn-primary">Guardar Cambios</button>
            </div>
        </div>

    </div>
    <!-- /.card -->

</section>
<!-- /.contenido -->
@endsection
