@extends('admin.includes.app')

@section('contenido')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">  <a href="{{ url()->previous() }}" class="btn-link mr-3">Atras</a> Pedido {{$pedido->id}}</div>
            <div class="card-body">
                <pedido-component :pedido="{{$pedido}}"></pedido-component>
            </div>
        </div>

    </div>
</div>
@endsection
@push('scripts')
    <script src="{{ asset('js/admin/pedidos.js') }}"></script>
@endpush
