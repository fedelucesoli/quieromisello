@extends('admin.includes.app')

@section('contenido')

@include('layouts.includes.alerts')

<div class="row">
    <div class="col-8">
        <div class="card">
            <div class="card-header">PEDIDOS</div>
            <div class="card-body">
                Listado de pedidos
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <h5 class="card-header">Herramientas</h5>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <a href="{{ route('admin.optimizar') }}">Artisan - Optimize </a>
                </li>
                <li class="list-group-item">
                    <a href="{{ route('admin.optimizar') }}">Artisan - cache </a>
                </li>
                <li class="list-group-item">
                    <a href="{{ route('admin.optimizar') }}">Artisan - migrate </a>
                </li>
            </ul>

        </div>
    </div>

</div>



@endsection
