<div class="card">
    <form action="{{ route('admin.esquemas.store') }}" method="post" class="form" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="tipo" value="esquema">
        <input type="hidden" name="id_producto" value="{{$producto}}">
        <div class="card-body">
            <h4>Crear esquema</h4>
            <hr>
            <div class="form-group">
                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                <input type="text" name="nombre" id="nombre" placeholder="" class="form-control" value="{{$nombre}} "
                    value="{{ old('nombre') }}">
            </div>
            <div class="form-group">
                <label for="precio">Adicional<span class="text-danger">*</span></label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">$</span>
                    </div>
                    <input type="text" name="precio" id="precio" placeholder="0" class="form-control" value="{{ old('precio') }}">
                </div>
            </div>

            <label for="">Cantidad de Sellos <span class="text-danger">*</span></label>
            <div class="form-group">
                {{-- <label for="sello-l">Sellos L <span class="text-danger">*</span></label> --}}
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">L</span>
                    </div>
                    <input type="text" name="cantidad_l" id="sello-l" placeholder="0" class="form-control"
                        value="{{ old('sello-l') }}">
                </div>
            </div>
            <div class="form-group">
                {{-- <label for="sello-l">Sellos M <span class="text-danger">*</span></label> --}}
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">M</span>
                    </div>
                    <input type="text" name="cantidad_m" id="sello-m" placeholder="0" class="form-control"
                        value="{{ old('sello-l') }}">
                </div>
            </div>
            <div class="form-group">
                {{-- <label for="sello-l">Sellos S <span class="text-danger">*</span></label> --}}
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">S</span>
                    </div>
                    <input type="text" name="cantidad_s" id="sello-s" placeholder="0" class="form-control"
                        value="{{ old('sello-l') }}">
                </div>
            </div>


            <h4 class="mt-5">Imagenes</h4>
            <hr>
            <div class="form-group">
                <label for="portada">Portada </label>
                <input type="file" name="portada" id="portada" class="form-control">
            </div>
            <div class="form-group">
                <label for="image">Galeria</label>
                <input type="file" name="image[]" id="image" class="form-control" multiple>
                <small class="text-muted">Podes elegir mas de una imagen</small>
            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="btn-group">
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        </div>
    </form>
</div>
<!-- /.card -->
