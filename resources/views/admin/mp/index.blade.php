@extends('admin.includes.app')

@section('contenido')

    <div class="row justify-contenido-center">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header">MercadoPago</h5>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{route('admin.mercadopago.crearticket')}}">Crear ticket</a>

                </div>
            </div>
        </div>
    </div>

@endsection
