@extends('layouts.app')
@section('titulo', $producto->nombre . '- Quiero mi sello' )

@section('content')

<section class="container mb-5" id="producto" data-id="{{$producto->id}}">

        <div class="row">

            {{-- Slider con imagenes al costado, en mobile sin imagenes --}}

            @if ($producto->portada)

                <div class="col-12 col-sm-12 col-md-8 offset-md-2 col-lg-7 offset-lg-0">
                    <img data-sizes="auto"
                    src="{{asset('imagenes/thumbs/'. $producto->portada)}}"
                    data-srcset="{{asset('imagenes/full/'. $producto->portada)}}"
                    class="lazyload rounded shadow-sm w-100"
                    alt="{{ $producto->nombre }}">
                </div>


            @endif

            <div class="col-12 col-sm-12 col-md-12 col-lg-5 py-4">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb pl-0">
                        <li class="breadcrumb-item"> <a href="{{ route('index') }}">Inicio</a></li>

                        @if ($categoria = $producto->categorias->first()->parent)
                        <li class="breadcrumb-item ">
                            <a href="{{ route('categoria', $categoria['slug'] ) }}">{{ $categoria['nombre'] }}</a>
                        </li>
                        @endif

                        <li class="breadcrumb-item">
                            <a href="{{ route('categoria', $producto->categorias->first()['slug'] ) }}">{{ $producto->categorias->first()['nombre'] }}</a>
                        </li>
                        {{-- <li class="breadcrumb-item active" aria-current="page">{{ $producto->nombre }}</li> --}}
                    </ol>
                </nav>


                <h2 class="text-danger">{{ $producto->nombre }}</h2>

                <p class=" lead mr-auto text-muted"> <small>$ </small> <span id="precio">{{ $producto->precio }}</span></p>

                <hr>

                <p class="text-muted">{!! $producto->descripcion !!}</p>



                {{-- Si es personalizable --}}
                {{-- cargo un blade con el tipo de personalizable y  --}}
                @if ($producto->tipo_personalizable )

                    @switch($producto->tipo_personalizable)
                        @case('arma-tu-set')
                            @include('front.caracteristicas.arma-tu-set')
                            @break

                        @case('seño-personalizado')
                            @include('front.caracteristicas.seño-personalizado')
                            @break

                        @case('automatico')
                            @include('front.caracteristicas.automatico')
                            @break
                        @case('marca')
                            @include('front.caracteristicas.marca')
                            @break

                        @default

                    @endswitch

                @endif
                {{-- Si tiene caracteristicas las cargo --}}
                {{-- Sello con nombre Caracterisitica - nomrbe- tipografias - dibujo --}}
                {{-- Sello con nombre Caracterisiticas_valores - input- tipografias1 - dibujo1 --}}
                <form action="{{route('addToCart')}}"  method="POST" role="form" id="addToCart" class=" py-3">
                    @csrf

                <div class="row">


                    @if (count($caracteristicas))
                        @foreach ($caracteristicas as $nombre => $caracteristica)

                            <div class="form-group col-12 col-md-8" id="{{$nombre}}">

                                <label for="{{$nombre}}">{{$nombre}}</label>
                                <select class="custom-select" name="{{$nombre}}" >
                                    @foreach ($caracteristica as $valor)
                                        <option value="{{$valor->nombre}}">{{$valor->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endforeach

                    @endif



                    <div class="form-group col-12 col-md-8" id="cantidad">

                        <label for="cantidad">Cantidad</label>

                        <input class="form-control" type="number" min="1" value="1" max="{{ $producto->stock }}" name="cantidad" >

                    </div>

                    <input type="hidden" name="productoId" value="{{ $producto->id }}">

                    <input type="hidden" name="precio" id="finalPrice" value="{{$producto->precio}}">

                    <div class="form-group col-12">

                        <button type="submit" class="btn p-3 btn-primary py-2 btn-block  rounded"><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-handbag mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1a2 2 0 0 0-2 2v4.5a.5.5 0 0 1-1 0V3a3 3 0 0 1 6 0v4.5a.5.5 0 0 1-1 0V3a2 2 0 0 0-2-2z"></path>
                        <path fill-rule="evenodd" d="M3.405 6a.5.5 0 0 0-.498.45l-.912 6.9A1.5 1.5 0 0 0 3.488 15h9.024a1.5 1.5 0 0 0 1.493-1.65l-.913-6.9a.5.5 0 0 0-.497-.45h-9.19zm-1.493.35A1.5 1.5 0 0 1 3.405 5h9.19a1.5 1.5 0 0 1 1.493 1.35L15 13.252A2.5 2.5 0 0 1 12.512 16H3.488A2.5 2.5 0 0 1 1 13.251l.912-6.9z"></path>
                    </svg> AGREGAR AL PEDIDO</button>

                    </div>

                </div>

                </form>

                 {{-- @include('front.includes.buscar-envios') --}}

            </div>


        </div>





</section>

@include('front.includes.modal')

 @role('superadmin')

 {{-- <div class="fixed-bottom py-3 px-4 text-center">
     <a class="btn btn-lg btn-primary w-50 shadow" href="#">EDITAR</a>
 </div> --}}

 @endrole

@endsection

@push('zocalo')
    @include('front.includes.banner-envio-pago')
@endpush

@push('scripts')
    <script src="{{ asset('js/buscar-envios.js') }}"></script>
@endpush
