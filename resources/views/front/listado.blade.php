@extends('layouts.app')
@section('titulo', $categoria->nombre )

@section('content')

<section class="container my-4">
    <nav aria-label="breadcrumb ">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="{{route('index')}}">Inicio</a></li>
            @if ($categoria_parent = $categoria->parent)
             <li class="breadcrumb-item ">
                <a href="{{ route('categoria', $categoria_parent['slug'] ) }}">{{ $categoria_parent['nombre'] }}</a>
             </li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">{{$categoria->nombre}}</li>
        </ol>
    </nav>

</section>

<section class="container my-3">
    <div class="row">
        {{-- <div class="col-12 mb-3 text-center">
            <h4>{{$categoria->nombre}}</h4>
            <hr>
        </div> --}}
        <div class="col-12 col-md-3 order-2 order-md-1 ">
            <h5 class="d-md-none mt-5 mt-md-1">Categorias</h5>
            <div class="list-group list-group-flush1">
                @foreach ($categorias as $cat)
                    <a href="{{route('categoria', $cat->slug)}}" class="list-group-item list-group-item-action">{{$cat->nombre}}</a>
                @endforeach
            </div>
        </div>
        <div class="col-12 col-md-9 order-md-2">

            <div class="row">
            @foreach ($productos as $key => $producto)

                    @include('front.includes.card-producto', $producto->first())

            @endforeach
            </div>
            {{-- @foreach ($categoria->subcategorias as $subcategoria)
             <div class="row">
                <div class="col-12">
                    <h4>{{$subcategoria->nombre}}</h4>
                </div>
                @foreach ($subcategoria->productos as $producto)
                     @include('front.includes.card-producto', $producto)
                @endforeach
            </div>
            @endforeach --}}

        </div>

    </div>

</section>



{{-- @include('front.includes.banner-envio-pago') --}}
@include('front.includes.modal')

@endsection

@push('zocalo')
    @include('front.includes.banner-envio-pago')
@endpush

