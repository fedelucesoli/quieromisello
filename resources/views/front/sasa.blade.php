@extends('layouts.app')
@section('titulo', '' )

@section('content')

<section id="container" class="container ">

    <div class="row justify-content-center">
        <div class="col-12 mt-5 py-4 bg-white shadow rounded">

            <div id="cabecera" class=" mb-4 sticky-top">
                <div class="row justify-content-center ">
                    <div class="col cabecera-nav clickable" v-bind:class="{ 'active ': pasos.actual == 1 }"
                        data-paso="1" v-on:click="irPaso(1)">
                        <span class="circle">1</span> INFO
                    </div>
                    <div class="col cabecera-nav" v-bind:class="{ 'active ': pasos.actual == 2 }" data-paso="2"
                        v-on:click="irPaso(2)">
                        <span class="circle">2</span> ESQUEMA
                    </div>
                    <div class="col cabecera-nav" v-bind:class="{ 'active ': pasos.actual == 3 }" data-paso="3"
                        v-on:click="irPaso(3)">
                        <span class="circle">3</span> SELLOS
                    </div>
                    <div class="col cabecera-nav" v-bind:class="{ 'active ': pasos.actual == 4 }" data-paso="4"
                        v-on:click="irPaso(4)">
                        <span class="circle">4</span> EXTRAS
                    </div>
                </div>
            </div>

                <div id="contenedor">

                    <div class="col-12 py-5" id="nombre" v-if="pasos.actual == 1">
                        <form action="" class="form text-center">

                            <h4>Para quien es el set?</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" v-model="datosParaEnviar.nombre" id="nombre" aria-describedby="tuNombre" placeholder="Nombre">
                            </div>

                        </form>


                    </div>


                    <div id="esquemas" v-if="pasos.actual == 2">
                        {{-- <h4>Esquemas</h4> --}}
                        <div class="row">
                            <div class="col-6 my-4 text-center"  v-for="(esquema, index) in esquemas" :data-id="esquema.id" v-on:click='selectEsquema(index)'>
                                <div class=" esquemas" >
                                    <p class="mb-1">@{{esquema.nombre}}</p>
                                    <img  :src="'/imagenes/catalog/'+esquema.portada" :alt="esquema.nombre" class="img-fluid rounded esquema" v-bind:class="{ 'activo': esquemaSeleccionado.id == esquema.id } ">
                                </div>
                            </div>

                    </div>

                    <div  id="sellos" v-if="pasos.actual == 3">

                        <div class="row" v-for="(sellos, tamanio, index) in catalogo">
                            <div class="col-12 mt-3 text-center"><h4>Sellos @{{tamanio}}</h4> </div>

                            <div class="col-4 col-sm-4 col-md-4 col-lg-3 my-2 px-2" v-for="sello in sellos">
                                    {{-- v-bind:class="{ 'activo': sello.datos.seleccionado }" --}}
                                <div
                                v-bind:class="[{'activo': sello.datos }, 'card rounded-lg sellos', sello.tamanio]"
                                v-on:click.prevent="selectSello(sello, index, tamanio)">

                                    <img class="lazyload card-img-top w-100" data-sizes="auto"

                                        data-aspectratio="1/1"
                                        :data-src="'/img/sellos/'+ sello.portada"
                                        :alt="sello.nombre">
                                    <div class="card-img-overlay"> </div>
                                    <div class="sello-numero "  v-bind:class="sello.tamanio"> @{{sello.nombre}} </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-12">
                    <div class="boton">
                            <a href="" class="btn btn-primary btn-block btn-dark " v-on:click="siguientePaso">SIGUIENTE
                                {{-- <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-arrow-right-circle" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z" />
                                </svg> --}}
                            </a>

                    </div>

                </div>

            </div>

    </div>
</section>



@endsection

@push('scripts')
    <script src="{{ asset('js/arma-tu-set.js') }}"></script>
@endpush
