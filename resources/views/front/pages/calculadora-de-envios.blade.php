@extends('layouts.app')
@section('titulo', 'Calcular envio')

@section('content')

<div class="container mt-5  py-4" id="envios" v-cloak>

    <div class="row justify-content-center">
        <div class="col-12 col-md-10 col-lg-7 ">
            <div class="card">
                <div class="card-body ">
                    <img src="{{asset('imagenes/logos-iconos/envios.svg')}}" alt="QUIEROMISELLO" height="90" class="my-2 d-block">
                    <h5 class="card-title pb-0 mb-0">Calculadora de envios</h5>
                    <p class="card-text mb-2 text-muted">Ingresa tu código postal para conocer un estimado.</p>
                    <envios-component :seleccionable="envioSeleccionable"></envios-component>
                    {{-- <div class="input-group my-3" v-show="envio.showForm">
                        <input type="number" v-model="envio.cp" class="form-control" placeholder="Código Postal"
                            aria-label="Código Postal" aria-describedby="codigopostal">
                        <div class="input-group-append">
                            <button class="btn btn-outline-danger" type="button"
                                v-on:click.prevent="calcularenvio()">CALCULAR</button>
                        </div>
                    </div>
                    <small id="" v-show="envio.error"
                        class="form-text  text-center text-muted">@{{ envio.errortxt }}</small>

                    <div class="mt-4" v-html="envio.html"></div> --}}

                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
<script src="{{ asset('js/envios.js') }}"></script>
@endpush
