<div class="row">
  <div class="col-12 text-center">
    <h6>Opciones de envío</h6>
    {{-- <p> Envío a {{$menvios['body']->destination->state->name}} ({{$menvios['body']->destination->zip_code}})</p> --}}
  </div>

  <div class="col-12">

        <ul class="list-group">
        @foreach ($data as $envio)
        <li class="list-group-item py-4" data-id="">
            {{-- <span class="align-middle float-left"> <i class="fa fa-check mr-3"></i> </span> --}}
            <span><img src="{{asset('imagenes\logos-iconos\oca.png')}}" height="25" alt="Envios por Oca" class="mr-2"></span>

            <span class="text-muted ">
                {!!$envio['name']!!}
                <small>{{$envio['pago']}}</small>
            </span>
            <span class="float-right d-inline">$ {{$envio['cost']}}</span>
            </li>
        @endforeach


        {{-- <li class="list-group-item pt-4" data-id="">
            <span><img src="{{asset('imagenes\logos-iconos\oca.png')}}" height="25" alt="Envios por Oca" class="mr-3 "></span>
            <span class="text-muted d-inline">
            {!!$ocaenvios['name']!!}
                <small>{{$ocaenvios['pago']}}</small>
            </span>
            <span class="float-right d-inline">$ {{$ocaenvios['cost']}}</span>

          </li> --}}
      </ul>

  </div>

</div>
