@extends('layouts.app')

@section('content')

<section id="preguntas-frecuentes">
    <div class="container mt-5 py-4">

        <div class="row justify-content-center">
            <div class="col-12 col-sm-10 col-md-8 col-lg-6">
                <div class="card">
                    <div class="card-body ">
                        <img src="{{asset('imagenes/logos-iconos/preguntas-frecuentes.svg')}}" alt="QUIEROMISELLO"
                            height="90" class="my-2 d-block">
                        <h5 class="card-title pb-0 mb-0">Preguntas frecuentes</h5>
                        <p class="card-text mb-2 text-muted">Cualquier otra duda contactanos por facebook</p>

                        <div class="accordion" id="accordionExample">
                            {{-- PEDIDO --}}
                            <div class="card">
                                <div class="card-header pl-2" id="pedido-heading">
                                    <h2 class="mb-0 ">
                                        <button class="btn btn-link text-left" type="button" data-toggle="collapse"
                                            data-target="#pedido" aria-expanded="false" aria-controls="pedido"> Como
                                            hago un pedido?
                                        </button>
                                    </h2>
                                </div>

                                <div id="pedido" class="collapse " aria-labelledby="pedido-heading"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            Manos a la obra, vas a necesitar papel y lápiz. <br>
                                            <ul>
                                                <li>Anota el esquema de set que elegiste con las cantidades y tamaños de
                                                    sellos que trae. </li>
                                                <li>Anda anotando los códigos de los sellos que gustan para completar el
                                                    esquema. </li>
                                                <li>Cuando terminaste nos lo mandas por facebook o instagram asi
                                                    registramos tu pedido y te
                                                    pasamos el ticket de pago. </li>
                                            </ul>
                                            Cualquier duda que tengas preguntanos en <a
                                                href="https://www.facebook.com/quieromisello">Facebook</a> o <a
                                                href="https://www.instagram.com/quieromisello">Instagram</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            {{-- SETS --}}
                            <div class="card">
                                <div class="card-header pl-2" id="sets-heading">
                                    <h2 class="mb-0 ">
                                        <button class="btn btn-link text-left" type="button" data-toggle="collapse"
                                            data-target="#sets" aria-expanded="false" aria-controls="sets"> Como es el
                                            tema de los sets?
                                        </button>
                                    </h2>
                                </div>

                                <div id="sets" class="collapse " aria-labelledby="sets-heading"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            Tenes 3 tamaños de set y varios esquemas para armalos.
                                            Todos traen una almohadilla entintada y vienen en caja de plástico o
                                            acrílico. <br>
                                            Los sellos son de 3 tamaños:
                                            <ul>
                                                <li>
                                                    Sello <span class="fa-layers fa-fw" style=""> <i
                                                            class="fa fa-square  text-danger" data-fa-transform="grow-6"
                                                            style="color:"></i> <span class="fa-layers-text fa-inverse"
                                                            data-fa-transform="" style="font-weight:800">L</span>
                                                    </span> de <span class="text-muted">4x4cm</span>
                                                </li>
                                                <li>
                                                    Sello <span class="fa-layers fa-fw" style=""> <i
                                                            class="fa fa-square  text-info" data-fa-transform="grow-6"
                                                            style="color:"></i> <span class="fa-layers-text fa-inverse"
                                                            data-fa-transform="" style="font-weight:800">M</span>
                                                    </span> de <span class="text-muted">4x2cm</span>
                                                </li>
                                                <li>
                                                    Sello <span class="fa-layers fa-fw" style=""> <i
                                                            class="fa fa-square  text-success"
                                                            data-fa-transform="grow-6" style="color:"></i> <span
                                                            class="fa-layers-text fa-inverse" data-fa-transform=""
                                                            style="font-weight:800">S</span> </span> de <span
                                                        class="text-muted">2x2cm</span>
                                                </li>
                                            </ul>

                                        </p>
                                    </div>
                                </div>
                            </div>
                            {{-- ENVIOS PAGOS --}}
                            <div class="card">
                                <div class="card-header pl-2" id="enviospago-heading">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link text-left" type="button" data-toggle="collapse"
                                            data-target="#enviospago" aria-expanded="false" aria-controls="enviospago">
                                            Como los pago y como hacen los envíos?
                                        </button>
                                    </h2>
                                </div>

                                <div id="enviospago" class="collapse " aria-labelledby="sets-heading"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Para pagarlos te pasamos un ticket de MercadoPago, completas tus datos, tu
                                        domicilio. Podes pagarlos con
                                        débito,
                                        crédito, transferencia, en RapiPago o PagoFacil. <br><br>
                                        Los envíos son por OCA, por el momento MercadoPago solo permite a domicilio.
                                        El envio es cargo del comprador podemos enviarte todos los sets que quieras en
                                        un solo paquete.
                                        Los sellos y el envío se pagan juntos en el mismo ticket de pago
                                        <a href="{{route('envios')}}">Consultar el costo del envío</a>
                                    </div>
                                </div>
                            </div>
                            {{-- CUANDO LLEGAN --}}
                            <div class="card">
                                <div class="card-header pl-2" id="cuandollegan-heading">
                                    <h2 class="mb-0 ">
                                        <button class="btn btn-link text-left" type="button" data-toggle="collapse"
                                            data-target="#cuandollegan" aria-expanded="false"
                                            aria-controls="cuandollegan">Cuando me llegan?
                                        </button>
                                    </h2>
                                </div>

                                <div id="cuandollegan" class="collapse " aria-labelledby="cuandollegan-heading"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            Hacemos el envío dentro de los 5 días hábiles de recibido el pago. Los
                                            pedidos grandes tienen una
                                            demora adicional
                                            de 2 días.
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>


                        {{-- https://www.kimidori.es/content/5-pago-seguro-financiacion --}}

                    </div>
                </div>
            </div>
        </div>

    </div>


</section>

@endsection
