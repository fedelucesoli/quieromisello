@extends('layouts.app')
@section('titulo', '' )

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Sigla</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Calle</th>
                            <th scope="col">Cod. Postal</th>
                            <th scope="col">Localidad</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>133</td>
                            <td>BNO</td>
                            <td>AV.ALVAREZ THOMAS </td>
                            <td>ALVAREZ THOMAS 1440 </td>
                            <td>1375 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>27</td>
                            <td>AZL</td>
                            <td>AZUL </td>
                            <td>RUTA NAC. Nº 3 503 </td>
                            <td>7300 </td>
                            <td>AZUL </td>
                        </tr>
                        <tr>
                            <td>28</td>
                            <td>BHI</td>
                            <td>BAHIA BLANCA </td>
                            <td>PUEYRREDON 262 </td>
                            <td>8000 </td>
                            <td>BAHIA BLANCA </td>
                        </tr>
                        <tr>
                            <td>70</td>
                            <td>BCE</td>
                            <td>BALCARCE </td>
                            <td>CALLE 21 734 </td>
                            <td>7620 </td>
                            <td>BALCARCE </td>
                        </tr>
                        <tr>
                            <td>29</td>
                            <td>BRC</td>
                            <td>BARILOCHE </td>
                            <td>VICE ALTE. O. CONNOR 1031 </td>
                            <td>8400 </td>
                            <td>BARILOCHE </td>
                        </tr>
                        <tr>
                            <td>107</td>
                            <td>BVL</td>
                            <td>BELL VILLE </td>
                            <td>AV.ESPAÑA 20 </td>
                            <td>2550 </td>
                            <td>BELL VILLE </td>
                        </tr>
                        <tr>
                            <td>125</td>
                            <td>BRA</td>
                            <td>BRAGADO </td>
                            <td>LAVALLE 66 </td>
                            <td>6640 </td>
                            <td>BRAGADO </td>
                        </tr>
                        <tr>
                            <td>130</td>
                            <td>FTE</td>
                            <td>CALAFATE </td>
                            <td>17 DE OCTUBRE 50 </td>
                            <td>9405 </td>
                            <td>EL CALAFATE </td>
                        </tr>
                        <tr>
                            <td>128</td>
                            <td>CVI</td>
                            <td>CALETA OLIVIA </td>
                            <td>LAVALLE 58 </td>
                            <td>9011 </td>
                            <td>CALETA OLIVIA </td>
                        </tr>
                        <tr>
                            <td>34</td>
                            <td>CMP</td>
                            <td>CAMPANA </td>
                            <td>GUEMES 730 </td>
                            <td>2804 </td>
                            <td>CAMPANA </td>
                        </tr>
                        <tr>
                            <td>910</td>
                            <td>CDG</td>
                            <td>CAÑADA DE GOMEZ</td>
                            <td>ESPAÑA 180</td>

                            <td>2500 </td>
                            <td>CAÑADA DE GOMEZ </td>

                        <tr>
                            <td>49</td>
                            <td>CTC</td>
                            <td>CATAMARCA </td>
                            <td>VILLEGAS 837 </td>
                            <td>4700 </td>
                            <td>SAN FDO.D.VALLE CATAMARCA</td>
                        </tr>
                        <tr>
                            <td>127</td>
                            <td>CHB</td>
                            <td>CHACABUCO </td>
                            <td>PRIMERA JUNTA 112 </td>
                            <td>6740 </td>
                            <td>CHACABUCO </td>
                        </tr>
                        <tr>
                            <td>31</td>
                            <td>CHA</td>
                            <td>CHASCOMUS </td>
                            <td>BELGRANO 252 </td>
                            <td>7130 </td>
                            <td>CHASCOMUS </td>
                        </tr>
                        <tr>
                            <td>140</td>
                            <td>ITO</td>
                            <td>CHILECITO </td>
                            <td>25 DE MAYO 444 </td>
                            <td>5360 </td>
                            <td>CHILECITO </td>
                        </tr>
                        <tr>
                            <td>61</td>
                            <td>COY</td>
                            <td>CHIVILCOY </td>
                            <td>AV 3 DE FEBRERO 384 </td>
                            <td>6620 </td>
                            <td>CHIVILCOY </td>
                        </tr>
                        <tr>
                            <td>32</td>
                            <td>CHO</td>
                            <td>CHOELE CHOEL </td>
                            <td>SAN MARTIN 824 </td>
                            <td>8360 </td>
                            <td>CHOELE CHOEL </td>
                        </tr>
                        <tr>
                            <td>146</td>
                            <td>C19</td>
                            <td>CI CASA CENTRAL </td>
                            <td>LA RIOJA 301 </td>
                            <td>1352 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>141</td>
                            <td>FL1</td>
                            <td>CI FLORES </td>
                            <td>FRAY CAYETANO RODRIGUEZ 27 </td>
                            <td>1375 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>139</td>
                            <td>CI5</td>
                            <td>CI JOSE HERNANDEZ </td>
                            <td>JOSE HERNANDEZ 2379 </td>
                            <td>1352 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>148</td>
                            <td>C20</td>
                            <td>CI MEXICO </td>
                            <td>Mexico  867 </td>
                            <td>1135 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>147</td>
                            <td>C21</td>
                            <td>CI ONCE </td>
                            <td>LARREA 619 </td>
                            <td>1352 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>1253</td>
                            <td>VCO</td>
                            <td>CI VILLA CONSTITUCIÓN</td>
                            <td>IRIGOYEN 201</td>

                            <td>2919 </td>
                            <td>VA.CONSTITUCION </td>

                        <tr>
                            <td>52</td>
                            <td>ETI</td>
                            <td>CIPOLLETTI </td>
                            <td>25 DE MAYO 233 </td>
                            <td>8324 </td>
                            <td>CIPOLLETTI </td>
                        </tr>
                        <tr>
                            <td>47</td>
                            <td>CRD</td>
                            <td>COMODORO RIVADAVIA </td>
                            <td>RIVADAVIA 150 </td>
                            <td>9000 </td>
                            <td>COMODORO RIVADAVIA </td>
                        </tr>
                        <tr>
                            <td>114</td>
                            <td>CDU</td>
                            <td>CONCEPCION DEL URUGUAY </td>
                            <td>SARMIENTO 2150 </td>
                            <td>3260 </td>
                            <td>CONCEPCION DEL URUGUAY </td>
                        </tr>
                        <tr>
                            <td>46</td>
                            <td>COC</td>
                            <td>CONCORDIA </td>
                            <td>H. IRIGOYEN 771 </td>
                            <td>3200 </td>
                            <td>CONCORDIA </td>
                        </tr>
                        <tr>
                            <td>40</td>
                            <td>COA</td>
                            <td>CORDOBA (COA)</td>
                            <td>JUAN B. JUSTO 5278</td>
                            <td>5000 </td>
                            <td>CORDOBA </td>
                        </tr>
                        <tr>
                            <td>41</td>
                            <td>COR</td>
                            <td>CORDOBA (COR)</td>
                            <td>LA RIOJA 1142</td>
                            <td>5000 </td>
                            <td>CORDOBA </td>
                        </tr>
                        <tr>
                            <td>48</td>
                            <td>CSU</td>
                            <td>CORONEL SUAREZ </td>
                            <td>MITRE 947 </td>
                            <td>7540 </td>
                            <td>CNEL.SUAREZ </td>
                        </tr>
                        <tr>
                            <td>35</td>
                            <td>CNQ</td>
                            <td>CORRIENTES </td>
                            <td>RUTA Nº12 Y AV. JUAN PERON s/n </td>
                            <td>3400 </td>
                            <td>CORRIENTES </td>
                        </tr>
                        <tr>
                            <td>42</td>
                            <td>EJE</td>
                            <td>CRUZ DEL EJE </td>
                            <td>ALVEAR 340 </td>
                            <td>5280 </td>
                            <td>CRUZ DEL EJE </td>
                        </tr>
                        <tr>
                            <td>122</td>
                            <td>VSF</td>
                            <td>Ctro. Operaciones Bs.As. </td>
                            <td>Mariano Ferreyra  302 </td>
                            <td>9960 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>30</td>
                            <td>CCO</td>
                            <td>CUTRAL-CO </td>
                            <td>DI PAOLO 502 </td>
                            <td>8322 </td>
                            <td>CUTRAL CO </td>
                        </tr>
                        <tr>
                            <td>138</td>
                            <td>DVT</td>
                            <td>DEVOTO </td>
                            <td>FAGNANO 3611 </td>
                            <td>1352 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>CI1</td>
                            <td>DIAGONAL CI </td>
                            <td>ROQUE SAENZ PEÑA 625 </td>
                            <td>1352 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>67</td>
                            <td>LOR</td>
                            <td>DOLORES </td>
                            <td>ARISTOBULO DEL VALLE 312</td>
                            <td>7100 </td>
                            <td>DOLORES </td>
                        </tr>
                        <tr>
                            <td>20</td>
                            <td>PQN</td>
                            <td>DON TORCUATO </td>
                            <td>OMBU 34 </td>
                            <td>1611 </td>
                            <td>DON TORCUATO </td>
                        </tr>
                        <tr>
                            <td>50</td>
                            <td>ELO</td>
                            <td>ELDORADO </td>
                            <td>AV. SAN MARTIN 2888 </td>
                            <td>3380 </td>
                            <td>ELDORADO </td>
                        </tr>
                        <tr>
                            <td>51</td>
                            <td>EQS</td>
                            <td>ESQUEL </td>
                            <td>MITRE 777 </td>
                            <td>9200 </td>
                            <td>ESQUEL </td>
                        </tr>
                        <tr>
                            <td>131</td>
                            <td>FLV</td>
                            <td>FLORENCIO VARELA </td>
                            <td>LAS AMERICAS 1684 </td>
                            <td>1888 </td>
                            <td>FLORENCIO VARELA </td>
                        </tr>
                        <tr>
                            <td>55</td>
                            <td>FMA</td>
                            <td>FORMOSA </td>
                            <td>POLICÍA DE TERRITORIOS NAC. 444 </td>
                            <td>3600 </td>
                            <td>FORMOSA </td>
                        </tr>
                        <tr>
                            <td>137</td>
                            <td>FM1</td>
                            <td>FORMOSA CENTRO </td>
                            <td>SAAVEDRA 518 </td>
                            <td>3600 </td>
                            <td>FORMOSA </td>
                        </tr>
                        <tr>
                            <td>58</td>
                            <td>GPO</td>
                            <td>GENERAL PICO </td>
                            <td>CALLE 37 606 </td>
                            <td>6360 </td>
                            <td>GRAL.PICO </td>
                        </tr>
                        <tr>
                            <td>57</td>
                            <td>GNR</td>
                            <td>GENERAL ROCA </td>
                            <td>ROCH DALE 36 </td>
                            <td>8332 </td>
                            <td>GRAL.ROCA </td>
                        </tr>
                        <tr>
                            <td>79</td>
                            <td>OYA</td>
                            <td>GOYA </td>
                            <td>AV. JOSE J. ROLON 417 </td>
                            <td>3450 </td>
                            <td>GOYA </td>
                        </tr>
                        <tr>
                            <td>56</td>
                            <td>GHU</td>
                            <td>GUALEGUAYCHU </td>
                            <td>SANTIAGO DIAZ 18 </td>
                            <td>2820 </td>
                            <td>GUALEGUAYCHU </td>
                        </tr>
                        <tr>
                            <td>43</td>
                            <td>JMA</td>
                            <td>JESUS MARIA </td>
                            <td>PEDRO J FRIAS (NORTE) 519 </td>
                            <td>5220 </td>
                            <td>JESUS MARIA </td>
                        </tr>
                        <tr>
                            <td>66</td>
                            <td>JUJ</td>
                            <td>JUJUY </td>
                            <td>SAN MARTÍN 276 </td>
                            <td>4600 </td>
                            <td>JUJUY </td>
                        </tr>
                        <tr>
                            <td>64</td>
                            <td>JNI</td>
                            <td>JUNIN </td>
                            <td>BENITO DE MIGUEL 629 </td>
                            <td>6000 </td>
                            <td>JUNIN </td>
                        </tr>
                        <tr>
                            <td>88</td>
                            <td>LCA</td>
                            <td>LA CARLOTA </td>
                            <td>HIPÓLITO YRIGOYEN 240 </td>
                            <td>2670 </td>
                            <td>LA CARLOTA </td>
                        </tr>
                        <tr>
                            <td>1197</td>
                            <td>LP1</td>
                            <td>LA PLATA (D. 73)</td>
                            <td>DIAGONAL 73 60</td>
                            <td>1900 </td>
                            <td>LA PLATA </td>
                        </tr>
                        <tr>
                            <td>60</td>
                            <td>IRJ</td>
                            <td>LA RIOJA </td>
                            <td>AV. PRES. CARLOS SAÚL MENEM 753</td>
                            <td>5300 </td>
                            <td>LA RIOJA </td>
                        </tr>
                        <tr>
                            <td>89</td>
                            <td>LYE</td>
                            <td>LABOULAYE </td>
                            <td>AVELLANEDA 126 </td>
                            <td>6120 </td>
                            <td>LABOULAYE </td>
                        </tr>
                        <tr>
                            <td>1313</td>
                            <td>LFE</td>
                            <td>LAFERRERE </td>
                            <td>J. M. DE ROSAS 10890</td>
                            <td>1757 </td>
                            <td>LAFERRERE </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>AVE</td>
                            <td>LANÚS</td>
                            <td>R. DE ESCALADA DE S. MARTÍN 689 </td>
                            <td>1824 </td>
                            <td>LANUS </td>
                        </tr>
                        <tr>
                            <td>73</td>
                            <td>SMT</td>
                            <td>LIB.GRAL.SAN MARTIN </td>
                            <td>T. THOMAS 307</td>
                            <td>5570 </td>
                            <td>LIB.GRL.SAN MARTIN </td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>LIR</td>
                            <td>LINIERS </td>
                            <td>AV. RIVADAVIA 10608</td>
                            <td>1375 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>136</td>
                            <td>LBO</td>
                            <td>LOBOS </td>
                            <td>ALBERDI 212 </td>
                            <td>7240 </td>
                            <td>LOBOS </td>
                        </tr>
                        <tr>
                            <td>15</td>
                            <td>LZM</td>
                            <td>LOMAS DE ZAMORA </td>
                            <td>AV. HIPÓLITO IRIGOYEN 8562 </td>
                            <td>1832 </td>
                            <td>LOMAS DE ZAMORA </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>ADG</td>
                            <td>LUIS GUILLON </td>
                            <td>BOULEVARD BS. AS. 1459</td>
                            <td>1838 </td>
                            <td>LUIS GUILLON </td>
                        </tr>
                        <tr>
                            <td>68</td>
                            <td>LUJ</td>
                            <td>LUJAN </td>
                            <td>ITALIA 1034 </td>
                            <td>6700 </td>
                            <td>LUJAN </td>
                        </tr>
                        <tr>
                            <td>71</td>
                            <td>MDQ</td>
                            <td>MAR D PLATA </td>
                            <td>CHAMPAGNAT 2371 </td>
                            <td>7600 </td>
                            <td>MAR DEL PLATA </td>
                        </tr>
                        <tr>
                            <td>126</td>
                            <td>MD1</td>
                            <td>MAR D PLATA CENTRO </td>
                            <td>INDEPENDENCIA 2044 </td>
                            <td>7600 </td>
                            <td>MAR DEL PLATA </td>
                        </tr>
                        <tr>
                            <td>108</td>
                            <td>MJU</td>
                            <td>MARCOS JUAREZ </td>
                            <td>AV. BELGRANO 815 </td>
                            <td>2580 </td>
                            <td>MARCOS JUAREZ </td>
                        </tr>
                        <tr>
                            <td>144</td>
                            <td>MZ2</td>
                            <td>MENDOZA </td>
                            <td>CERVANTES 1287 </td>
                            <td>5501 </td>
                            <td>GODOY CRUZ </td>
                        </tr>
                        <tr>
                            <td>117</td>
                            <td>MDZ</td>
                            <td>MENDOZA1 </td>
                            <td>JUAN B JUSTO 130 </td>
                            <td>5500 </td>
                            <td>MENDOZA </td>
                        </tr>
                        <tr>
                            <td>118</td>
                            <td>DES</td>
                            <td>MERCEDES </td>
                            <td>CALLE 33  843 </td>
                            <td>6600 </td>
                            <td>MERCEDES </td>
                        </tr>
                        <tr>
                            <td>134</td>
                            <td>MNO</td>
                            <td>MORENO </td>
                            <td>AV. FRANCISCO PIOVANO 2962 </td>
                            <td>1744 </td>
                            <td>MORENO </td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>PQO</td>
                            <td>MORON </td>
                            <td>AV. HIPÓLITO YRIGOYEN 117 </td>
                            <td>1708 </td>
                            <td>MORON </td>
                        </tr>
                        <tr>
                            <td>113</td>
                            <td>C12</td>
                            <td>MORON CI </td>
                            <td>9 DE JULIO 234 </td>
                            <td>1708 </td>
                            <td>MORON </td>
                        </tr>
                        <tr>
                            <td>72</td>
                            <td>NEC</td>
                            <td>NECOCHEA </td>
                            <td>CALLE 64 3164 </td>
                            <td>7630 </td>
                            <td>NECOCHEA </td>
                        </tr>
                        <tr>
                            <td>76</td>
                            <td>NQN</td>
                            <td>NEUQUEN </td>
                            <td>FELIX SAN MARTIN 1450 </td>
                            <td>8300 </td>
                            <td>NEUQUEN </td>
                        </tr>
                        <tr>
                            <td>63</td>
                            <td>JIO</td>
                            <td>NUEVE DE JULIO </td>
                            <td>HIPÓLITO YRIGOYEN 1111 </td>
                            <td>6500 </td>
                            <td>9 DE JULIO </td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>CI7</td>
                            <td>OBELISCO CI </td>
                            <td>CERRITO 404 </td>
                            <td>1465 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>77</td>
                            <td>OBR</td>
                            <td>OBERA </td>
                            <td>GOBERNADOR BARREYRO 1179 </td>
                            <td>3360 </td>
                            <td>OBERA </td>
                        </tr>
                        <tr>
                            <td>78</td>
                            <td>OLV</td>
                            <td>OLAVARRIA </td>
                            <td>AVENIDA DEL VALLE 4168 </td>
                            <td>7400 </td>
                            <td>OLAVARRIA </td>
                        </tr>
                        <tr>
                            <td>909</td>
                            <td>IVO</td>
                            <td>OLIVOS </td>
                            <td>BLAS PARERA 2597 </td>
                            <td>1636 </td>
                            <td>OLIVOS </td>
                        </tr>
                        <tr>
                            <td>99</td>
                            <td>ORA</td>
                            <td>ORAN </td>
                            <td>20 DE FEBRERO 242 </td>
                            <td>4530 </td>
                            <td>ORAN </td>
                        </tr>
                        <tr>
                            <td>143</td>
                            <td>PCH</td>
                            <td>PACHECO </td>
                            <td>HIPOLITO YRIGOYEN 1607 </td>
                            <td>1617 </td>
                            <td>GRAL.PACHECO </td>
                        </tr>
                        <tr>
                            <td>18</td>
                            <td>PLO</td>
                            <td>PALERMO </td>
                            <td>GODOY CRUZ 2746 </td>
                            <td>1352 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>123</td>
                            <td>MSP</td>
                            <td>PAQ CAPITAL FEDERAL </td>
                            <td>AV Coronel Roca  3450 </td>
                            <td>1437 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>81</td>
                            <td>PRA</td>
                            <td>PARANA </td>
                            <td>ALMAFUERTE 1504 </td>
                            <td>3100 </td>
                            <td>PARANA </td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>FLS</td>
                            <td>PARQUE CENTENARIO </td>
                            <td>AMBROSETTI 768 </td>
                            <td>1375 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>25</td>
                            <td>AOL</td>
                            <td>PASO DE LOS LIBRES </td>
                            <td>BELGRANO 1060 </td>
                            <td>3230 </td>
                            <td>PASO DE LOS LIBRES </td>
                        </tr>
                        <tr>
                            <td>82</td>
                            <td>PRS</td>
                            <td>PCIA. ROQUE SAENZ PEÑA </td>
                            <td>RIVADAVIA 801</td>
                            <td>3700 </td>
                            <td>PRES.ROQUE SAENZ PEÑA </td>
                        </tr>
                        <tr>
                            <td>62</td>
                            <td>INO</td>
                            <td>PERGAMINO </td>
                            <td>ITALIA 471 </td>
                            <td>2700 </td>
                            <td>PERGAMINO </td>
                        </tr>
                        <tr>
                            <td>36</td>
                            <td>PIA</td>
                            <td>PILAR</td>
                            <td>AV. COLECTORA 12 DE OCTUBRE 850</td>
                            <td>1629 </td>
                            <td>PILAR </td>
                        </tr>
                        <tr>
                            <td>132</td>
                            <td>PNM</td>
                            <td>PINAMAR </td>
                            <td>BUNGE 1596 </td>
                            <td>7167 </td>
                            <td>PINAMAR </td>
                        </tr>
                        <tr>
                            <td>83</td>
                            <td>PSS</td>
                            <td>POSADAS </td>
                            <td>NEUQUEN 2310</td>
                            <td>3300 </td>
                            <td>POSADAS </td>
                        </tr>
                        <tr>
                            <td>1408</td>
                            <td>PSQ</td>
                            <td>POSADAS CENTRO</td>
                            <td>AYACUCHO 2267</td>
                            <td>3300 </td>
                            <td>POSADAS </td>
                        </tr>
                        <tr>
                            <td>59</td>
                            <td>IGR</td>
                            <td>PUERTO IGUAZU </td>
                            <td>CORREA LUNA 250 </td>
                            <td>3370 </td>
                            <td>PTO.IGUAZU </td>
                        </tr>
                        <tr>
                            <td>111</td>
                            <td>PMY</td>
                            <td>PUERTO MADRYN </td>
                            <td>BELGRANO 742 </td>
                            <td>9120 </td>
                            <td>PTO.MADRYN </td>
                        </tr>
                        <tr>
                            <td>135</td>
                            <td>PUN</td>
                            <td>PUNTA ALTA </td>
                            <td>25 DE MAYO 278 </td>
                            <td>8109 </td>
                            <td>PUNTA ALTA </td>
                        </tr>
                        <tr>
                            <td>19</td>
                            <td>QMS</td>
                            <td>QUILMES </td>
                            <td>LAVALLE 663 </td>
                            <td>1878 </td>
                            <td>QUILMES </td>
                        </tr>
                        <tr>
                            <td>85</td>
                            <td>RAF</td>
                            <td>RAFAELA </td>
                            <td>AV. LUIS FANTI 794 </td>
                            <td>2300 </td>
                            <td>RAFAELA </td>
                        </tr>
                        <tr>
                            <td>87</td>
                            <td>RCQ</td>
                            <td>RECONQUISTA </td>
                            <td>SAN LORENZO 845 </td>
                            <td>3560 </td>
                            <td>RECONQUISTA </td>
                        </tr>
                        <tr>
                            <td>91</td>
                            <td>RES</td>
                            <td>RESISTENCIA </td>
                            <td>RUTA 11 KM. 1006.5 S/N</td>
                            <td>3500 </td>
                            <td>RESISTENCIA </td>
                        </tr>
                        <tr>
                            <td>119</td>
                            <td>RCU</td>
                            <td>RIO CUARTO </td>
                            <td>AV. AMADEO SABATTINI 3600 </td>
                            <td>5800 </td>
                            <td>RIO CUARTO </td>
                        </tr>
                        <tr>
                            <td>92</td>
                            <td>RGL</td>
                            <td>RIO GALLEGOS </td>
                            <td>SUREDA 99 </td>
                            <td>9400 </td>
                            <td>RIO GALLEGOS </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>RGA</td>
                            <td>RIO GRANDE </td>
                            <td>MOYANO 418 </td>
                            <td>9420 </td>
                            <td>RIO GRANDE </td>
                        </tr>
                        <tr>
                            <td>44</td>
                            <td>TER</td>
                            <td>RIO TERCERO </td>
                            <td>AV. SAVIO 1577 </td>
                            <td>5850 </td>
                            <td>RIO TERCERO </td>
                        </tr>
                        <tr>
                            <td>94</td>
                            <td>ROS</td>
                            <td>ROSARIO </td>
                            <td>SANTIAGO 380 </td>
                            <td>2000 </td>
                            <td>ROSARIO </td>
                        </tr>
                        <tr>
                            <td>93</td>
                            <td>RO1</td>
                            <td>ROSARIO CI </td>
                            <td>CORRIENTES 746 </td>
                            <td>2000 </td>
                            <td>ROSARIO </td>
                        </tr>
                        <tr>
                            <td>100</td>
                            <td>SLA</td>
                            <td>SALTA </td>
                            <td>CASEROS 1332 </td>
                            <td>4400 </td>
                            <td>SALTA </td>
                        </tr>
                        <tr>
                            <td>149</td>
                            <td>SAO</td>
                            <td>SAN ANTONIO OESTE </td>
                            <td>CALLE 6 DE ENERO 400 </td>
                            <td>8520 </td>
                            <td>SAN ANTONIO OESTE </td>
                        </tr>
                        <tr>
                            <td>53</td>
                            <td>FCO</td>
                            <td>SAN FRANCISCO </td>
                            <td>URQUIZA 874 </td>
                            <td>2400 </td>
                            <td>SAN FRANCISCO </td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>CI3</td>
                            <td>SAN ISIDRO CENTRO CI </td>
                            <td>BELGRANO 301 </td>
                            <td>1642 </td>
                            <td>SAN ISIDRO </td>
                        </tr>
                        <tr>
                            <td>120</td>
                            <td>UAQ</td>
                            <td>SAN JUAN </td>
                            <td>MENDOZA 2778 </td>
                            <td>5425 </td>
                            <td>VA.KRAUSSE </td>
                        </tr>
                        <tr>
                            <td>21</td>
                            <td>SJU</td>
                            <td>SAN JUSTO CI </td>
                            <td>PERÚ 2628 </td>
                            <td>1754 </td>
                            <td>SAN JUSTO </td>
                        </tr>
                        <tr>
                            <td>145</td>
                            <td>LZO</td>
                            <td>SAN LORENZO </td>
                            <td>DORREGO 1598 </td>
                            <td>2200 </td>
                            <td>SAN LORENZO </td>
                        </tr>
                        <tr>
                            <td>69</td>
                            <td>LUQ</td>
                            <td>SAN LUIS </td>
                            <td>PRINGLES 1740 </td>
                            <td>5700 </td>
                            <td>SAN LUIS </td>
                        </tr>
                        <tr>
                            <td>23</td>
                            <td>SMN</td>
                            <td>SAN MARTIN </td>
                            <td>COCHABAMBA 2308</td>
                            <td>1650 </td>
                            <td>SAN MARTIN </td>
                        </tr>
                        <tr>
                            <td>102</td>
                            <td>SMA</td>
                            <td>SAN MARTIN DE LOS ANDES </td>
                            <td>AV. KOESSLER 1495</td>
                            <td>8370 </td>
                            <td>SAN MARTIN DE LOS ANDES </td>
                        </tr>
                        <tr>
                            <td>22</td>
                            <td>SMG</td>
                            <td>SAN MIGUEL </td>
                            <td>CONCEJAL TRIBULATO 951 </td>
                            <td>1663 </td>
                            <td>SAN MIGUEL </td>
                        </tr>
                        <tr>
                            <td>75</td>
                            <td>NIC</td>
                            <td>SAN NICOLAS </td>
                            <td>ESPAÑA 280 </td>
                            <td>2900 </td>
                            <td>SAN NICOLAS </td>
                        </tr>
                        <tr>
                            <td>129</td>
                            <td>SPD</td>
                            <td>SAN PEDRO </td>
                            <td>OLIVEIRA CEZAR 25 </td>
                            <td>2930 </td>
                            <td>SAN PEDRO </td>
                        </tr>
                        <tr>
                            <td>24</td>
                            <td>AFA</td>
                            <td>SAN RAFAEL </td>
                            <td>SAAVEDRA 56 </td>
                            <td>5600 </td>
                            <td>SAN RAFAEL </td>
                        </tr>
                        <tr>
                            <td>98</td>
                            <td>SFN</td>
                            <td>SANTA FE </td>
                            <td>MENDOZA 3343 </td>
                            <td>3000 </td>
                            <td>SANTA FE </td>
                        </tr>
                        <tr>
                            <td>96</td>
                            <td>RSA</td>
                            <td>SANTA ROSA </td>
                            <td>DANTE ALIGHIERI 1346 </td>
                            <td>6300 </td>
                            <td>STA.ROSA (SANTA ROSA) </td>
                        </tr>
                        <tr>
                            <td>97</td>
                            <td>SDE</td>
                            <td>SANTIAGO DEL ESTERO </td>
                            <td>HIPOLITO YRIGOYEN 880 </td>
                            <td>4200 </td>
                            <td>SANTIAGO DEL ESTERO </td>
                        </tr>
                        <tr>
                            <td>86</td>
                            <td>SUN</td>
                            <td>SUNCHALES </td>
                            <td>MITRE 41 </td>
                            <td>2322 </td>
                            <td>SUNCHALES </td>
                        </tr>
                        <tr>
                            <td>103</td>
                            <td>TDL</td>
                            <td>TANDIL </td>
                            <td>RODRIGUEZ 1025 </td>
                            <td>7000 </td>
                            <td>TANDIL </td>
                        </tr>
                        <tr>
                            <td>101</td>
                            <td>TTG</td>
                            <td>TARTAGAL </td>
                            <td>PARAGUAY Y GUEMES 0 </td>
                            <td>4560 </td>
                            <td>TARTAGAL </td>
                        </tr>
                        <tr>
                            <td>90</td>
                            <td>REL</td>
                            <td>TRELEW </td>
                            <td>9 DE JULIO 235 </td>
                            <td>9100 </td>
                            <td>TRELEW </td>
                        </tr>
                        <tr>
                            <td>65</td>
                            <td>TQL</td>
                            <td>TRENQUE LAUQUEN </td>
                            <td>SIMINI 676 </td>
                            <td>6400 </td>
                            <td>TRENQUE LAUQUEN </td>
                        </tr>
                        <tr>
                            <td>80</td>
                            <td>OYO</td>
                            <td>TRES ARROYOS </td>
                            <td>AV. SAN MARTÍN 375 </td>
                            <td>7500 </td>
                            <td>TRES ARROYOS </td>
                        </tr>
                        <tr>
                            <td>104</td>
                            <td>TUC</td>
                            <td>TUCUMAN </td>
                            <td>PJE LOPEZ 3192 </td>
                            <td>4000 </td>
                            <td>TUCUMAN </td>
                        </tr>
                        <tr>
                            <td>74</td>
                            <td>YAN</td>
                            <td>TUNUYAN </td>
                            <td>R. SAENZ PEÑA 1169 </td>
                            <td>5560 </td>
                            <td>TUNUYAN </td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>USH</td>
                            <td>USHUAIA </td>
                            <td>MAIPU 790 </td>
                            <td>9410 </td>
                            <td>USHUAIA </td>
                        </tr>
                        <tr>
                            <td>110</td>
                            <td>VTO</td>
                            <td>VENADO TUERTO </td>
                            <td>12 DE OCTUBRE 806 </td>
                            <td>2600 </td>
                            <td>VENADO TUERTO </td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>CI4</td>
                            <td>VIAMONTE CI </td>
                            <td>VIAMONTE 526 </td>
                            <td>1375 </td>
                            <td>CAPITAL FEDERAL </td>
                        </tr>
                        <tr>
                            <td>105</td>
                            <td>VDM</td>
                            <td>VIEDMA </td>
                            <td>PRINGLES 83 </td>
                            <td>8500 </td>
                            <td>VIEDMA </td>
                        </tr>
                        <tr>
                            <td>45</td>
                            <td>VCP</td>
                            <td>VILLA CARLOS PAZ </td>
                            <td>Uruguay 108</td>
                            <td>5152 </td>
                            <td>VA.CARLOS PAZ </td>
                        </tr>
                        <tr>
                            <td>112</td>
                            <td>VDR</td>
                            <td>VILLA DOLORES </td>
                            <td>BELGRANO 21 </td>
                            <td>5870 </td>
                            <td>VA.DOLORES </td>
                        </tr>
                        <tr>
                            <td>106</td>
                            <td>VLG</td>
                            <td>VILLA GESELL </td>
                            <td>BUENOS AIRES  731 </td>
                            <td>7165 </td>
                            <td>VA.GESELL </td>
                        </tr>
                        <tr>
                            <td>109</td>
                            <td>VMA</td>
                            <td>VILLA MARIA </td>
                            <td>CARLOS PELLEGRINI 1006 </td>
                            <td>5900 </td>
                            <td>VA.MARIA (VILLA MARIA) </td>
                        </tr>
                        <tr>
                            <td>121</td>
                            <td>VME</td>
                            <td>VILLA MERCEDES </td>
                            <td>SAN MARTIN 319 </td>
                            <td>5730 </td>
                            <td>VA.MERCEDES </td>
                        </tr>
                        <tr>
                            <td>26</td>
                            <td>APZ</td>
                            <td>ZAPALA </td>
                            <td>AV. 12 DE JULIO 269 </td>
                            <td>8340 </td>
                            <td>ZAPALA </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

@endsection
