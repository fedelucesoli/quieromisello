<section class="container my-5">
    <div class="row mt-5">

        <div class="col-12">
            <h2 class="mb-0">Arma tu set</h2>
            <svg width="300" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 361 10.01">
                <defs>
                    <style>
                        .cls-1 {
                            fill: none;
                            stroke: #7cc5d5;
                            stroke-miterlimit: 10;
                            stroke-width: 4px;
                        }

                    </style>
                </defs>
                <g id="Capa_2" data-name="Capa 2">
                    <g id="Layer_1" data-name="Layer 1">
                        <path class="cls-1"
                            d="M361,8c-4.15,0-8.3-6-12.45-6s-8.3,6-12.45,6-8.29-6-12.44-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6S278,2,273.86,2s-8.3,6-12.45,6S253.12,2,249,2s-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6-8.29-6-12.44-6S166,8,161.83,8s-8.3-6-12.45-6-8.3,6-12.45,6-8.3-6-12.45-6S116.18,8,112,8s-8.29-6-12.44-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.44,6S29.05,2,24.9,2,16.6,8,12.45,8,4.15,2,0,2" />
                    </g>
                </g>
            </svg>

            <p class="lead mt-3">Hay muchas maneras dististas de combinar los sellos para que tu set sea bien tuyo!</p>
        </div>

        <div class="col-12 col-md-6 my-2">
            <div class=" card  shadow-sm  p-4">
            <h5 class="text-center mb-1">Tamaños de los sellos</h5>
            <hr>

                <div class="row text-center py-3 justify-content-around align-items-center">

                    <div class="col ">
                        <span class="lead mb-3">4x4cm</span>
                        <img src="{{ asset('img/sello-l.svg') }}" alt="">
                    </div>
                    <div class="col">
                        <span class="lead mb-3">4x2cm</span>
                        <img src="{{ asset('img/sello-m.svg') }}" alt="">
                    </div>
                    <div class="col">
                        <span class="lead mb-3">2x2cm</span>
                        <img src="{{ asset('img/sello-s.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 my-2">
            <div class=" card  shadow-sm  p-4">
            <h5 class="text-center mb-1">Tamaños de los sets</h5>
            <hr>

                <div class="row text-center py-3 justify-content-around align-items-center">

                    <div class="col ">
                        <span class="lead mb-3">Mediano</span>
                        <img src="{{ asset('imagenes/catalog/mediano-1.jpg') }}" alt="" class="img-fluid">
                    </div>
                    <div class="col">
                        <span class="lead mb-3">Gigante</span>
                        <img src="{{ asset('imagenes/catalog/gigante1.jpg') }}" alt="" class="img-fluid">
                    </div>
                    <div class="col">
                        <span class="lead mb-3">Mega</span>
                        <img src="{{ asset('imagenes/catalog/mega1.jpg') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>



        <div class="col-12 text-center mt-5">

                <a href="{{ route('categoria', 'arma-tu-set') }}" class="btn mr-auto btn-outline-primary btn-lg"> <span class="mr-2">&#x1F60D; </span> QUIERO MI SET!</a>

        </div>

        {{-- <div class="col-12 col-md-8">
            <div class="bg-light rounded tarjeta px-2 py-4 text-center">
                <img class="img-fluid rounded"
                    src="  {{ asset('imagenesraw/arma-tu-set-index.jpg') }}"
                    alt="Sellos de correción">
            </div>
        </div> --}}

    </div>

</section>
{{--
<section class="container">

    <div class="row">

        <div class="col-12 text-center">
            <h1 class="text-danger">Sellos divertidos para corregir!</h1>
            <p class="lead text-muted"> Super prácticos para llevar en la mochila a todos lados!! Diseñamos y fabricamos
                sellos de goma para corrección docente. Nuestra meta es hacerte la vida mas fácil!</p>
        </div>
        <div class="col-12">
            @include('front.includes.carousel')
        </div>

    </div>


    <div class="row mt-5">
        <div class="col-12 col-md-4 mb-5">
            <h2 class="mb-0">Arma tu set</h2>
            <svg width="50%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 361 10.01">
                <defs>
                    <style>
                        .cls-1 {
                            fill: none;
                            stroke: #7cc5d5;
                            stroke-miterlimit: 10;
                            stroke-width: 4px;
                        }

                    </style>
                </defs>
                <g id="Capa_2" data-name="Capa 2">
                    <g id="Layer_1" data-name="Layer 1">
                        <path class="cls-1"
                            d="M361,8c-4.15,0-8.3-6-12.45-6s-8.3,6-12.45,6-8.29-6-12.44-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6S278,2,273.86,2s-8.3,6-12.45,6S253.12,2,249,2s-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6-8.29-6-12.44-6S166,8,161.83,8s-8.3-6-12.45-6-8.3,6-12.45,6-8.3-6-12.45-6S116.18,8,112,8s-8.29-6-12.44-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.45,6-8.3-6-12.45-6-8.3,6-12.44,6S29.05,2,24.9,2,16.6,8,12.45,8,4.15,2,0,2" />
                    </g>
                </g>
            </svg>
            <p class="lead mt-3">Hay muchas maneras dististas de combinar los sellos para que tu set sea bien tuyo!</p>

            <h5>Tamaños de set</h5>

            <ul>
                <li>
                    Sello <span class="fa-layers fa-fw" style=""> <i class="fa fa-square  text-danger"
                            data-fa-transform="grow-6" style="color:"></i> <span class="fa-layers-text fa-inverse"
                            data-fa-transform="" style="font-weight:800">L</span> </span> de <span
                        class="text-muted">4x4cm</span>
                </li>
                <li>
                    Sello <span class="fa-layers fa-fw" style=""> <i class="fa fa-square  text-info"
                            data-fa-transform="grow-6" style="color:"></i> <span class="fa-layers-text fa-inverse"
                            data-fa-transform="" style="font-weight:800">M</span> </span> de <span
                        class="text-muted">4x2cm</span>
                </li>
                <li>
                    Sello <span class="fa-layers fa-fw" style=""> <i class="fa fa-square  text-success"
                            data-fa-transform="grow-6" style="color:"></i> <span class="fa-layers-text fa-inverse"
                            data-fa-transform="" style="font-weight:800">S</span> </span> de <span
                        class="text-muted">2x2cm</span>
                </li>
            </ul>



            <p class="text-center mt-5">

                <a href="{{ route('categoria', 'arma-tu-set') }}"
                    class="btn mr-auto btn-primary btn-lg"> &#x1F60D; QUIERO MI SET!</a>
            </p>
        </div>
        <div class="col-12 col-md-8">
            <div class="bg-light rounded tarjeta px-2 py-4 text-center">
                <img class="img-fluid rounded"
                    src="  {{ asset('imagenesraw/arma-tu-set-index.jpg') }}"
                    alt="Sellos de correción">
            </div>
        </div>

    </div>

</section> --}}
