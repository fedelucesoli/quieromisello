@extends('layouts.app') 
@section('titulo', 'Ventas por mayor')

@section('content')

<div class="container mt-5  py-4" id="mayoristas" v-cloak>

    <div class="row justify-content-center">
        <div class="col-12 col-sm-10 col-md-8 col-lg-5">
            <div class="card">
                <div class="card-body ">
                    
                    <form class="text-center px-3 py-3" action="#!" v-show="form.showForm">

                        <p class="h4 mb-2">Mayoristas</p>
                        <p class="card-text mb-4 text-muted">Dejanos tus datos y te contactamos.</p>

                        <transition 
                        enter-active-class="animate__animated animate__fadeIn"
                        leave-active-class="animate__animated animate__fadeOut">

                            <div class="alert alert-danger my-2" v-show="form.error" role="alert">
                                <p id="" class="form-text text-muted mb-0">@{{ form.errortxt }}</p>
                            </div>
                        </transition>
                        
                        <transition 
                        enter-active-class="animate__animated animate__fadeIn"
                        leave-active-class="animate__animated animate__fadeOut">

                            <div class="alert alert-success my-2" v-show="form.success" role="alert">
                                <p id="" class="form-text text-muted mb-0" v-html="form.html"></p>
                            </div>
                        </transition>

                        
                        <!-- nombre -->
                        <div class="form-group">
                            <input type="text" v-model="form.nombre" id="nombre" class="form-control form-control-lg"
                                placeholder="Nombre">
                        </div>

                        <!-- Ciudad -->
                        <div class="form-group">
                            <input type="text" v-model="form.ciudad" id="ciudad" class="form-control form-control-lg"
                                placeholder="Ciudad">
                        </div>

                        <!-- E-mail -->
                        <div class="form-group">
                            <input type="email" v-model="form.email" id="email" class="form-control form-control-lg"
                                placeholder="E-mail">
                        </div>

                        <!-- Phone number -->
                        <div class="form-group">
                            <input type="text" v-model="form.telefono" id="telefono" class="form-control form-control-lg"
                                placeholder="Telefono" aria-describedby="telefono">
                        </div>

                        <!-- Newsletter -->
                        {{-- <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="defaultRegisterFormNewsletter">
                            <label class="custom-control-label" for="defaultRegisterFormNewsletter">Subscribe to our newsletter</label>
                        </div> --}}

                        <button class="btn btn-outline-danger my-4 btn-lg d-block" v-on:click.prevent="enviarForm()"
                            v-html="form.textButton"></button>

                    </form>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@push('scripts')
    <script src="{{ asset('js/mayoristas.js') }}"></script>
@endpush
