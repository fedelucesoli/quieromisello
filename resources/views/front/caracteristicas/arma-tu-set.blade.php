<div class="row  justify-content-center">

    <div class="col-12 my-2">
        <div class="pasos" :class="{'completado': wizard.esquemaComplete}" data-toggle="modal" data-target="#esquemas">
            <div>Esquemas</div>
        </div>
    </div>

    <div class="col-12 my-2" v-for="paso in wizard.pasos" v-on:click="abrirModal(paso)">

        <div class="pasos" :class="{'completado': paso.completado}" >
            <div>@{{paso.nombre}}</div>
            <div v-if="wizard.esquemaComplete">
                @{{esquemaSeleccionado.contador(paso.tamaño)}}
            </div>
        </div>

    </div>

</div>

<div class="modal" tabindex="-1" id="esquemas">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Esquemas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">

            <div class="col-6 my-4 text-center"  v-for="(esquema, index) in esquemas" :data-id="esquema.id" v-on:click='selectEsquema(index)'>
                <div class=" esquemas" >
                    <p class="mb-1">@{{esquema.nombre}} - $ @{{esquema.precio}}</p>

                    <img
                    :src="'/imagenes/catalog/'+esquema.portada"
                    :alt="esquema.nombre"
                    class="img-fluid rounded esquema"
                    :class="{ 'activo': esquemaSeleccionado.id == esquema.id } "
                    >

                </div>
            </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-block btn-primary"  :disabled="!esquemaSeleccionado" v-on:click="siguientePaso">Siguiente</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" id="sellos">
  <div class="modal-dialog modal-dialog-scrollable">

    <div class="modal-content">

                <div class="d-relative " style="display: none">
                    <div class="alert alert-warning alert-dismissible " role="alert">
                        <span class="error"><i class="bi bi-exclamation-diamond-fill"></i></span>
                        <span v-html="wizard.alert.texto"></span>
                    </div>
                </div>
      <div class="modal-header">
        <h5 class="modal-title">SELLOS </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-loading"></div>
      <div class="modal-body">
        <div class="row">

            <div class="col-4 col-sm-4 col-md-4 col-lg-3 my-2 px-2 sellos" :class="sello.tamanio" v-for="(sello, index) in sellos">
                <div
                v-bind:class="[{'activo': sello.datos }, 'card rounded-lg ', sello.tamanio]"
                v-on:click.prevent="selectSello(sello, index, sello.tamanio)">

                    <img class="lazyload card-img-top w-100" style="height: 100px" data-sizes="auto"
                        data-aspectratio="1/1"
                        :src="'/img/loader.svg'"
                        :data-src="'/img/sellos/'+ sello.portada"
                        :alt="sello.nombre">
                    <div class="card-img-overlay"> </div>
                    <div class="sello-numero "  v-bind:class="sello.tamanio"> @{{sello.nombre}} </div>
                </div>
            </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-block btn-primary" :disabled="!esquemaSeleccionado" v-on:click="siguientePaso">Siguiente</button>
      </div>
    </div>
  </div>

</div>

<div class="mt-5">

    <button
                class="btn p-3 btn-primary py-2 btn-block rounded"

    v-on:click="enviarPedido"
    >

        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-handbag mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M8 1a2 2 0 0 0-2 2v4.5a.5.5 0 0 1-1 0V3a3 3 0 0 1 6 0v4.5a.5.5 0 0 1-1 0V3a2 2 0 0 0-2-2z"></path>
        <path fill-rule="evenodd" d="M3.405 6a.5.5 0 0 0-.498.45l-.912 6.9A1.5 1.5 0 0 0 3.488 15h9.024a1.5 1.5 0 0 0 1.493-1.65l-.913-6.9a.5.5 0 0 0-.497-.45h-9.19zm-1.493.35A1.5 1.5 0 0 1 3.405 5h9.19a1.5 1.5 0 0 1 1.493 1.35L15 13.252A2.5 2.5 0 0 1 12.512 16H3.488A2.5 2.5 0 0 1 1 13.251l.912-6.9z"></path>
    </svg> AGREGAR AL PEDIDO</button>

</div>





@push('scripts')
    <script src="{{ asset('js/arma-tu-set1.js') }}"></script>
@endpush

