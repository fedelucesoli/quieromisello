<div class="mt-5">
    <div class="row">
        <div class="col-12">
            <h4>Que tiene que decir:</h4>
            <div class="input-group my-3">
            <input v-model="texto" class="form-control" placeholder="Seño Gabriela" aria-label="Tu nombre" aria-describedby="tu nombre" />

            </div>
        </div>

        <div class="col-12"><h4>Elegis la tipografia:</h4></div>
        <div v-for="n in 10" class="col-10 col-sm-6 my-2" v-on:click="seleccionarTipografia(n)" >
            <div class="d-flex p-3 bg-white rounded " v-bind:class="{'border border-primary' : tipografia === n }">
                <div class="align-self-center"><p class="mb-0 mr-4">@{{n}}</p></div>
                <div class="align-self-center">
                    <img class="lazy img-fluid"
                    :src="'/img/personalizado/tipografias/'+ n +'.png'"
                    alt="{{$alt ?? 'Quiero mi sello'}}">
                </div>
            </div>
        </div>

    </div>

    <div class="row mt-5">
        <div class="col-12"><h4>Y si queres un dibujito</h4></div>
        <div v-for="n in 15" class="col-6 col-sm-4 my-2" v-on:click="seleccionarDibujo(n)" >
            <div class="d-flex p-3 bg-white rounded" v-bind:class="{'border border-primary' : dibujo === n }">
                <div class="align-self-center"><p class="mb-0 mr-4">@{{n}}</p></div>
                <div class="align-self-center">
                    <img class="lazy img-fluid"
                    :src="'/img/personalizado/dibujos/'+ n +'.png'"
                    alt="{{$alt ?? 'Quiero mi sello'}}">
                </div>
            </div>
        </div>

        <div class="col-12 mt-5">

            <button
            class="btn p-3 btn-primary py-2 btn-block rounded"
            v-on:click="addtoCart"
            >

                <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-handbag mr-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M8 1a2 2 0 0 0-2 2v4.5a.5.5 0 0 1-1 0V3a3 3 0 0 1 6 0v4.5a.5.5 0 0 1-1 0V3a2 2 0 0 0-2-2z"></path>
                <path fill-rule="evenodd" d="M3.405 6a.5.5 0 0 0-.498.45l-.912 6.9A1.5 1.5 0 0 0 3.488 15h9.024a1.5 1.5 0 0 0 1.493-1.65l-.913-6.9a.5.5 0 0 0-.497-.45h-9.19zm-1.493.35A1.5 1.5 0 0 1 3.405 5h9.19a1.5 1.5 0 0 1 1.493 1.35L15 13.252A2.5 2.5 0 0 1 12.512 16H3.488A2.5 2.5 0 0 1 1 13.251l.912-6.9z"></path>
            </svg> AGREGAR AL PEDIDO</button>

        </div>
    </div>
</div>

@push('scripts')

    <script src="{{asset('js/sello-personal.js')}}"></script>

@endpush
