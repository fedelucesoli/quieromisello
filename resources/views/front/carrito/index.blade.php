@extends('layouts.app')

@section('titulo', 'Carrito de compras - Quiero mi sello')

@section('content')

{{-- {{dd(\Cart::getContent())}} --}}
        <div class="container mt-5">

            <div class="row ">
                <div class="col"><h3 class="text-center">Detalle de tu pedido! </h3></div>

                <div class="col-sm-12">
                    @if (Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                </div>
                <div class="col-12 mt-4 p-3 rounded  bg-white shadow">
                    @if (\Cart::isEmpty())
                    <div class="col text-center my-5">
                        <p class="lead">Todavia no agregaste ningun producto al pedido.</p>
                        <a href="{{url('/')}}" class="">Mira nuestros productos!</a>
                    </div>
                    @else


                    @foreach(\Cart::getContent() as $item)
                    <div class="row align-items-center mb-4">

                        <div class="col-3 col-md-2">
                            <img class="lazyload rounded img-fluid" style="" data-sizes="auto" src="{{asset('imagenes/thumbs/'. $item->associatedModel->portada)}}" data-srcset="{{asset('imagenes/full/'. $item->associatedModel->portada)}}" alt="{{$item->nombre}}">
                        </div>

                        <div class="col flex-grow">

                            <p class="lead mb-3">{{ $item->name }}</p>

                            @if($item->associatedModel->tipo_personalizable === "arma-tu-set")

                                <p>Esquema: {{Arr::get($item->attributes, 'esquema.nombre')}}</p>
                                <p>Sellos:
                                    @foreach($item->attributes['sellos'] as $key  => $value)
                                        {{$value['nombre'] }} -
                                    @endforeach
                                </p>
                            @else

                                @foreach($item->attributes as $key  => $value)
                                    <p > {{$key }} : {{$value}}</p>
                                @endforeach
                            @endif

                            {{-- <p class="cantidad">Cantidad: {{ $item->quantity }}</p> --}}
                        <a href="">Agregar otro</a>
                        </div>

                        <div class="col-3  text-right ml-auto">

                            <p class="d-inline">$ {{$item->getPriceSum()}}</p>
                            <a href="{{route('carrito.remove', ['id'=> $item->id])}}" type="submit" class="btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"> <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/> </svg>
                            </a>

                        </div>

                    </div>
                    <hr>
                    @endforeach
                    <div class="row justify-content-end">
                        <div class="col col-lg-3 text-lg-right">  Total: </div>
                        <div class="col col-lg-3 text-right ">  $ {{\Cart::getTotal()}} </div>
                    </div>
                        <hr>

                    <div class="row justify-content-end ">
                        <div class="col col-lg-3 text-lg-right mt-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-truck text-dark mx-1 " viewBox="0 0 16 16">
                            <path d="M0 3.5A1.5 1.5 0 0 1 1.5 2h9A1.5 1.5 0 0 1 12 3.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-3.998-.085A1.5 1.5 0 0 1 0 10.5v-7zm1.294 7.456A1.999 1.999 0 0 1 4.732 11h5.536a2.01 2.01 0 0 1 .732-.732V3.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .294.456zM12 10a2 2 0 0 1 1.732 1h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4zm-9 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                            </svg>
                            <span>  Calculá el costo de envío</span>
                        </div>
                        <div class="col-12 col-lg-3 mt-2 text-right">


                            <form method="POST" id="buscar-envios">
                                <div class="input-group">
                                    <input type="text" name="shipment_cost-cp" class="form-control" placeholder="Código Postal"
                                        aria-label="Código Postal" aria-describedby="codigopostal">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary bg-white" type="submit">CALCULAR</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div id="envios-resultados" class=" mt-3"></div>
                    </div>

                    <hr>
                    <div class="row py-3 justify-content-end">
                        <div class="col-12 col-lg-4">
                            <a href="{{url('checkout')}}" class="btn btn-block btn-primary">INICIAR PEDIDO</a>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-12 col-lg-4 text-center mt-2">
                            <a href="{{url('/')}}" class="text-dark">Seguir comprando</a>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>



@endsection

@push('scripts')
    <script src="{{ asset('js/buscar-envios.js') }}"></script>
@endpush
