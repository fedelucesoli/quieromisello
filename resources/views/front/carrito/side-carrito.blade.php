<div id="bs-canvas-right" class="bs-canvas bs-canvas-anim bs-canvas-right position-fixed h-100">
{{-- <div id="carrito"> --}}
    <div class="container clearfix">
        <div class="row">
            <button type="button" class="bs-canvas-close close" aria-label="Close"><span aria-hidden="true" class="text-light">&times;</span></button>
        </div>
            <h3>Detalle de tu pedido </h3>

            <div class="row">
                <div class="col-sm-12">
                    @if (Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif
                </div>
            </div>
            <div class="row mt-4 p-3 rounded bg-white shadow">
                <div class="col-12">
                    @if (\Cart::isEmpty())
                        <p class="my-5">Todavia no agregaste ningun producto al pedido.</p>
                    @else
                    <table class="table table-hover">
                        <thead class="text-muted text-small">
                        <tr>
                            <th scope="col" class=" border-0">PRODUCTO</th>
                            <th scope="col" width="180" class=" border-0">PRECIO</th>
                            <th scope="col" width="120" class=" border-0">CANTIDAD</th>
                            {{-- <th scope="col" class="text-right border-0" width="200">Acciones </th> --}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\Cart::getContent() as $item)
                            <tr>
                                <td>
                                    <figure class="media">
                                        <figcaption class="media-body">
                                            <h5 class="title text-truncate">{{ Str::words($item->name,20) }}</h5>
                                            <a href="{{ route('checkout.cart.remove', $item->id) }}" class="">Eliminar</a>
                                            @foreach($item->attributes as $key  => $value)
                                                <dl class="dlist-inline small">
                                                    <dt>{{ ucwords($key) }}: </dt>
                                                    <dd>{{ ucwords($value) }}</dd>
                                                </dl>
                                            @endforeach
                                        </figcaption>
                                    </figure>
                                </td>

                                <td>
                                    <div class="price-wrap">
                                        <p class="d-inline ">$ {{ $item->price }}</p>
                                        <small class="text-muted">c/u</small>
                                    </div>
                                </td>
                                <td>
                                    <var class="price">{{ $item->quantity }}</var>
                                </td>
                                <td class="text-right">

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>

            <div class="row align-content-end">
                <div class="col-6">TOTAL</div>
                <div class="col-6 text-right">200</div>

                {{-- <a href="{{ route('checkout.cart.clear') }}" class="btn btn-danger btn-block mb-4">Clear Cart</a> --}}
                {{-- <div class="col-12 col-md-6">
                    <p class="total">TOTAL: ${{ \Cart::getSubTotal() }} </p>
                    SUBROTAL :
                    ENVIO :
                    Total {{ \Cart::getTotal() }}
                    <a href="" class="btn btn-success btn-lg btn-block"> Envuar pedido</a>
                </div> --}}
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
                <div class="col-12"><h1 class="display-1">asdasd</h1> </div>
            </div>

        </div>
</div>
