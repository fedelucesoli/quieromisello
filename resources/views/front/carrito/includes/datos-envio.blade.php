<div class="col-12 mb-3" >
                    <h4>Datos para el envio</h4>
                    <hr>
                    <form method="post">
                        <div class="form-group">
                            <label for="calle">Calle</label>
                            <input type="text" name="calle" v-model="envio.direccion.calle" class="form-control" id="calle" aria-describedby="calle" v-on:blur="validar" data-regex="/^(?=.{1,30}$)(([a-zá-úÁ-ÚA-Z0-9](\.|\s|\,|\,\s)?)((\s|\s\-\s|\-|\.|\s\.|\s\.\s)[a-zá-úÁ-ÚA-Z0-9])?(\s)?)*$/" data-message="Ingrese una calle válida" data-required="1">
                            <small id="calle-msg" class="form-text text-muted invalid-feedback"></small>
                        </div>
                        <div class="form-group">
                            <label for="calle-numero">Numero</label>
                            <input type="text" name="calle_numero" v-model="envio.direccion.calle_numero" class="form-control" id="calle_numero" aria-describedby="calle-numero" v-on:blur="validar" data-regex="/^(?=.{1,10}$)(([a-zá-úÁ-ÚA-Z0-9](\.|\s|\,|\,\s|\/)?)((\s|\s\-\s|\-|\.|\s\.|\s\.\s|\s\/|\s\/\s)[a-zá-úÁ-ÚA-Z0-9])?(\s)?)*$/" data-message="Ingrese un número de calle válido" data-required="1">
                            <small id="calle-numero-msg" class="form-text text-muted invalid-feedback"></small>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="piso">Piso</label>
                                <input type="text" name="piso" v-model="envio.direccion.piso" class="form-control" id="piso" aria-describedby="piso" v-on:blur="validar" data-regex="/^(?=.{1,10}$)(([a-zá-úÁ-ÚA-Z0-9](\.|\s|\,|\,\s|\/)?)((\s|\s\-\s|\-|\.|\s\.|\s\.\s|\s\/|\s\/\s)[a-zá-úÁ-ÚA-Z0-9])?(\s)?)*$/" data-message="Ingrese un piso válido" data-required="0">
                                <small id="piso-msg" class="form-text text-muted invalid-feedback"></small>
                            </div>
                            <div class="form-group col-6">
                                <label for="depto">Depto</label>
                                <input type="text" name="depto" v-model="envio.direccion.depto" class="form-control" id="depto" aria-describedby="depto" v-on:blur="validar" data-regex="/^(?=.{1,10}$)(([a-zá-úÁ-ÚA-Z0-9](\.|\s|\,|\,\s|\/)?)((\s|\s\-\s|\-|\.|\s\.|\s\.\s|\s\/|\s\/\s)[a-zá-úÁ-ÚA-Z0-9])?(\s)?)*$/" data-message="Ingrese un departamento válido" data-required="0">
                                <small id="depto-msg" class="form-text text-muted invalid-feedback"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="domicilio_adicionales">Datos adicionales</label>
                            <input type="text" name="domicilio_adicionales" v-model="envio.direccion.domicilio_adicionales" class="form-control" id="domicilio_adicionales" aria-describedby="domicilio_adicionales" v-on:blur="validar" data-regex="/^(?=.{1,10}$)(([a-zá-úÁ-ÚA-Z0-9](\.|\s|\,|\,\s|\/)?)((\s|\s\-\s|\-|\.|\s\.|\s\.\s|\s\/|\s\/\s)[a-zá-úÁ-ÚA-Z0-9])?(\s)?)*$/" data-message="Ingrese un número de calle válido">
                            <small id="domicilio_adicionales-msg" class="form-text text-muted invalid-feedback"></small>
                        </div>
                        <div class="form-group">
                            <label for="ciudad">Ciudad</label>
                            <input type="text" name="ciudad" v-model="envio.direccion.ciudad" class="form-control" id="ciudad" aria-describedby="ciudad" v-on:blur="validar" data-regex="/^(?=.{1,30}$)(([a-zá-úÁ-ÚA-Z0-9](\.|\s|\,|\,\s)?)((\s|\s\-\s|\-|\.|\s\.|\s\.\s)[a-zá-úÁ-ÚA-Z0-9])?(\s)?)*$/" data-message="Ingrese una ciudad válida" data-required="1">
                            <small id="ciudad-msg" class="form-text text-muted invalid-feedback"></small>
                        </div>
                        <div class="form-group">
                            <label for="codigo-postal">Código Postal</label>
                            <input type="number" name="codigo_postal" v-model="envio.direccion.codigo_postal" class="form-control" id="codigo_postal" aria-describedby="Codigo Postal" v-on:blur="validar" data-regex="/^[1-9]{1}[0-9]{3}$/" data-message="Ingrese un código postal válido " data-required="1">
                            <small id="codigo-postal-msg" class="form-text text-muted invalid-feedback"></small>
                        </div>

                        <div class="form-group">
                            <label for="provincia">Provincia</label>
                            <select name="provincia" v-model="envio.direccion.provincia" id="provincia" class="custom-select" data-required="1">
                                <option value="BUENOS AIRES">Buenos Aires</option>
                                <option value="CAPITAL FEDERAL">Capital Federal</option>
                                <option value="CATAMARCA">Catamarca</option>
                                <option value="CHACO">Chaco</option>
                                <option value="CHUBUT">Chubut</option>
                                <option value="CORDOBA">Córdoba</option>
                                <option value="CORRIENTES">Corrientes</option>
                                <option value="ENTRE RIOS">Entre Rios</option>
                                <option value="FORMOSA">Formosa</option>
                                <option value="JUJUY">Jujuy</option>
                                <option value="LA PAMPA">La Pampa</option>
                                <option value="LA RIOJA">La Rioja</option>
                                <option value="MENDOZA">Mendoza</option>
                                <option value="MISIONES">Misiones</option>
                                <option value="NEUQUEN">Neuquén</option>
                                <option value="RIO NEGRO">Río Negro</option>
                                <option value="SALTA">Salta</option>
                                <option value="SAN JUAN">San Juan</option>
                                <option value="SAN LUIS">San Luis</option>
                                <option value="SANTA CRUZ">Santa Cruz</option>
                                <option value="SANTA FE">Santa Fe</option>
                                <option value="SGO. DEL ESTERO">Santiago del Estero</option>
                                <option value="TIERRA DEL FUEGO">Tierra del Fuego</option>
                                <option value="TUCUMAN">Tucumán</option>
                            </select>
                        </div>

                        <input type="hidden" name="envio" value="">

                    </form>
                </div>
