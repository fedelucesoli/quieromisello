@extends('layouts.app')

@section('titulo', 'Proceso de compra - Quiero mi sello')

@section('content')

{{-- {{dd(\Cart::getContent())}} --}}
<div class="container mt-5">

    <h3 class="text-center">Procesar pedido </h3>

    <div class="row ">
        <div class="col-sm-12">
            @if (Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
        </div>
    </div>

    <div class="d-flex justify-content-center">
        <div class="mt-4 p-3 rounded bg-white shadow col-12 col-md-8">

            <div class="row" v-if="!finalizar">

                {{-- DATOS --}}
                <div class="col-12 mb-3">
                    <h4>Dejanos tus datos de contacto</h4>
                    <hr>
                    <form method="post" id="form" class="form">
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre" v-model="contacto.nombre"
                                    aria-describedby="Nombre" v-on:blur="validar"
                                    data-regex="/^(?=.{2,45}$)([a-zA-Zá-úÁ-Ú](\s[a-zA-Zá-úÁ-Ú])?(\s)?)*$/"
                                    data-message="Ingrese un nombre válido" data-required="1">
                                <small id="nombre-msg" class="form-text text-muted invalid-feedback"></small>
                            </div>
                            <div class="form-group col-6">
                                <label for="apellido">Apellido</label>
                                <input type="text" class="form-control" id="apellido" v-model="contacto.apellido"
                                    aria-describedby="Apellido" v-on:blur="validar"
                                    data-regex="/^(?=.{2,45}$)([a-zA-Zá-úÁ-Ú](\s[a-zA-Zá-úÁ-Ú])?(\s)?)*$/"
                                    data-message="Ingrese un apellido válido" data-required="1">
                                <small id="apellido-msg" class="form-text text-muted invalid-feedback"></small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" v-model="contacto.email"
                                aria-describedby="correo electronico" class="field__input border-radius"
                                v-on:blur="validar" data-regex="/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z]{2,4}$/"
                                data-message="Ingrese un email válido" data-required="1">
                            <small id="email-msg" class="form-text text-muted invalid-feedback"></small>
                        </div>
                    </form>
                </div>

                {{-- ENVIO --}}
                <div class="col-12 mb-5">
                    <h4>Envío</h4>
                    <hr>
                    <envios-component :seleccionable="true" @eventoenvio="selectEnvio"></envios-component>
                </div>


                {{-- DATOS --}}


                <div class="col-12 mb-5" v-if="showFormDomicilio">@include('front.carrito.includes.datos-envio') </div>

                {{-- BOTONERA --}}
                <div class="col-12 ">
                    <hr>
                    <div class="row align-items-center ">
                        <div class="col">
                            <a href="{{url('/')}}" class="text-dark link"><i class="bi bi-arrow-left mr-2"></i>Volver a
                                la tienda</a>
                        </div>
                        <div class="col text-right"> <button v-on:click.prevent="enviarPedido" :disabled="btnDisabled"
                                class="btn btn-primary ">FINALIZAR</button></div>
                    </div>
                </div>

            </div>

            <div class="d-flex flex-column justify-content-center align-items-center px-2" v-if="finalizar">
                <h3 class="mx-auto  pt-4 pb-2">Muchas gracias por tu pedido!</h3>


                <p class="text-center">Tu numero de pedido es:</p>
                <div class="d-flex ">
                    <div class="pt-2 pb-1 px-4 bg-light rounded">
                        <p class="h6 pt-1">@{{idpedido}} </p>
                    </div>
                    <a href="" class="btn btn-primary ml-2 px-4">Copiar</a>
                </div>

                <p class="mt-4">Texto de como seguir la compra. Mandanos el numero de pedido por FACEBOOK o INSTAGRAM
                    para continuar la compra.</p>
                {{-- TODO agregar botones de las redes --}}
            </div>


        </div>
    </div>

</div>

@endsection

@push('scripts')
<script src="{{asset('js/checkout.js')}}"></script>
@endpush
