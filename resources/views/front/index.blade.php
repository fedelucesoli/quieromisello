@extends('layouts.app')
@section('content')


@include('front.pages.arma-tu-set-explicacion')


<section class="container my-5">
    <div class="row">
        <div class="col-12">
            <h4>Nuestros productos</h4>

        </div>

        @foreach ($productos as $producto)
        <div class="col-12 col-sm-6 col-lg-3 my-3">
            <div class="card shadow  tarjeta1" style="">
                @include('front.includes.imagen', ['imagen' => $producto->portada, 'alt'=> $producto->nombre])


                <div class="card-body">
                    <a href="{{route('producto', $producto->slug)}}"
                        class="h5 card-title card-link stretched-link">{{$producto->nombre}}</a>
                </div>
            </div>
        </div>
        @endforeach

    </div>

</section>



@endsection

@push('zocalo')
@include('front.includes.banner-envio-pago')
@endpush
