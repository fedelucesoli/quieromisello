@extends('layouts.app') @section('content')

<div class="container ">

  <div class="row justify-content-center">
    <div class="col-12 col-lg-8 mx-auto text-center mt-5 ">
      <div class="card py-4">
        <div class="card-body ">
          <h1 class="text-danger">Estamos haciendo <br> la tienda!</h1>
          <div class="row">

            <div class="col-12 ">
<div id="mailchimp">
  <p class="lead">
    Dejanos tu email y te avisamos cuando este lista!
  </p>
    <!-- Begin MailChimp Signup Form -->
    <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
    <style type="text/css">
      #mc_embed_signup{background:transparent; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
      /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
         We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
    </style>
    <div id="mc_embed_signup">
    <form action="https://quieromisello.us19.list-manage.com/subscribe/post?u=386d6ac7091f19acd02658c06&amp;id=d694c3e0c3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">

      <input type="email" value="" name="EMAIL" class="email" style="height: 38px" id="mce-EMAIL" placeholder="Email" required>
        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_386d6ac7091f19acd02658c06_d694c3e0c3" tabindex="-1" value=""></div>
        <div class="clear"><input type="submit" value="Suscribirme" name="subscribe" id="mc-embedded-subscribe" class="butt1on btn btn-outline-danger"></div>
        </div>
    </form>
    </div>
    <!--End mc_embed_signup-->
</div>
</div>
<div class="col-12">
  <div class="mt-4">
      <p class="lead">Encontranos en:</p>

    <div class="social-btns">
      <a class="social-btn facebook"  href="https://www.facebook.com/quieromisello"><i class="bi bi-facebook"></i></i></a>
      <a class="social-btn instagram"  href="https://www.instagram.com/quieromisello"><i class="bi bi-instagram"></i></i></a>
    </div>

  </div>
</div>
            </div>
          </div>


      </div>


    </div>
  </div>

</div>


@endsection

{{-- @push('scripts')
<script src="{{ asset('js/envios.js') }}"></script>
@endpush --}}
