<ul class="list-group list-group-flush mt-5">
    <li class="list-group-item py-4">
        <i class="fas fa-credit-card mt-1  position-absolute text-muted"> </i>
        <p class="lead d-block pl-5 mb-1">
            Hasta 12 cuotas sin interés
        </p>
        <div class="pl-5">
            <p class="text-muted d-none d-lg-block">Podés abonar tu pedido a través de MercadoPago que
                acepta diversos medios pago como: efectivo, tarjeta de crédito y débito.</p>
            <a class="text-info " href="https://www.mercadopago.com.ar/cuotas?seller_id=&marketplace=NONE"
                target="_blank"> Ver medios de pago <i class="fa fa-chevron-right"></i></a>
            {{-- <a href="#" v-on:click.prevent="mostrarPagos()" class="text-success">Ver medios de pago <i class="fa fa-chevron-right"></i></a> --}}

        </div>

    </li>

    <li class="list-group-item py-4">
        <i class="fas fa-truck mt-1 position-absolute text-muted"></i>

        <p class="lead d-block ml-5 mb-1"> Envíos a todo el pais por OCA </p>
        <div class="pl-5">
            {{-- <p>Es gratis para compras superiores a $ 2.999 y para compras de menor valor, el costo es de $ 299. Recibirás tu pedido en tu domicilio o en la sucursal del correo en un plazo máximo de 15 días hábiles una vez acreditado tu pago.</p> --}}
            <a href="#" v-on:click.prevent="mostrarForm()" class="text-info ">Calcular Envío <i
                    class="fa fa-chevron-right"></i></a>
        </div>
        <transition enter-active-class="animate__animated animate__fadeIn"
            leave-active-class="animate__animated animate__fadeOut">
            <div class=" py-4" v-show="envio.showForm">

                <div class="input-group my-3">
                    <input type="text" v-model="envio.cp" class="form-control" placeholder="Código Postal"
                        aria-label="Código Postal" aria-describedby="codigopostal">
                    <div class="input-group-append">
                        <button class="btn btn-outline-danger" type="button"
                            v-on:click.prevent="calcularenvio()" v-html="envio.textButton"></button>
                    </div>
                </div>

            </div>
        </transition>
        <transition enter-active-class="animate__animated animate__fadeIn"
            leave-active-class="animate__animated animate__fadeOut">
            <div class=" py-4" v-show="envio.showHtml" v-html="envio.html"> </div>
        </transition>
    </li>

</ul>

@push('scripts')

    <script src="{{ asset('js/producto.js') }}"></script>

@endpush
