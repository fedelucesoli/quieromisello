<img class="lazyload card-img-top w-100"
    data-sizes="auto"
    src="{{asset('imagenes/thumbs/'. $imagen)}}"
    data-srcset="{{asset('imagenes/full/'. $imagen)}}"
    alt="{{$alt ?? 'Quiero mi sello'}}">

