<section class="bg-light py-5 mt-auto">
  <div class="container">
    {{-- Bloque - Formas de pago y envios --}}

    <div class="row">
      <div class="col-12 col-md-6 media my-4 ">
        <img src="{{asset('imagenes/logos-iconos/mp.png')}}" class="lazy mr-3 img-fluid" width="90px" alt="...">
        <div class="media-body">
          <h5 class="mt-0 mb-1 text-uppercase">Formas de pago</h5>
         Para pagarlos te pasamos un ticket de MercadoPago. Podes pagarlos con débito, crédito, transferencia, en RapiPago o PagoFacil.
        </div>
      </div>
      <div class="col-12 col-md-6 media my-4 ">
        <img src="{{asset('imagenes/logos-iconos/oca.png')}}" class="lazy mr-3" width="90px" alt="...">
        <div class="media-body">
          <h5 class="mt-0 mb-1 text-uppercase">Envios</h5>
          Hacemos envíos a todo el pais por OCA <br><a href="{{route('envios')}}">Calcular Envío</a>
        </div>
      </div>
    </div>
  </div>
</section>
