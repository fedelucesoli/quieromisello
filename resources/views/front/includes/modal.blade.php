<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="producto"
    aria-hidden="true" id="modal">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <button type="button" class="close ml-auto py-1 px-3 text-lg" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="h1 text-danger">&times;</span>

            </button>
            <div id="contenido">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="producto"
    aria-hidden="true" id="modal-producto">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <button type="button" class="close ml-auto py-1 px-3 text-lg" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="h1 text-danger">&times;</span>

            </button>
            <div id="contenido-producto">

            </div>
        </div>
    </div>
</div>
