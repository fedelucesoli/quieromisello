<div class="container">

    <div class="row">
        @if ($producto->portada)
        <div class="col-12 mb-4">
            <img src="{{asset('imagenes/thumbs/'.$producto->portada)}}" class="lazyload rounded shadow img-fluid"
                alt="{{$producto->nombre}}">
        </div>

        @endif
        @if ($producto->imagenes)

        <div class="col-md-12">
            <div class="carousel slide text-center" id="myCarousel" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{!! asset('uploads/full_size/'.$producto->imagenes['filename']) !!}"
                            alt="{{ $producto->nombre }}" class="img-fluid rounded" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-1 order-md-first mt-2 mt-md-0">
            <div class="row">
                <div class="col col-md-12 ">
                    <a href="#myCarousel" data-target="#myCarousel" data-slide-to="0" class="active">
                        <img src="{!! asset('uploads/thumb_size/'.$producto->imagenes['filename']) !!}"
                            alt="{{ $producto->nombre }}" class=" img-fluid rounded mb-3" />
                    </a>
                </div>
            </div>
        </div>

        @endif

        <div class="col-12">
            <p class="h6 mr-auto text-muted "><small>$</small>{{$producto->precio}}</p>
            <h4 class="text-danger">{{$producto->nombre}}</h4>
            <p class="text-muted">{{$producto->descripcion}}</p>
        </div>


    </div>

</div>
