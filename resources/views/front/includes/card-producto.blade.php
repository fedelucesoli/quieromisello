<div class="col-6 col-md-4 col-lg-3 my-3">
    <div class="card shadow" style="">
        
            @if ($producto->badge)
                <span class="card-notify-badge">{{$producto->badge}}</span>
            @endif
            <a href="{{route('producto', $producto->slug)}}">
                @include('front.includes.imagen', ['imagen' => $producto->portada, 'alt' => $producto->nombre ] )
            </a>
        
        <div class="card-body">
        <a href="{{route('producto', $producto->slug)}}"
            @if ($producto->categorias->first()['slug'] == 'quiero-mi-sello-emprendedor')
                data-toggle="modal" data-slug="{{$producto->slug}}" data-target="#modal-producto"
            @endif
            class="card-title card-link ">{{$producto->nombre}}</a>
            <p class="mb-0 ">$ {{$producto->precio}}</p>
        </div>

    </div>
</div>