<div id="slider-botones" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#slider-botones" data-slide-to="0" class="active"></li>
    <li data-target="#slider-botones" data-slide-to="1"></li>
    <li data-target="#slider-botones" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner rounded">

    <div class="carousel-item active">
        <img
        data-sizes="auto"
        src="{{asset('imagenesraw/quieromisello-correccion1-lq.jpg')}}"
        data-srcset="{{asset('imagenesraw/quieromisello-correccion1-lq.jpg')}} 220w,
        {{asset('imagenesraw/quieromisello-correccion1.jpg')}} 900w" class="lazyload blur-up d-block w-100" />

    </div>
     <div class="carousel-item">
              <img
        data-sizes="auto"
        src="{{asset('imagenesraw/quieromisello-correccion2-lq.jpg')}}"
        data-srcset="{{asset('imagenesraw/quieromisello-correccion2-lq.jpg')}} 220w,
        {{asset('imagenesraw/quieromisello-correccion2.jpg')}} 900w" class="lazyload blur-up d-block w-100" />
    </div>
    <div class="carousel-item">
              <img
        data-sizes="auto"
        src="{{asset('imagenesraw/quieromisello-correccion3-lq.jpg')}}"
        data-srcset="{{asset('imagenesraw/quieromisello-correccion3-lq.jpg')}} 220w,
        {{asset('imagenesraw/quieromisello-correccion3.jpg')}} 900w" class="lazyload blur-up d-block w-100" />
    </div>
  </div>
  <a class="carousel-control-prev" href="#slider-botones" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#slider-botones" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>
