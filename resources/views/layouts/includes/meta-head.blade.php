
<!-- Nombre de la aplicación web (solo debe usarse si la página web se usa como una aplicación) -->
<meta name="application-name" content="Quiero Mi Sello">

<!-- Color del tema para Chrome, Firefox OS y Opera -->
<meta name="theme-color" content="#f78197">

<!-- Breve descripción del documento (limitada a 150 caractéres) -->
<!-- Este contenido *podría* usarse como parte de los resultados del motor de búsqueda. -->
<meta name="description" content="Sellos de correción para docentes">

<!-- Controla el comportamiento del rastreo e indexación del motor de búsqueda -->
{{-- <meta name="robots" content="index,follow"><!-- Todos los motores de búsqueda --> --}}
{{-- <meta name="googlebot" content="index,follow"><!-- Específico de Google --> --}}


<!-- FACEBOOK -->
<meta property="fb:app_id" content="463293140868642">
<meta property="og:url" content="https://quieromisello.com">
<meta property="og:type" content="website">
<meta property="og:title" content="QuieroMiSello">
<meta property="og:image" content="{{asset('img/portadas/hero1.jpg')}}">
<meta property="og:description" content="Sellos de correción para docentes!">
<meta property="og:site_name" content="QuieroMiSello">
<meta property="og:locale" content="en_ES">
{{-- <meta property="article:author" content=""> --}}

<!-- TWITTER -->

{{-- <meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@site_account">
<meta name="twitter:creator" content="@individual_account">
<meta name="twitter:url" content="http://example.com/page.html">
<meta name="twitter:title" content="Content Title">
<meta name="twitter:description" content="Content description less than 200 characters">
<meta name="twitter:image" content="{{asset('img/portadas/hero1.jpg')}}"> --}}

<!-- NO LO GUARDES EN PINTEREST -->
<meta name="pinterest" content="nopin" description="No permitido!">


<!--[if IEMobile]><meta http-equiv="cleartype" content="on" /><![endif]-->
