<footer class="py-3 mt-auto">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <img src="{{asset('imagenes/logos-iconos/logo-color.svg')}}"
                    alt="Quiero mi Sello!" class="" height="20">
            </div>
            <div class="col-12 col-sm-6 text-md-right">
                <p class="text-muted d-inline mr-2">Encontranos en: </p>
                <a class="facebook mr-2" href="https://www.facebook.com/quieromisello">Facebook</a>
                {{-- <i class="fab fa-facebook-f"></i> --}}
                <a class="instagram" href="https://www.instagram.com/quieromisello">Instagram</a>
                {{-- <i class="fab fa-instagram"></i> --}}
            </div>
        </div>
    </div>
</footer>
