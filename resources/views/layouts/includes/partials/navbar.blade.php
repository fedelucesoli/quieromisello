<nav class="navbar navbar-expand-md nav-light bg-white fixed-top animated faster shadow-sm" id="navbar">
    <div class="container">

      <a class="navbar-brand text-center" href="{{route('arma-tu-set')}}"> <img src="{{asset('img/SVG/logo-color.svg')}}" alt="Quiero Mi Sello" height="30"> </a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
            <i class="fas fa-bars mt-1"></i>
          </span>
      </button>

      {{-- TODO  http://www.coloursonline.com.ar/ navbar min-height - 100vh --}}
      <div class="collapse navbar-collapse " id="menu">
        <ul class="navbar-nav ml-auto">

        <!-- Authentication Links -->
        
        @role('superadmin')

          {{-- <li class="nav-item"> <a class="nav-link text-muted" role="button" href="{{ route('admin.dashboard') }}">Administar</a> </li> --}}
          <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('arma-tu-set') }}">Arma tu set</a> </li>
          <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('preguntas-frecuentes') }}">Preguntas frecuentes</a> </li>

        @endrole

        @role('admin') 
        <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('arma-tu-set') }}">Arma tu set</a> </li>
        <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('envios') }}">Envios</a> </li>
        <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('preguntas-frecuentes') }}">Preguntas frecuentes</a> </li>
        @endrole

        @guest
           {{-- ENVIOS --}}
            <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('envios') }}">Envios</a> </li>
            <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('preguntas-frecuentes') }}">Preguntas frecuentes</a> </li>           

           {{-- <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('login') }}">Iniciar Sesion</a> </li> --}}
           {{-- <li class="nav-item text-muted"> <a class="nav-link text-muted" role="button" href="{{ route('register') }}">Registrarse</a> </li> --}}

        @else
            
           <li class="nav-item dropdown">
             {{-- TODO - perfil de USUARIO --}}
             <a class="nav-link dropdown-toggle text-muted" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  <i class="fas fa-user"></i>  </a>
             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01">
                @role('admin|superadmin')
               <a class="dropdown-item active" href="{{ route('arma-tu-set') }}"> Ver pagina </a>
                <a class="dropdown-item text-muted" href="{{ route('admin.dashboard') }}"> Dashboard </a>
                @endrole
               <a class="dropdown-item text-muted" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Cerrar Sesion </a>
               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> {{ csrf_field() }} </form>
             </div>
           </li>

        @endguest

        </ul>

      </div>
    </div>
  </nav>
