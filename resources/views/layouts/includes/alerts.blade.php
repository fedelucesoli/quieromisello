@if (session('error'))

<div class="container mt-3">
    <div class="alert alert-danger">
        {!! session('error') !!}
    </div>
</div>

@endif

@if (session('message'))

<div class="container mt-3">
    <div class="alert alert-success">
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    </div>
</div>

@endif
