<nav class="navbar navbar-expand-md nav-light bg-white fixed-top animated faster shadow-sm">
    <div class="container">

        {{-- <a class="navbar-brand mx-auto" href="{{ url('/') }}">
        <img src="{{ asset('imagenes/logos-iconos/logo-svg-color.svg') }}" alt="">
        </a> --}}
        <a class="navbar-brand text-center" href="{{ url('/') }}"> <img
                src="{{ asset('imagenes/logos-iconos/logo-color.svg') }}" alt="Quiero Mi Sello"
                height="30"> </a>



        @role('superadmin')
        <ul class="navbar-nav ml-auto d-md-none">
            <li class="nav-item">
                <a href="{{ route('carrito') }}" class="nav-link btn ml-auto text-muted" type="button">
                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-handbag" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 1a2 2 0 0 0-2 2v4.5a.5.5 0 0 1-1 0V3a3 3 0 0 1 6 0v4.5a.5.5 0 0 1-1 0V3a2 2 0 0 0-2-2z" />
                        <path fill-rule="evenodd" d="M3.405 6a.5.5 0 0 0-.498.45l-.912 6.9A1.5 1.5 0 0 0 3.488 15h9.024a1.5 1.5 0 0 0 1.493-1.65l-.913-6.9a.5.5 0 0 0-.497-.45h-9.19zm-1.493.35A1.5 1.5 0 0 1 3.405 5h9.19a1.5 1.5 0 0 1 1.493 1.35L15 13.252A2.5 2.5 0 0 1 12.512 16H3.488A2.5 2.5 0 0 1 1 13.251l.912-6.9z" />
                    </svg>
                    <small class="text-muted">{{ $cartCount }}</small>
                </a>
            </li>
        </ul>


        @endrole
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon">
                <svg viewBox="0 0 16 16" class="bi bi-list" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                        d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                </svg>
            </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->

            <!-- Right Side Of Navbar -->

            <ul class="navbar-nav ml-auto">
            @role('superadmin')
            <li class="nav-item">
                <a href="{{ route('carrito') }}" class="nav-link btn ml-auto text-muted" type="button">
                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-handbag" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 1a2 2 0 0 0-2 2v4.5a.5.5 0 0 1-1 0V3a3 3 0 0 1 6 0v4.5a.5.5 0 0 1-1 0V3a2 2 0 0 0-2-2z" />
                        <path fill-rule="evenodd"
                            d="M3.405 6a.5.5 0 0 0-.498.45l-.912 6.9A1.5 1.5 0 0 0 3.488 15h9.024a1.5 1.5 0 0 0 1.493-1.65l-.913-6.9a.5.5 0 0 0-.497-.45h-9.19zm-1.493.35A1.5 1.5 0 0 1 3.405 5h9.19a1.5 1.5 0 0 1 1.493 1.35L15 13.252A2.5 2.5 0 0 1 12.512 16H3.488A2.5 2.5 0 0 1 1 13.251l.912-6.9z" />
                    </svg>
                    <small class="text-muted">{{ $cartCount }}</small>
                </a>
            </li>
            @endrole
                <li class="nav-item">
                    <a class="nav-link text-muted" href="{{ route('envios') }}">Calcular envios</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-muted" href="{{ route('mayoristas') }}">Ventas por Mayor</a>
                </li>
                @role('superadmin')

                    <li class="nav-item dropdown">
                        <a class="nav-link text-muted dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Productos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ url('categoria/arma-tu-set') }}">Armá tu set</a>
                            <a class="dropdown-item" href="{{ url('categoria/sellos-personalizados') }}">Sellos personalizados</a>
                            <a class="dropdown-item" href="{{ url('categoria/quiero-mi-sello-emprendedor') }}">Sellos emprendedores</a>
                            <a class="dropdown-item" href="{{ url('categoria/almohadillas-y-tintas') }}">Almohadillas y tintas</a>
                        </div>
                    </li>

                    <!-- Authentication Links -->
                @endrole
                @guest
                    <li class="nav-item">
                        <a class="nav-link text-muted"
                            href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>

                    @if(Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-muted"
                                href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif

                @else

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{ route('admin.index') }}">
                                Panel de control
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>

                @endguest
            </ul>
        </div>



    </div>
</nav>
