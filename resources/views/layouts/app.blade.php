<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> --}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('titulo', config('app.name'))</title>

    <link rel="manifest" href="/manifest.webmanifest">

    <link rel="shortcut icon" type="favicon" href='/favicon.ico'/>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link defer href="{{ asset('css/app-quiero.css') }}" rel="stylesheet">

    @stack('styles')

    {{-- fontawesome --}}
    {{-- <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"> </script> --}}

    {{-- Animate --}}

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" /> --}}

    {{-- @include('layouts.includes.meta-head') --}}


</head>

<body>

    {{-- <div class="bs-canvas-overlay bs-canvas-anim bg-dark position-fixed w-100 h-100"></div> --}}

    @include('layouts.includes.navbar')

   <div class="mt-5 pt-4" id="app">

        @yield('subnav')

        @include('layouts.includes.alerts')

        <main  class="flex-grow">

            @yield('content')

        </main>

        @stack('modal')

    </div>

    {{-- @include('front.carrito.carrito') --}}

    @stack('zocalo')

    @include('layouts.includes.footer')

    {{-- <script src="{{asset('js/manifest.js')}}"></script> <script src="{{asset('js/vendor.js')}}"></script> --}}
    <script src="{{asset('js/app.js')}}"></script>

    @stack('scripts')

    @include('layouts.includes.analytics')
    @include('layouts.includes.pixelfacebook')
    @include('layouts.includes.messenger')

</body>
</html>
