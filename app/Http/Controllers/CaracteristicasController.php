<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CaracteristicaCreateRequest;
use App\Http\Requests\CaracteristicaUpdateRequest;
use App\Repositories\CaracteristicaRepository;

/**
 * Class CaracteristicasController.
 *
 * @package namespace App\Http\Controllers;
 */
class CaracteristicasController extends Controller
{
    /**
     * @var CaracteristicaRepository
     */
    protected $repository;


    /**
     * CaracteristicasController constructor.
     *
     * @param CaracteristicaRepository $repository
     *
     */
    public function __construct(CaracteristicaRepository $repository)
    {
        $this->repository = $repository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $caracteristicas = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $caracteristicas,
            ]);
        }

        return view('caracteristicas.index', compact('caracteristicas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CaracteristicaCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CaracteristicaCreateRequest $request)
    {
        try {

            // $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $caracteristica = $this->repository->createEsquema($request->all());

            $response = [
                'message' => 'Caracteristica created.',
                'data'    => $caracteristica->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $caracteristica = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $caracteristica,
            ]);
        }

        return view('caracteristicas.show', compact('caracteristica'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $caracteristica = $this->repository->find($id);

        return view('caracteristicas.edit', compact('caracteristica'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CaracteristicaUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CaracteristicaUpdateRequest $request, $id)
    {
        try {

            // $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $caracteristica = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Caracteristica updated.',
                'data'    => $caracteristica->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Caracteristica deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Caracteristica deleted.');
    }
}
