<?php

namespace App\Http\Controllers;

use Cart;
use Illuminate\Http\Request;
use App\Repositories\ProductoRepository;


class CartController extends Controller
{
    protected $productos;

    public function __construct( ProductoRepository $repository ) {
        $this->productos = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.carrito.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addItem(Request $request)
    {
        $producto = $this->productos->with(['categorias'])->find($request->input('productoId'));

        $options = $request->except('_token', 'productoId', 'precio', 'cantidad');

        Cart::add([
            'id' => uniqid(),
            'name' => $producto->nombre,
            'price' => intval($request->input('precio')),
            'quantity' => $request->input('cantidad'),
            'attributes' => $options,
            'associatedModel' => $producto
        ]);

        if ($request->expectsJson()) {
            return response()->json([
                'message' => 'Clientes deleted.',
                'url' => route('carrito')
            ]);
        }

        return redirect('carrito')->with('mensaje', $producto->nombre.' agregado al pedido');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cart::update($id, $request->quantity);
        return redirect('cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeItem($id)
    {
        Cart::remove($id);

        if (Cart::isEmpty()) {
            return redirect('/');
        }
        return redirect()->back()->with('message', 'Eliminamos el producto del pedido.');
    }

    public function clearCart()
    {
        Cart::clear();

        return redirect('/');
    }
}

