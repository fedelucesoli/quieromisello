<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ProductoCreateRequest;
use App\Http\Requests\ProductoUpdateRequest;
use App\Repositories\ProductoRepository;
use App\Validators\ProductoValidator;
use App\Repositories\CategoriasRepository;
use App\Helpers\ImageHelper;

/**
 * Class ProductosController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProductosController extends Controller
{
    /**
     * @var ProductoRepository
     */
    protected $repository;
    protected $categoriasRepository;

    /**
     * @var ProductoValidator
     */
    protected $validator;

    /**
     * ProductosController constructor.
     *
     * @param ProductoRepository $repository
     * @param ProductoValidator $validator
     */
    public function __construct(
        ProductoRepository $repository,
        ProductoValidator $validator,
        CategoriasRepository $categoriasRepository
        )
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->categoriasRepo  = $categoriasRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $categorias = $this->categoriasRepo->all();

        // dd($request);
        if($categoria = $request->query('categoria')){
            $categoria = $this->categoriasRepo->where('id', $categoria)->first();
            $productos = $categoria->productos;
            foreach ($categoria->subcategorias as $subcate) {
                $productos = $productos->concat($subcate->productos);
            }
            $productos = collect($productos);
            return view('admin.productos.list', compact('productos', 'categorias'));
        }
        $productos = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $productos,
            ]);
        }

        return view('admin.productos.list', compact('productos', 'categorias'));
    }

    public function create()
    {
        $categorias = $this->categoriasRepo->lista('id', 'asc');
        return view('admin.productos.create',[
            'categorias' => $categorias
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ProductoCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $input = $request->all();

            if ($request->hasFile('portada')) {
                $imageHelper = new ImageHelper();
                $photo = $imageHelper->upload($request->portada);

                if (!$photo['error']) {

                    $input['portada'] = $photo['imagen'];
                } else {
                    $input['portada'] = 'placeholder.jpg';
                }
            }

            $producto = $this->repository->create($input);

            if ($request->has('categoria')) {
                $producto->categorias()->sync([$request->input('categoria')]);
            } else {
                $this->repository->detachCategories();
            }



            $response = [
                'message' => 'Producto created.',
                'data'    => $producto->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            // return redirect()->route('admin.index', [$producto])->with('message', $response['message']);

            return redirect()->back()->with('message', $response['message']);


        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = $this->repository->with('esquemas')->find($id);


        if (request()->wantsJson()) {

            return response()->json([
                'data' => $producto,
            ]);
        }

        return view('admin.productos.show', compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = $this->repository->find($id);

        $categorias = $this->categoriasRepo->lista('id', 'asc');

        return view('admin.productos.edit', compact('producto', 'categorias'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ProductoUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            if ($request->hasFile('portada_file')) {
                $imageHelper = new ImageHelper();
                $photo = $imageHelper->upload($request->portada_file);

                if (!$photo['error']) {
                    $request->merge(['portada' => $photo['imagen']]);
                } else {
                    $request->merge(['portada' => '']);
                }
           }

            $producto = $this->repository->update($request->all(), $id);

            if ($request->has('categoria')) {

                $producto->categorias()->sync([$request->input('categoria')]);
            }


            $response = [
                'message' => 'Producto updated.',
                'data'    => $producto->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $producto = $this->repository->find($id);
        $producto->categorias()->sync([]);
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Producto deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Producto deleted.');
    }


}
