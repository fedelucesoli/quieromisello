<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index');
    }

    public function optimizar()
    {
        $message = \Artisan::call('optimize');
        $message = \Artisan::call('clear-compiled');
        $message = \Artisan::call('package:discover');

        // $message = \Artisan::call('config:clear');

        // $message1 = \Artisan::call('route:cache');
        // $message1 = \Artisan::call('view:cache');

        return back()->with('message', 'Optimizado!');
    }
    public function migrar()
    {
        $message = \Artisan::call('migrate');


        return back()->with('status', 'Optimizado!');
    }
}
