<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\EsquemasCreateRequest;
use App\Http\Requests\EsquemasUpdateRequest;
use App\Repositories\EsquemasRepository;
use App\Validators\EsquemasValidator;
use App\Helpers\ImageHelper;


/**
 * Class EsquemasController.
 *
 * @package namespace App\Http\Controllers;
 */
class EsquemasController extends Controller
{
    /**
     * @var EsquemasRepository
     */
    protected $repository;

    /**
     * @var EsquemasValidator
     */
    protected $validator;

    /**
     * EsquemasController constructor.
     *
     * @param EsquemasRepository $repository
     * @param EsquemasValidator $validator
     */
    public function __construct(EsquemasRepository $repository, EsquemasValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $esquemas = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $esquemas,
            ]);
        }

        return view('esquemas.index', compact('esquemas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EsquemasCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(EsquemasCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input = $request->all();
            if ($request->hasFile('portada')) {
                $imageHelper = new ImageHelper();
                $photo = $imageHelper->upload($request->portada);

                if (!$photo['error']) {

                    $input['portada'] = $photo['imagen'];
                } else {
                    $input['portada'] = 'placeholder.jpg';
                }
            }

            $esquema = $this->repository->create($input);
            // $esquema = $this->repository->create($request->all());

            $response = [
                'message' => 'Esquemas created.',
                'data'    => $esquema->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $esquema = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $esquema,
            ]);
        }

        return view('esquemas.show', compact('esquema'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $esquema = $this->repository->find($id);

        return view('esquemas.edit', compact('esquema'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EsquemasUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(EsquemasUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $esquema = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Esquemas updated.',
                'data'    => $esquema->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Esquemas deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Esquemas deleted.');
    }
}
