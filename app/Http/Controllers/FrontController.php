<?php

namespace App\Http\Controllers;

use App\Mail\NuevoPedido;
use App\Mail\RegistroMayorista;
use App\Repositories\CategoriasRepository;
use App\Repositories\ClientesRepository;
use App\Repositories\EsquemasRepository;
use App\Repositories\PedidosRepository;
use App\Repositories\ProductoRepository;
use App\Repositories\SelloRepository;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Class Front Controller.
 *
 * @package namespace App\Http\Controllers;
 */
class FrontController extends Controller
{

    protected $productos;
    protected $categoriasRepository;
    protected $sellos;
    protected $esquemas;

    public function __construct(
        ProductoRepository $productos,
        CategoriasRepository $categoriasRepository,
        SelloRepository $sellos,
        EsquemasRepository $esquemas
    ) {
        $this->productos = $productos;
        $this->categorias = $categoriasRepository;
        $this->sellos = $sellos;
        $this->esquemas = $esquemas;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function construccion()
    {
        return view('front.construccion');
    }

    public function index()
    {

        if (Auth::guest()) {
            return view('front.construccion');
        }

        $productos = $this->productos->portada();
        return view('front.index', compact('productos'));

    }

    /**
     * Mostrar Los productos por categorias
     */
    public function showProductoPorCategorias($categoria)
    {

        $categorias = $this->categorias->portada();

        try {

            $categoria = $this->categorias->where('slug', $categoria)->first();
            // dd($categoria->load('subcategorias.productos'));
            $productos = $categoria->productos;
            // dd($productos);
            foreach ($categoria->subcategorias as $subcate) {
                $productos = $productos->concat($subcate->productos);
            }
            $productos = collect($productos);

            // dd($productos);
            //  $productos = $this->productos->find($categoria->id);
            // dd($productos);

            // $categoria = $categoria->with('subcategoria')->get();
            // $categoria = $this->categorias->with(['subcategoria', 'productos' => function ($query) {
            //     $query->where('estado', true);
            // }])->findByField('slug', $categoria)->first();
            // $productos = $categoria->productos;

        } catch (\Throwable $th) {
            throw $th;
        }

        // Si quiere Json
        if (request()->wantsJson()) {return response()->json(['data' => $productos]);}

        // if($categoria['slug'] == 'arma-tu-set' ){
        //     return view('front.arma-tu-set', compact('productos', 'categoria', 'categorias'));
        // }

        return view('front.listado', compact('productos', 'categoria', 'categorias'));
    }

    /**
     * Mostrar producto
     *  - Si es personalizable -> mostrar wizard
     *  -> productos similares
     */
    public function show($slug)
    {

        $producto = $this->productos
            ->with(['esquemas', 'categorias', 'caracteristicas'])
            ->findByField('slug', $slug)
            ->first();

        $caracteristicas = $producto->caracteristicas->groupBy('tipo')->all();

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $producto,
                'html' => view('front.includes.productomodal', compact('producto', 'caracteristicas'))->render(),
            ]
            );
        }

        return view('front.producto', compact('producto', 'caracteristicas'));
    }

    /**
     *
     * Producto agregado al Carrito
     *    Save carrito con las caracteristicas
     *  - si tiene extras mostrar pagina con productos extras
     *  - sino mostrar alert popup
     *
     */

    public function addToCart(Request $request)
    {

        $producto = $this->productos->with(['categorias'])->find($request->input('productoId'));

        $options = $request->except('_token', 'productoId', 'precio', 'cantidad');

        Cart::add([
            'id' => uniqid(),
            'name' => $producto->nombre,
            'price' => $request->input('precio'),
            'quantity' => $request->input('cantidad'),
            'attributes' => $options,
            'associatedModel' => $producto,
        ]);

        // Session::flash('mensaje', $producto->nombre.' agregado al pedido');

        if ($request->expectsJson()) {
            return response()->json([
                'message' => 'Clientes deleted.',
                'url' => route('carrito'),
            ]);
        }

        return redirect()->route('carrito')->with('mensaje', $producto->nombre . ' agregado al pedido');
    }

    /**
     * Pasarela de pago [   Finalizar pedido   ]
     *  - Resumen carrito con los productos
     *  - Form de datos
     *  - Guardamos el pedido y generaramos un numero externo para mostrar
     *  - Mostramos la pantalla de numero de pedido y pasos a seguir
     *  -
     */

    public function checkout()
    {
        return view('front.carrito.checkout');
    }

    public function postCheckout(Request $request, PedidosRepository $pedidos, ClientesRepository $clienteRepository)
    {
        $carrito = Cart::getContent();

        $cliente = $clienteRepository->findByField('email', $request->email)->first();
        if (is_null($cliente)) {
            $datos_cliente = [
                'first_name' => $request->contacto['nombre'],
                'last_name' => $request->contacto['apellido'],
                'email' => $request->contacto['email'],
                // 'phone' => '', agregar numero de celular al form
                // 'street_name' => $request->apellido,
                // 'street_number' => $request->numero,
                // 'floor' => $request->piso,
                // 'apartment' => $request->depto,
                // 'city' => $request->ciudad,
                // 'state' => $request->provincia,
                // // 'comments' => '',
                // 'zip_code' => $request->codigo_postal,
                'estado' => 'activo',
                'notas' => '',
                'tipo' => 'web',
            ];
            $cliente = $clienteRepository->create($datos_cliente);
        }

        // Creo Pedido
        $datos_pedido = [
            'cliente_id' => $cliente->id,
            'estado' => 'cargado',
            'estado_produccion' => 'no iniciado',
            'total' => Cart::getTotal(),
            'tipo_pago' => 'no sabemos',
            'estado_pago' => 'no sabemos',
            'envio_id' => 1,
            'tipo_envio' => $request->envio['datosEnvio']['tipoEnvio'],
            'envio' => $request->envio,
            'estado_envio' => 'no sabemos',
            'carrito' => Cart::getContent(),
            'datos' => '',
        ];

        $pedido = $pedidos->create($datos_pedido);
        $pedido->cliente = $cliente;
        $pedido->urlpedido = route('admin.pedidos.show', $pedido->pedido_id);
        // - Enviar mail a quieromiselloya@gmail.com
        Cart::clear();

        Mail::to('fedelucesoli@gmail.com')->send(new NuevoPedido($pedido));
        // crear link de pago
        //
        // Mandar mail a quieromisello - con detalle del pedido -
        // cargar pedido en planilla de google
        //
        // crear comanda en pdf-?
        if ($request->expectsJson()) {
            return response()->json([
                'message' => 'Pedido Creado.',
                'id' => $pedido->pedido_id,
            ]);
        }
        return view('front.carrito.checkout');

    }

    public function esquemas()
    {
        try {
            // $producto = $this->productos->where('tipo_personalizable', 'arma-tu-set')->with('esquemas')->get();
            $producto = $this->esquemas->with('producto')->all();

        } catch (\Throwable $th) {
            throw $th;
        }

        // Si quiere Json
        if (request()->wantsJson()) {return response()->json(['data' => $producto]);}

        return view('front.productos.esquemas', compact('producto'));
    }

    public function cargar_wizard($producto)
    {
        /**
         * Este metodo determina si es personalizado el producto
         * Devuelve todos los datos personalizables y catalogos
         */

        if ($producto->tipo === 'set-correcion-personalizable') {
            $vista = 'front.productos.set-correcion-personalizable'; // $producto->tipo
            $catalogos = $this->sellos->getSellosCorrecion();
            $response = array(
                $vista,
                $catalogos,
            );

            if ($producto->tipo === 'sello-automatico-personalizable') {

            }
            return $response;

        }
    }

    public function mayoristas()
    {
        return view('front.pages.mayoristas');
    }

    public function post_mayoristas(Request $request)
    {

        // Validar datos
        $validator = $request->validate([
            'nombre' => 'required',
            'ciudad' => 'required',
            'email' => 'required|email',
            'telefono' => 'required|numeric',
        ]);

        $datos = new \stdClass();
        $datos->nombre = $request->get('nombre');
        $datos->ciudad = $request->get('ciudad');
        $datos->telefono = $request->get('telefono');
        $datos->email = $request->get('email');
        $datos->sender = 'WebQuiero';
        $datos->receiver = '';

        // - Enviar mail a quieromiselloya@gmail.com

        Mail::to('quieromiselloya@gmail.com')->send(new RegistroMayorista($datos));

        // - Devolver un mensaje a la pagina anterior

        return response()->json([
            'status' => true,
            // 'response' => $menvios,
            'html' => 'Gracias por contactarte',
            // 'html' => view('web.partials.envio', $data)->render()
        ]);
    }

    public function envios()
    {
        return view('front.pages.calculadora-de-envios');
    }

    public function sasa()
    {
        return view('front.sasa');
    }

    public function preguntasFrecuentes()
    {
        return view('front.pages.preguntas-frecuentes');
    }

}
