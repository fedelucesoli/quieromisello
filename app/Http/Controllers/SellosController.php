<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\SelloCreateRequest;
use App\Http\Requests\SelloUpdateRequest;
use App\Repositories\SelloRepository;
use App\Validators\SelloValidator;

/**
 * Class SellosController.
 *
 * @package namespace App\Http\Controllers;
 */
class SellosController extends Controller
{
    /**
     * @var SelloRepository
     */
    protected $repository;

    /**
     * @var SelloValidator
     */
    protected $validator;

    /**
     * SellosController constructor.
     *
     * @param SelloRepository $repository
     * @param SelloValidator $validator
     */
    public function __construct(SelloRepository $repository, SelloValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $sellos = $this->repository->all();

        $grouped = $sellos->groupBy(['catalogo_idioma', 'tamanio']);
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $grouped,
            ]);
        }

        return view('sellos.index', compact('sellos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SelloCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(SelloCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $sello = $this->repository->create($request->all());

            $response = [
                'message' => 'Sello created.',
                'data'    => $sello->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sello = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $sello,
            ]);
        }

        return view('sellos.show', compact('sello'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sello = $this->repository->find($id);

        return view('sellos.edit', compact('sello'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SelloUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(SelloUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $sello = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Sello updated.',
                'data'    => $sello->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Sello deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Sello deleted.');
    }
}
