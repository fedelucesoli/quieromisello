<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CategoriasCreateRequest;
use App\Http\Requests\CategoriasUpdateRequest;
use App\Repositories\CategoriasRepository;
// use App\Validators\CategoriasValidator;
use App\Helpers\ImageHelper;

/**
 * Class CategoriasController.
 *
 * @package namespace App\Http\Controllers;
 */
class CategoriasController extends Controller
{
    /**
     * @var CategoriasRepository
     */
    protected $repository;



    /**
     * CategoriasController constructor.
     *
     * @param CategoriasRepository $repository

     */
    public function __construct(CategoriasRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $categorias = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $categorias,
                ]);
            }

        return view('admin.categorias.index', compact('categorias'))->render();
    }

    public function create()
    {
        return view('admin.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoriasCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CategoriasCreateRequest $request)
    {
        try {


            // $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $categoria = $this->repository->create($request->all());

            $response = [
                'message' => 'Categorias created.',
                'data'    => $categoria->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $categoria,
            ]);
        }

        return view('admin.categorias.show', compact('categoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = $this->repository->find($id);
        // dd(view('admin.categorias.edit', compact('categoria')));

        return view('admin.categorias.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoriasUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CategoriasUpdateRequest $request, $id)
    {
        try {

            // $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            if ($request->has('portada_file')) {
                $imageHelper = new ImageHelper();
                $photo = $imageHelper->upload($request->portada_file);

                if (!$photo['error']) {
                    $request->merge(['portada' => $photo['imagen']]);
                } else {
                    $request->merge(['portada' => '']);
                }
            }

            $categoria = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Categorias updated.',
                'data'    => $categoria->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Categorias deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Categorias deleted.');
    }
}
