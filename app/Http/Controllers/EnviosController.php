<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\EnvioCreateRequest;
use App\Http\Requests\EnvioUpdateRequest;
use App\Repositories\EnvioRepository;
use App\Validators\EnvioValidator;
use App\Helpers\FedeOcaHelper;

/**
 * Class EnviosController.
 *
 * @package namespace App\Http\Controllers;
 */
class EnviosController extends Controller
{
    /**
     * @var EnvioRepository
     */
    protected $repository;

    /**
     * @var EnvioValidator
     */
    protected $validator;

    /**
     * EnviosController constructor.
     *
     * @param EnvioRepository $repository
     * @param EnvioValidator $validator
     */
    public function __construct(EnvioRepository $repository, EnvioValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $envios = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $envios,
            ]);
        }

        return view('envios.index', compact('envios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EnvioCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(EnvioCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $envio = $this->repository->create($request->all());

            $response = [
                'message' => 'Envio created.',
                'data'    => $envio->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $envio = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $envio,
            ]);
        }

        return view('envios.show', compact('envio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $envio = $this->repository->find($id);

        return view('envios.edit', compact('envio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EnvioUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(EnvioUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $envio = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Envio updated.',
                'data'    => $envio->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Envio deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Envio deleted.');
    }

    public function getCostoEnvio(Request $request){

        $validatedData = $request->validate([
            'cp' => 'required|numeric',
        ]);

        session(['codigo_postal' => $request->cp]);
        $respuesta = array();


        $envio_domicilio 	= new FedeOcaHelper($cuit = '27-19056392-3', $operativa = 326109);
        $respuesta['domicilio']  = $envio_domicilio->tarifarEnvioCorporativo(.5, 0.003, 7240, $request->cp, 1, 500);
        if(empty($respuesta['domicilio'])){
            return response()->json([
                        'error' => 'El codigo postal no existe'
                    ])->setStatusCode(400);
        }
        $sucursales  = self::getSucursal($request->cp);

        if($sucursales){

            $envio_sucursal = new FedeOcaHelper($cuit = '27-19056392-3', $operativa = 326112);
            $respuesta['sucursal'] = $envio_sucursal->tarifarEnvioCorporativo(.5, 0.003, 7240, $request->cp, 1, 500);
            $respuesta['sucursal']['sucursales'] = $sucursales;

        }

       return response()->json([
            'respuesta' => $respuesta
        ]);
    }

    function getSucursal($codigo_postal){

        $envio_sucursal = new FedeOcaHelper($cuit = '27-19056392-3', $operativa = 326112);
        $sucursales = $envio_sucursal->getCentrosImposicionPorCP($codigo_postal);

        $respuesta = array();
        foreach ($sucursales as $sucursal) {
            if($sucursal['TipoAgencia'] === 'Sucursal OCA'){
                $respuesta[] = $sucursal;
            }
        }

        if(empty($respuesta)){
            return false;
        }else{
            return $respuesta;
        }

    }
}
