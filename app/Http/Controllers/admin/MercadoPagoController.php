<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use MercadoPago as MercadoPago;

use function GuzzleHttp\json_encode;

// use MercadoPago as MercadoPago;

class MercadoPagoController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
        // TODO poner claves en ENV
        // GIANNA
        // MercadoPago\SDK::setClientId("786444997213356");
        // MercadoPago\SDK::setClientSecret("1SaQucaTyswrFxyAOk0vOTZfTDwvQ5Or");
        // MercadoPago\SDK::setAccessToken('PROD_ACCESS_TOKEN');

        // VFEDE
        // MercadoPago\SDK::setClientId("8532287569750036");
        // MercadoPago\SDK::setClientSecret("eKEfkXd1S34cGdtvji5TokRuQx9SK6VY");

        // $this->mp = new MercadoPago;
    }


    public function index(){
        return view('admin.mp.dashboard');
    }

    public function storePedido(){

        // recibo datos
        // validar
        // ordenar
        // guardar
    }

    public function getAlgo(Request $request){
        $busqueda = $request->a;
        $tipo = $request->b;

        if($tipo == 'pago'){
            try {
                $response = $this->getPago($request->a);

            } catch (Exception $e) {
                // <h2>{{ $exception->getMessage() }}</h2>
                // php artisan vendor:publish --tag=laravel-errors
                return false;
            }
        }elseif($tipo == 'merchant'){
            try {
                $response = $this->getMerchant($request->a);
            } catch (Exception $e) {
                // <h2>{{ $exception->getMessage() }}</h2>
                // php artisan vendor:publish --tag=laravel-errors
                return false;
            }
        }

        return json_encode($response);

    }

    public function getPago($paymentId){
        $payment = $this->mp->get( "/v1/payments/" . $paymentId );
        // $payment = $this->mp->get(
        //     "/v1/payments/search",
        //     array(
        //         "external_reference" => "123456789",
        //         "limit" => 50,
        //         "offset" => 200,
        //         "sort" => "id",
        //         "criteria" => "desc"
        //     )
        // );
        return $payment;
    }

    public function getMerchant($merchantId){

        $merchant = $this->mp->get("/merchant_orders/" . $merchantId);

        // $merchant = $this->mp->get("/merchant_orders/search", array(
        //         "external_reference" => $merchantId,
        //         "limit" => 1,
        //         // "offset" => 0,
        //         // "sort" => "id",
        //         "criteria" => "asc"
        //     ) );
        return $merchant;
    }


    public function crearTicket(Request $request)
    {
        //  VALIDATION

        $preference = new MercadoPago\Preference();

        $item = new MercadoPago\Item();
        $item->title = 'titulo';
        $item->quantity = 1;
        $item->unit_price = 1;
        $preference->items = array($item);
        $preference->external_reference = 'federico';


        $shipments = new MercadoPago\Shipments();
        $shipments->mode = "me2";
        // $shipments->dimensions = "30x30x30,500";
        // $shipments->local_pickup = true;

        $preference->shipments = $shipments;

        // $preference->back_urls = array(
        //     'success' => "https://www.quieromisello.com",
        //     'pending' => "https://www.quieromisello.com",
        // );

        $preference->save();

        return json_encode(array('link' => $preference->init_point));
    }

    public function mediosDePago()
    {
        // $payment_methods = $this->mp->get('/v1/payment_methods');
        var_dump($payment_methods);
        return $payment_methods['body'];
        // RESPONSE
        // 'id' => string 'maestro' (length=7)
        //   'name' => string 'Maestro' (length=7)
        //   'payment_type_id' => string 'debit_card' (length=10)
        //   'status' => string 'active' (length=6)
        //   'secure_thumbnail' => string 'https://www.mercadopago.com/org-img/MP3/API/logos/maestro.gif' (length=61)
        //   'thumbnail' => string 'http://img.mlstatic.com/org-img/MP3/API/logos/maestro.gif' (length=57)
        //   'deferred_capture' => string 'unsupported' (length=11)

    }

}
