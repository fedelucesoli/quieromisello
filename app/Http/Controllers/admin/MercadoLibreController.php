<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Vcoud\Mercadolibre\Meli;
use App\Helpers\OcaHelper;


class MercadoLibreController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
        // TODO poner claves en ENV

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.mp.dashboard');
    }

    public function calcularEnvios(Request $request)
    {


        $validatedData = $request->validate([
            'cp' => 'required|numeric',
        ]);

        session(['codigo_postal' => $request->cp]);


        /* STORE RESULTADOS EN COOKIE O SESSION PARA NO TENER QUE REPETIR QUERYS A OCA */


        // dd($request->session()->all());


        $oca 	= new OcaHelper($cuit = '27-19056392-3', $operativa = 326112);
        $oca1 	= new OcaHelper($cuit = '27-19056392-3', $operativa = 326109);

        // $PesoTotal, $VolumenTotal, $CodigoPostalOrigen, $CodigoPostalDestino, $CantidadPaquetes, $ValorDeclarado)

        $respuesta  = $oca->tarifarEnvioCorporativo(.5, 0.003, 7240, $request->cp, 1, 500);
        $respuesta1  = $oca1->tarifarEnvioCorporativo(.5, 0.003, 7240, $request->cp, 1, 500);



        $data['ocaenvios'] = array(
            'name' =>  " Sucursal ",
            'pago' => '(pago en destino)',
            'cost' => \round($respuesta['0']['Precio']*1.21 , 0),
            // 'preciosiniva' => $respuesta['0']['Precio'],
            'detalle' => $respuesta['0']['Ambito']
        );

        $data['ocaenvios1'] = array(
            'name' =>  "Domicilio",
            'pago' => '(pago en destino)',
            'cost' => \round($respuesta1['0']['Precio']*1.21 , 0),
            // 'preciosiniva' => $respuesta1['0']['Precio'],
            'detalle' => $respuesta1['0']['Ambito']
        );

        return response()->json([
            'status' => true,
            'cp' => $request->cp,
            'html' => view('front.pages.envio', ['data' => $data])->render(),
            // 'html' => view('web.partials.envio', $data)->render()
        ]);
    }
}
