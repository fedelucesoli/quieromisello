<?php

namespace App\Http\Controllers;

use App\Http\Requests\PedidosCreateRequest;
use App\Http\Requests\PedidosUpdateRequest;
use App\Repositories\PedidosRepository;
use App\Validators\PedidosValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class PedidosController.
 *
 * @package namespace App\Http\Controllers;
 */
class PedidosController extends Controller
{
    /**
     * @var PedidosRepository
     */
    protected $repository;

    /**
     * @var PedidosValidator
     */
    protected $validator;

    /**
     * PedidosController constructor.
     *
     * @param PedidosRepository $repository
     * @param PedidosValidator $validator
     */
    public function __construct(PedidosRepository $repository, PedidosValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $pedidos = $this->repository->with('cliente')->all(['id', 'estado', 'pedido_id', 'total', 'created_at', 'cliente_id', 'carrito']);
        // dd($pedidos);
        return view('admin.pedidos.index', compact('pedidos'));
    }
    public function getPedidos()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $pedidos = $this->repository->with('cliente')->all(['id', 'pedido_id', 'total', 'created_at', 'cliente_id']);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $pedidos,
            ]);
        }

        return view('admin.pedidos.index', compact('pedidos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PedidosCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(PedidosCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $pedido = $this->repository->create($request->all());

            $response = [
                'message' => 'Pedidos created.',
                'data' => $pedido->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = $this->repository->with(['cliente'])->findByField('pedido_id', $id)->first();
        // dd($pedido);
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $pedido,
            ]);
        }

        return view('admin.pedidos.show', compact('pedido'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido = $this->repository->find($id);

        return view('admin.pedidos.edit', compact('pedido'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PedidosUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(PedidosUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $pedido = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Pedidos updated.',
                'data' => $pedido->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Pedidos deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Pedidos deleted.');
    }
}
