<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;

class ImageHelper
{
    public function upload($photo)
    {

        // NOMBRE DE LA IMAGEN
        $originalName = $photo->getClientOriginalName();
        $extension = $photo->getClientOriginalExtension();

        $originalNameWithoutExt = substr($originalName, 0, -4);
        $filename = $this->sanitize($originalNameWithoutExt);
        // $allowed_filename = $this->createUniqueFilename($filename);
        $filenameExt = $this->createUniqueFilename($filename, $extension);
        // $filenameExt = $allowed_filename . '.jpg';

        // SUBIR DISTINTOS TAMAÑÓS
        $uploadSuccess1 = $this->original($photo, $filenameExt);
        // $uploadSuccess2 = $this->icon($photo, $filenameExt);
        $uploadSuccess3 = $this->catalog($photo, $filenameExt);
        $uploadSuccess4 = $this->thumb($photo, $filenameExt);

        if (!$uploadSuccess1 || !$uploadSuccess4 || !$uploadSuccess3) {

            // Si QUIERE JSON
            // return Response::json([ 'error' => true, 'message' => 'Server error while uploading', 'code' => 500 ], 500);
            return array('error' => true, 'message' => 'Server error while uploading', 'code' => 500);
        }

        /**
         * Si todo salio bien paso un array con el nombre y ubicacion
         */

        // SI QUIERE JSON
        // return Response::json(['error' => false, 'code'  => 200, 'imagen' => $filenameExt], 200);

        return array('error' => false, 'code'  => 200, 'imagen' => $filenameExt);
    }


    public function createUniqueFilename($filename, $extension)
    {
        $full_size_dir = Config::get('images.full_size');
        $full_image_path = $full_size_dir . $filename . '.' . $extension;

        if (File::exists($full_image_path)) {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            return $filename . '-' . $imageToken . '.' . $extension;
        }

        return $filename . '.' . $extension;
    }
    /**
     * Optimize Original Img
     */
    public function original($photo, $filename)
    {
        $manager = new ImageManager();
        $image = $manager->make($photo)->encode('jpg')->save(Config::get('images.full_size') . $filename);
        return $image;
    }
    /**
     * Create Icon From Original
     */
    public function icon($photo, $filename)
    {
        $manager = new ImageManager();
        $image = $manager->make($photo)->encode('jpg')->resize(64, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(Config::get('images.icon_size')  . $filename);
        return $image;
    }
    public function catalog($photo, $filename)
    {
        $manager = new ImageManager();
        $image = $manager->make($photo)->encode('jpg')->resize(650, null,function ($constraint) {
            $constraint->aspectRatio();
        })->save(Config::get('images.catalog_size') . $filename);
        return $image;
    }


    public function thumb($photo, $filename)
    {
        $manager = new ImageManager();
        $image = $manager->make($photo)->encode('jpg')->resize(100, null, function ($constraint) {
            $constraint->aspectRatio();
        })->blur(15)->save(Config::get('images.thumb_size')  . $filename);
        

        return $image;
    }

    /**
     * Delete Image From Session folder, based on original filename
     */
    public function delete($originalFilename)
    {
        $full_size_dir = Config::get('images.full_size');
        $icon_size_dir = Config::get('images.icon_size');
        $thumb_size_dir = Config::get('images.thumb_size');
        $sessionImg = Img::where('original_name', 'like', $originalFilename)->first();
        if (empty($sessionImg)) {
            return Response::json([
                'error' => true,
                'code'  => 400
            ], 400);
        }
        $full_path1 = $full_size_dir . $sessionImg->filename . '.jpg';
        $full_path2 = $icon_size_dir . $sessionImg->filename . '.jpg';
        $full_path8 = $thumb_size_dir . $sessionImg->filename . '.jpg';
        if (File::exists($full_path1)) {
            File::delete($full_path1);
        }
        if (File::exists($full_path2)) {
            File::delete($full_path2);
        }
        if (File::exists($full_path8)) {
            File::delete($full_path8);
        }
        if (!empty($sessionImg)) {
            $sessionImg->delete();
        }
        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }
    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array(
            "~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?"
        );
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
        return ($force_lowercase) ? (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') : strtolower($clean) : $clean;
    }
}
