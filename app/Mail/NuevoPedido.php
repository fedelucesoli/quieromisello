<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NuevoPedido extends Mailable
{
    use Queueable, SerializesModels;
    public $pedido;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pedido)
    {

        $this->pedido = $pedido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('info@quieromisello.com')
            ->view('emails.pedidonuevo');

    }
}
