<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductoRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProductoRepository extends RepositoryInterface
{
    //
    public function syncCategorias(array $params);

    public function detachCategorias();
}
