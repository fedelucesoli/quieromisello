<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EsquemasRepository.
 *
 * @package namespace App\Repositories;
 */
interface EsquemasRepository extends RepositoryInterface
{
    //
}
