<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EnvioRepository;
use App\Entities\Envio;
use App\Validators\EnvioValidator;

/**
 * Class EnvioRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EnvioRepositoryEloquent extends BaseRepository implements EnvioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Envio::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EnvioValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
