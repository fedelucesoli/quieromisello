<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProductoRepository;
use App\Entities\Producto;
use App\Validators\ProductoValidator;

/**
 * Class ProductoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductoRepositoryEloquent extends BaseRepository implements ProductoRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Producto::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProductoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function syncCategorias(array $params)
    {
        $this->model->categorias()->sync($params);
    }


    public function detachCategorias()
    {
        $this->model->categorias()->detach();
    }



    public function activo()
    {
        return $this->model->where('estado', true)->orderBy('peso', 'desc');
    }

    public function grupcaract(){
        return $this->with('caracteristicas', function($query){
                return $query->groupBy('tipo');
        });
    }

    public function portada(){

        return $this->orderBy('peso', 'desc')->findWhere([ 'estado' => true, 'destacado' => true ]);

    }


}
