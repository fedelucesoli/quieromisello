<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SelloRepository.
 *
 * @package namespace App\Repositories;
 */
interface SelloRepository extends RepositoryInterface
{
    //
}
