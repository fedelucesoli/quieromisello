<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EsquemasRepository;
use App\Entities\Esquemas;
use App\Validators\EsquemasValidator;

/**
 * Class EsquemasRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EsquemasRepositoryEloquent extends BaseRepository implements EsquemasRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Esquemas::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EsquemasValidator::class;
    }

     protected $fieldSearchable = [
        'id_producto'
    ];

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

    }

}
