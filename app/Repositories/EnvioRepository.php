<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EnvioRepository.
 *
 * @package namespace App\Repositories;
 */
interface EnvioRepository extends RepositoryInterface
{
    //
}
