<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SelloRepository;
use App\Entities\Sello;
use App\Validators\SelloValidator;

/**
 * Class SelloRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SelloRepositoryEloquent extends BaseRepository implements SelloRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Sello::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return SelloValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getSellosCorrecion(){

        $sellos = $this->model->where('catalogo_nombre', 'sellos-correcion')->get();
        return $sellos->groupBy('catalogo_idioma');

    }

    public function lista(string $order = 'id', string $sort = 'desc', $except = [])
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

}
