<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\ProductoRepository::class, \App\Repositories\ProductoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CategoriasRepository::class, \App\Repositories\CategoriasRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CaracteristicaRepository::class, \App\Repositories\CaracteristicaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EsquemasRepository::class, \App\Repositories\EsquemasRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SelloRepository::class, \App\Repositories\SelloRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ClientesRepository::class, \App\Repositories\ClientesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PedidosRepository::class, \App\Repositories\PedidosRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EnvioRepository::class, \App\Repositories\EnvioRepositoryEloquent::class);
        //:end-bindings:
    }
}
