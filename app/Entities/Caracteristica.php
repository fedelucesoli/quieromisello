<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Caracteristica.
 *
 * @package namespace App\Entities;
 */
class Caracteristica extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'id_producto',
        'tipo',
        'nombre',
        'descripcion',
        'portada',
        'imagenes',
        'datos',
        'configuracion',
    ];

    protected $casts = [
        'datos' => 'array',
        'configuracion' => 'array',
    ];

    public function producto(){

        return $this->belongsTo('App\Entities\Producto');

    }

}


// Sets personalizado

// id
// id_producto
// id_esquema
// id_cliente
// datos - detalles
//     array - numero de sellos
//     array - tipo de letra - dibujo - texto
//     array - imagenlogo - tamaño

// configuracion
// extras -> array de ids_productos ()
// tipo - set-personalizado-selloLogo-especial
// imagenes

// created_at
// updated_at
