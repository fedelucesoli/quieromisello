<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Clientes.
 *
 * @package namespace App\Entities;
 */
class Clientes extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'street_name',
        'street_number',
        'floor',
        'apartment',
        'city',
        'state',
        'comments',
        'zip_code',
        'estado',
        'notas',
        'tipo'
    ];
    /**
     * Get all of the pedidos for the Clientes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
        return $this->hasMany('App\Entities\Pedidos');
    }

}
