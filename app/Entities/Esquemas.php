<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Esquemas.
 *
 * @package namespace App\Entities;
 */
class Esquemas extends Model implements Transformable
{
    use TransformableTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'id_producto',
        'cantidad_xl',
        'cantidad_l',
        'cantidad_m',
        'cantidad_s',
        'precio',
        'descripcion',
        'portada',
        'imagenes',
        'datos',
        'configuracion'
    ];
    protected $fieldSearchable = [
        'id_producto'
    ];

    protected $casts =
    [
        'id_producto' => 'integer',
        'datos' => 'array',
        'configuracion' => 'array',
        'cantidad_l' => 'integer',
        'cantidad_m' => 'integer',
        'cantidad_s' => 'integer',
        'precio' => 'integer'
    ];

    /**
     * Get the producto that owns the Esquemas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto(): BelongsTo
    {
        return $this->belongsTo(Producto::class, 'foreign_key', 'other_key');
    }

}
