<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Entities\Categorias;

/**
 * Class Producto.
 *
 * @package namespace App\Entities;
 */
class Producto extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'stock_publico',
        'stock_privado',
        'portada',
        'precio',
        'estado',
        'tipo',
        'slug',
        'configuracion',
        'datos',
        'imagenes',
        'personalizable',
        'tipo_personalizable',
        'sku',
        'peso',
        'badge',
        'precio_promocion',
        'destacado',

    ];
    protected $casts = [
        'datos' => 'array',
        'destacado' => 'boolean'
    ];

    public function categorias()
    {
        return $this->belongsToMany('App\Entities\Categorias')->with('parent');
    }

    public function esquemas(){

        return $this->hasMany('App\Entities\Esquemas', 'id_producto', 'id');
    }

    public function caracteristicas(){

        return $this->hasMany('App\Entities\Caracteristica', 'id_producto', 'id');

    }

    public function activo()
    {
        return $this->model->where('estado', true);
    }

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = $value;

        if (!$this->exists) {
            $this->attributes['slug'] = str_slug($value);
        }
    }





}
