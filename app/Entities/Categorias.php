<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
// use Kalnoy\Nestedset\NodeTrait;
use App\Entities\Producto;


/**
 * Class Categorias.
 *
 * @package namespace App\Entities;
 */
class Categorias extends Model implements Transformable
{
    // use NodeTrait;

    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'slug',
        'descripcion',
        'portada',
        'parent_id',
        'peso',
        'estado',
        'personalizado'
    ];

   
    
    public function productos(){

        return $this->belongsToMany('App\Entities\Producto')->where('estado', true)->orderBy('peso', 'desc');

    }

    public function subcategorias()
    {
        return $this->hasMany('App\Entities\Categorias', 'parent_id')->orderBy('peso', 'desc');

    }

    public function parent()
    {
        return $this->belongsTo('App\Entities\Categorias', 'parent_id');
    }

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = $value;
        if (!$this->exists  ||  !$this->attributes['slug']){
            $this->attributes['slug'] = str_slug($value);
        }

    }

}
