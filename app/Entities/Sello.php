<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Sello.
 *
 * @package namespace App\Entities;
 */
class Sello extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo',
        'catalogo_nombre',
        'catalogo_idioma',
        'catalogo_year',
        'nombre',
        'tamanio',
        'portada',
        'personalizable',
        'datos',
        'imagenes',
        'stock',
        'negativo'
    ];
    // protected $casts =
    // [
    //     'datos' => 'array',
    // ];

}
