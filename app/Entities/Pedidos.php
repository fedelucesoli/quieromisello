<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Pedidos.
 *
 * @package namespace App\Entities;
 */
class Pedidos extends Model implements Transformable
{
    use TransformableTrait;

    public static function boot()
    {

        parent::boot();

        static::created(function ($model) {

            $model->pedido_id = '21' . Carbon::now()->format('m') . Str::padLeft($model->id, 3, '0');
            $model->save();
        });

    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pedido_id',
        'cliente_id',
        'estado',
        'estado_produccion',
        'total',
        'tipo_pago',
        'estado_pago',
        'envio_id',
        'tipo_envio',
        'estado_envio',
        'carrito',
        'configuracion',
        'datos',
    ];
    protected $appends = ['fecha_amigable', 'dia'];
    protected $cast = ['carrito' => 'object', 'configuracion' => 'object'];
    // protected $primaryKey = ['pedido_id', 'id'];

    /**
     * Get the cliente that owns the Pedidos
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Entities\Clientes');
    }
    public function getFechaAmigableAttribute()
    {
        $fecha = Carbon::parse($this->attributes['created_at']);
        return $fecha->translatedFormat('l d \de M');
    }

    public function getDiaAttribute()
    {
        $fecha = Carbon::parse($this->attributes['created_at']);
        return Str::title($fecha->translatedFormat('l'));
    }

    public function getCreatedAtAttribute()
    {
        $fecha = Carbon::parse($this->attributes['created_at']);
        return Str::title($fecha->translatedFormat('d \/ m'));
    }

    public function setCarritoAttribute($value)
    {
        $this->attributes['carrito'] = serialize($value);
    }

    public function getCarritoAttribute($value)
    {
        return unserialize($value);
    }
    public function setConfiguracionAttribute($value)
    {
        $this->attributes['configuracion'] = serialize($value);
    }

    public function getConfiguracionAttribute($value)
    {
        return unserialize($value);
    }

}
