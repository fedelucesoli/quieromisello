<?php

// use Illuminate\Http\Request;


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('api')->get('/producto/{slug}', 'FrontController@show');
Route::middleware('api')->get('/envio', 'admin\MercadoLibreController@calcularEnvios');
Route::middleware('api')->get('/enviofede', 'EnviosController@getCostoEnvio');
Route::middleware('api')->get('/esquemas', 'EsquemasController@index');
Route::middleware('api')->get('/sellos', 'SellosController@index');
// Route::middleware('api')->get('/catalogos', 'FrontController@catalogo');

// Route::middleware('api')->get('/crearticket', 'admin\MercadoPago@crearTicket');
