<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/construccion', 'FrontController@construccion')->name('construccion');

Route::get('/', 'FrontController@index')->name('index');

Route::get('/mayoristas', 'FrontController@mayoristas')->name('mayoristas');
Route::post('/mayoristas', 'FrontController@post_mayoristas')->name('post.mayoristas');

// Route::get('/sasa', 'FrontController@sasa')->name('sasa');

Route::get('/categoria/{slug}', 'FrontController@showProductoPorCategorias')->name('categoria');

Route::get('/productos/{slug}', 'FrontController@show')->name('producto');

Route::get('/carrito', 'CartController@index')->name('carrito');
Route::post('/carrito/agregar', 'CartController@addItem')->name('addToCart');
Route::get('/carrito/item/{id}/remove', 'CartController@removeItem')->name('carrito.remove');
Route::get('/carrito/clear', 'CartController@clearCart')->name('checkout.cart.clear');

Route::get('/checkout', 'FrontController@checkout')->name('checkout');
Route::post('/postCheckout', 'FrontController@postCheckout')->name('finalizar-compra');


Route::get('/envios', 'FrontController@envios')->name('envios');
Route::get('/preguntas-frecuentes', 'FrontController@preguntasFrecuentes')->name('preguntas-frecuentes');


// Admin
Auth::routes(['register' => false]);

Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {

    Route::get('/', 'HomeController@index')->name('index');
    Route::get('optimizar', 'HomeController@optimizar')->name('optimizar');
    Route::get('fede', 'CategoriasController@index')->name('fede');

    Route::resource('categorias', 'CategoriasController');
    Route::resource('productos', 'ProductosController');
    Route::resource('caracteristicas', 'CaracteristicasController');
    Route::resource('esquemas', 'EsquemasController');

    Route::resource('pedidos', 'PedidosController');
    Route::get('getPedidos', 'PedidosController@getPedidos')->name('getPedidos');
    Route::resource('clientes', 'ClientesController');

    Route::get('mercadopago', 'admin\MercadoPagoController@index')->name('mercadopago');
    Route::get('crearticket', 'admin\MercadoPagoController@crearTicket')->name('mercadopago.crearticket');

});
