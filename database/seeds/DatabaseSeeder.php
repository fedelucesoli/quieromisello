<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SellosEmprendimientosSeeder::class);
       $this->call(SellosSeeder::class);
       $this->call(SellosInglesSeeder::class);
       $this->call(LaratrustSeeder::class);
    }
}
