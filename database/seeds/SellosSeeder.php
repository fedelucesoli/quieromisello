<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SellosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Sellos L
        for ($i=101; $i < 123; $i++) {
            DB::table('sellos')->insert([
                'tipo' => 'sello',
                'catalogo_nombre' => 'sellos-correcion',
                'catalogo_idioma' => 'Español',
                'catalogo_year' => '2020',
                'nombre' => $i,
                'tamanio' => 'L',
                'personalizable' => false,
                'portada' => $i.'.jpg'
            ]);
        }
        // Sellos M
        for ($i = 201; $i < 291; $i++) {
            DB::table('sellos')->insert([
                'tipo' => 'sello',
                'catalogo_nombre' => 'sellos-correcion',
                'catalogo_idioma' => 'Español',
                'catalogo_year' => '2020',
                'nombre' => $i,
                'tamanio' => 'M',
                'personalizable' => false,
                'portada' => $i . '.jpg'
            ]);
        }
        // Sellos S
        for ($i = 19; $i < 66; $i++)  {
            DB::table('sellos')->insert([
                'tipo' => 'sello',
                'catalogo_nombre' => 'sellos-correcion',
                'catalogo_idioma' => 'Español',
                'catalogo_year' => '2020',
                'nombre' => $i,
                'tamanio' => 'S',
                'personalizable' => false,
                'portada' => $i . '.jpg'
            ]);
        }
    }
}
