<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SellosInglesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Sellos L
        for ($i=401; $i < 409; $i++) {
            DB::table('sellos')->insert([
                'tipo' => 'sello',
                'catalogo_nombre' => 'sellos-correcion',
                'catalogo_idioma' => 'Ingles',
                'catalogo_year' => '2020',
                'nombre' => $i,
                'tamanio' => 'L',
                'personalizable' => false,
                'portada' => $i.'.jpg'
            ]);
        }
        // Sellos M
        for ($i = 411; $i < 439; $i++) {
            DB::table('sellos')->insert([
                'tipo' => 'sello',
                'catalogo_nombre' => 'sellos-correcion',
                'catalogo_idioma' => 'Ingles',
                'catalogo_year' => '2020',
                'nombre' => $i,
                'tamanio' => 'M',
                'personalizable' => false,
                'portada' => $i . '.jpg'
            ]);
        }
        // Sellos S
        for ($i = 441; $i < 460; $i++)  {
            DB::table('sellos')->insert([
                'tipo' => 'sello',
                'catalogo_nombre' => 'sellos-correcion',
                'catalogo_idioma' => 'Ingles',
                'catalogo_year' => '2020',
                'nombre' => $i,
                'tamanio' => 'S',
                'personalizable' => false,
                'portada' => $i . '.jpg'
            ]);
        }
    }
}
