<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SellosEmprendimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Sellos L
        for ($i=01; $i < 29; $i++) {
            $producto =  DB::table('productos')->insertGetId([
                'nombre' => "L".$i,
                'descripcion' => "Sello de goma de 4x4cm",
                'stock_publico' => 10,
                'stock_privado' => 10,
                'portada' => 'sello-emprendimiento-l-'.$i.'.jpg',
                'precio' => 180,
                'estado' => 1,
                'slug' => 'sello-emprendimiento-L'.$i                
            ]);
            
        }
        // Sellos M
        for ($i = 01; $i < 29; $i++) {
            $producto = DB::table('productos')->insertGetId([
                'nombre' => "M".$i,
                'descripcion' => "Sello de goma de 2x4cm",
                'stock_publico' => 10,
                'stock_privado' => 10,
                'portada' => 'sello-emprendiemiento-m-'.$i.'.jpg',
                'precio' => 125,
                'estado' => 1,
                'slug' => 'sello-emprendimiento-M'.$i 
            ]);

        }
        // Sellos S
        for ($i = 01; $i < 21; $i++)  {
            $producto =  DB::table('productos')->insertGetId([
                'nombre' => "S".$i,
                'descripcion' => "Sello de goma de 2x2cm",
                'stock_publico' => 10,
                'stock_privado' => 10,
                'portada' => 'sello-emprendimiento-s-'.$i.'.jpg',
                'precio' => 80,
                'estado' => 1,
                // 'datos' => ['tamaño'=>'S'],
                'slug' => 'sello-emprendimiento-S'.$i 
            ]);

        }
    }
}
