<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;


/**
 * Class CreateCategoriasTable.
 */
class CreateCategoriasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categorias', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->string('slug');
            $table->integer('peso');
            $table->string('de')->nullable();
            $table->longText('descripcion')->nullable();
            $table->longText('portada')->nullable();

            $table->boolean('estado')->default(0);
            $table->nestedSet();
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categorias');
	}
}
