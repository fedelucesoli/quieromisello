<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateClientesTable.
 */
class CreateClientesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table) {
            $table->bigIncrements('id')->autoIncrement();

            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();

            // $table->string('image_path')->nullable();
            $table->string('street_name',90)->nullable();
            $table->integer('street_number')->nullable();

            $table->string('floor')->nullable();
            $table->string('apartment')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('comments')->nullable();
            $table->integer('zip_code')->nullable();

            $table->string('estado')->nullable();
            $table->text('notas')->nullable(); // PROBLEMAS
            $table->string('tipo')->nullable(); // MERCADOPAGO O LOBOS
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clientes');
	}
}
