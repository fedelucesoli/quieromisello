<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSellosTable.
 */
class CreateSellosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sellos', function(Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');

            $table->string('catalogo_nombre')->nullable();
            $table->string('catalogo_idioma')->nullable();
            $table->string('catalogo_year')->nullable();

            $table->string('nombre');
            $table->string('tamanio');
            $table->string('portada')->nullable();


            $table->boolean('personalizable')->default(false);

            $table->longText('datos')->nullable();
            $table->longText('imagenes')->nullable();

            $table->integer('stock')->default(0);
            $table->string('negativo')->nullable();



            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sellos');
	}
}
