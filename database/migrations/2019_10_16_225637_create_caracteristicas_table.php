<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCaracteristicasTable.
 */
class CreateCaracteristicasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('caracteristicas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('id_producto')->nullable();
            $table->string('tipo');
            $table->string('selector');
            $table->string('nombre')->nullable();
            $table->longText('descripcion')->nullable();
            $table->longText('portada')->nullable();
            $table->longText('imagenes')->nullable();
            $table->longText('datos')->nullable();
            $table->longText('configuracion')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('caracteristicas');
	}
}
