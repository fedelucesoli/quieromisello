<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEsquemasTable.
 */
class CreateEsquemasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('esquemas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('id_producto')->nullable();

            $table->string('nombre')->nullable();

            $table->string('cantidad_xl')->nullable();
            $table->string('cantidad_l')->nullable();
            $table->string('cantidad_m')->nullable();
            $table->string('cantidad_s')->nullable();

            $table->decimal('precio', 5, 2);

            $table->longText('descripcion')->nullable();
            $table->longText('portada')->nullable();
            $table->longText('imagenes')->nullable();

            $table->longText('datos')->nullable();
            $table->longText('configuracion')->nullable();


            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('esquemas');
	}
}
