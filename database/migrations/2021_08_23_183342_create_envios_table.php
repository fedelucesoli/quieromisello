<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEnviosTable.
 */
class CreateEnviosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('envios', function(Blueprint $table) {
            $table->increments('id');

            $table->string('idoperativa')->nullable();
            $table->string('nroremito')->nullable();

            $table->string('calle',90)->nullable();
            $table->integer('nro')->nullable();

            $table->string('piso')->nullable();
            $table->string('depto')->nullable();
            $table->string('localidad')->nullable();
            $table->string('provincia')->nullable();
            $table->string('idci')->nullable();
            $table->string('comments')->nullable();
            $table->integer('zip_code')->nullable();

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('envios');
	}
}
