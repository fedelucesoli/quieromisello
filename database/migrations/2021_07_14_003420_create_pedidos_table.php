<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePedidosTable.
 */
class CreatePedidosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidos', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('pedido_id')->nullable();

            $table->integer('cliente_id')->unsigned()->index();

            $table->string('estado')->default('creado');
            $table->string('estado_produccion')->nullable();

            $table->decimal('total', 9, 2);

            $table->string('tipo_pago')->default(false);
            $table->string('estado_pago')->default(false);

            $table->integer('envio_id')->unsigned()->index();

            $table->longText('envio')->nullable();
            $table->string('tipo_envio')->nullable();
            $table->string('estado_envio')->nullable();

            $table->longText('carrito');

            // $table->integer('carrito_id')->unsigned()->index();

            $table->text('configuracion')->nullable();
            $table->text('datos')->nullable();
            $table->softDeletes();

            /**k
             * id ->
             *
             * estado - ABIERTO CERRADO CANCELADO
             *
             * estado_produccion
             *
             * productos -- carrito
             *
             * total
             * pago
             * estado_pago
             *
             *
             * cliente_id
             * envio_id
             *
             * notas
             *
             *
             */

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidos');
	}
}
