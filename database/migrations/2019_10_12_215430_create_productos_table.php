<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateProductosTable.
 */
class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion')->nullable();

            $table->integer('stock_publico')->nullable();
            $table->integer('stock_privado')->nullable();

            $table->string('portada')->nullable();
            $table->decimal('precio', 9, 2);
            $table->string('estado')->default(0);

            $table->string('tipo')->default(0);
            $table->string('personalizable')->nullable();
            $table->string('tipo_personalizable')->nullable();

            // $table->decimal('peso')->default(0)->nullable();
            // $table->decimal('largo')->nullable();
            // $table->decimal('ancho')->nullable();
            // $table->decimal('alto')->nullable();

            $table->string('slug');
            $table->text('configuracion')->nullable();
            $table->text('datos')->nullable();

            $table->string('sku')->nullable();
            $table->string('peso')->nullable();
            $table->string('badge')->nullable();
            $table->decimal('precio_promocion', 8, 2)->nullable();
            $table->boolean('destacado')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('productos');
    }
}
